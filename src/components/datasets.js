import React from "react";
import Translate from "@docusaurus/Translate";
import adarchiveImage from "@site/static/img/documentation/project-dataset-adarchive-logo-(cc0).jpg";
import anthologiagraecaImage from "@site/static/img/documentation/project-dataset-anthologia-graeca-logo-(cc0).jpg";
import ethnomusicologyImage from "@site/static/img/documentation/project-dataset-ethnomusicology-logo-(cc0).png";
import histsexImage from "@site/static/img/documentation/project-dataset-histsex-logo-(cc0).jpg";
import moemlImage from "@site/static/img/documentation/project-dataset-moeml-logo-(cc0).png";
import orlandoImage from "@site/static/img/documentation/project-dataset-orlando-logo-(cc0).png";
import orlandoImage2 from "@site/static/img/index-page-images/orlando.png";

import usaskImage from "@site/static/img/documentation/project-dataset-usask-art-logo-(cc0).png";
import yellowNinetiesImage from "@site/static/img/documentation/project-dataset-Y90s-banner-(c-public-domain).png";
import { translate } from "@docusaurus/Translate";
// TODO: Review if published AND status are needed
// TODO: Review if featured order is needed

const sampleDataset = {
  id: "sample-dataset",
  featured: true, // if true, will be displayed in the carousel on the homepage
  published: true,
  featured: true,
  status: "published|transforming|in review",
  page: "/docs/explore-lod/project-datasets/sample-dataset/",
  images: {
    entity: {
      file: "https://via.placeholder.com/150",
      credits: "Sample Dataset Image Credits",
      altText: "Sample Dataset Image",
      entityLink:
        "https://rs.lincsproject.ca/resource/ThinkingFrames?uri=http://temp.lincsproject.ca/sample-dataset",
    },
    logo: {
      file: "https://via.placeholder.com/150",
      credits: "Sample Dataset Image Credits",
      altText: "Sample Dataset Image",
      entityLink:
        "https://rs.lincsproject.ca/resource/ThinkingFrames?uri=http://temp.lincsproject.ca/sample-dataset",
    },
  },
  title: "Sample Dataset",
  longTitle: "Sample Sample Dataset",
  description: "This is a sample dataset.",
  shortDescription: "This is a sample dataset.",
  keywords: [
    "keyword 1",
    "keyword 2",
    "keyword 3",
    "and so on...",
  ],
  buttons: {
    Project: {
      label: "Learn about Sample Dataset",
      link: "/docs/explore-lod/project-datasets/sample-dataset/",
    },
    Website: {
      label: "Learn about Sample Dataset",
      link: "/docs/explore-lod/project-datasets/sample-dataset/",
    },
    "RS Search": {
      label: "Search Sample Dataset",
      link: "https://rs.lincsproject.ca/resource/search:sample-dataset",
    },
    "Application Profile": {
      label: "Sample Application Profile",
      link: "/docs/explore-lod/project-datasets/sample-dataset/",
    },
    "DOI":{
      label: "Sample Dataset DOI",
      link: "https://doi.org/10.1234/5678",
    },
    "Borealis": {
      label: "To Archived Data",
      link: "https://borealis-research.org/",
    },
  },
};

const datasets = {
  moeml: {
    id: "moeml",
    published: false,
    status: "unpublished",
    page: "/docs/explore-lod/project-datasets/moeml/",
    images: {
      logo: {
        file: moemlImage,
        altText: "Map of Early Modern London logo.",
      },
    },

    title: "MoEML",
    longTitle: "Map of Early Modern London",
    description: translate({
      id: "dataset.moeml.description.long",
      description: "Map of Early Modern London dataset description",
      message:
        "Map of Early Modern London maps the spatial imaginary of Shakespeare’s city by asking how London’s spaces and places were named, traversed, used, repurposed, and contested by various practitioners, writers, and civic officials. MoEML’s maps allow us to plot people, historical documents, literary works, and recent critical research onto topography and the built environment. An early contributor to the spatial turn and literary Geographic Information Systems (GIS), MoEML also provides a virtual space for exploring the meaning and representation of cultural space in the London of Shakespeare and his contemporaries.",
    }),
    shortDescription: translate({
      id: "dataset.moeml.description.short",
      description: "Map of Early Modern London dataset description",
      message:
        "Map of Early Modern London maps the spatial imaginary of Shakespeare’s city by asking how London’s spaces and places were named, traversed, used, and more.",
    }),
    keywords: [],
    buttons: {
      "RS Search": {
        label: translate({
          id: "dataset.moeml.button.explore",
          description: "Map of Early Modern London dataset button label",
          message: "Explore Map of Early Modern London",
        }),
        link: "https://rs.lincsproject.ca/resource/search:moeml",
      },
      Project: {
        label: translate({
          id: "dataset.moeml.button.learn",
          description: "Map of Early Modern London dataset button label",
          message: "Learn about MoEML",
        }),
        link: "/docs/explore-lod/project-datasets/moeml/",
      },
      Website: {
        label: "To Project Website",
        link: "https://mapoflondon.uvic.ca/index.htm",
      },
      Vocabulary: {
        label: "To Vocabulary",
        link: "https://vocab.lincsproject.ca/Skosmos/eml/en/",
      },
      "Application Profile": {
        label: "To Application Profile",
        link: "/docs/explore-lod/project-datasets/moeml/moeml-gazetteer-application-profile",
      },
    },
  },
  "usask-art": {
    id: "usask-art",
    published: true,
    featured: true,
    status: "published",
    page: "/docs/explore-lod/project-datasets/usask-art/",
    images: {
      entity: {
        file: "https://saskcollections.org/kenderdine/media/sask_kenderdine/images/1/2/5/33668_ca_object_representations_media_12521_page.jpg",
        credits: translate({
          id: "dataset.usask-art.image.entity.credits",
          description: "USask Art credits text",
          message:
            "Joi T. Arcand, ē-kī-nōhtē-itakot opwātisimowiskwēw (she used to want to be a fancy dancer), 2019, neon and associated hardware, 53 x 596 x 6 cm. Collection of the University of Saskatchewan. Purchase, 2019. Photograph by Carey Shaw.",
        }),
        altText: "",
        entityLink:
          "https://rs.lincsproject.ca/resource/ThinkingFrames?uri=https://saskcollections.org/kenderdine/Detail/objects/6842",
      },
      logo: {
        file: usaskImage,
        altText: "USask Art Galleries & Collection logo.",
      },
    },
    title: "USask Art",
    longTitle: "University of Saskatchewan Art Collection",
    description: translate({
      id: "dataset.usask-art.description.long",
      description: "USask Art dataset description",
      message:
        "The University of Saskatchewan Art Collection is a collection of over 6,000 works, spanning many art movements, styles, subjects, and media. Initially guided by the vision of its first president, Walter Murray, the collection contains works by Group of Seven painters Arthur Lismer and Lawren Harris; works by Saskatchewan based artists Frederick Loveroff, James Henderson, and Ernest Lindner; important early Canadian works; modernist North American and European works; Inuit and Indigenous art objects; and prairie folk art.",
    }),
    shortDescription: translate({
      id: "dataset.usask-art.description.short",
      description: "USask Art dataset short description",
      message:
        "The University of Saskatchewan Art Collection is a collection of over 6,000 works, spanning many art movements, styles, subjects, and media.",
    }),
    keywords: [
      "Academic art",
      "Art",
      "Art collection",
      "Twentieth-century",
      "Twenty-first century",
    ],
    buttons: {
      "RS Search": {
        label: translate({
          id: "dataset.usask-art.button.explore",
          description: "USask Art dataset button label",
          message: "Explore USask Art",
        }),
        link: "https://rs.lincsproject.ca/resource/search:usaskart",
      },
      Project: {
        label: translate({
          id: "dataset.usask-art.button.learn",
          description: "Carousel Learn Dataset label",
          message: "Learn about USask Art",
        }),
        link: "/docs/explore-lod/project-datasets/usask-art/",
      },
      "Application Profile": {
        label: "To Application Profile",
        link: "/docs/explore-lod/project-datasets/usask-art/usask-art-application-profile",
      },
      DOI: {
        label: "DOI",
        link: "https://doi.org/10.5683/SP3/40SXNH",
      },
      Borealis: {
        label: "To Archived Data",
        link: "https://borealisdata.ca/dataset.xhtml?persistentId=doi:10.5683/SP3/40SXNH",
      },
      Website: {
        label: "To Project Website",
        link: "https://artsandscience.usask.ca/galleries/",
      },
    },
  },
  "reed-london-online": {
    id: "reed-london-online",
    published: false,
    featured: false,
    status: "pre-transformation",
    page: "/docs/explore-lod/project-datasets/reed-london-online/",
    images: {
      banner: {
        file: "https://leaf.bucknell.edu/sites/default/files/projects/reed-london/banner/london_visscher_0.jpg",
        altText: "Depiction of old London and the Tamesis Fluvius river.",
      },
      logo: {
        file: "https://leaf.bucknell.edu/sites/default/files/projects/reed-london/logo/reed-london-final_stacked.png",
        altText: "Reed London logo.",
      },
    },
    title: "REED London Online",
    longTitle: "REED London Online",
    description: translate({
      id: "dataset.reed-london-online.description.long",
      description: "REED London Online dataset description",
      message:
        "REED London is a dynamic digital scholarly edition that brings together archival documents and editorial materials about pre-modern London performance, offering new ways of reading and experiencing theatre, music, and all forms of entertainment. REED London develops from the Records of Early English Drama (REED), a four-decades long project dedicated to drawing together evidence of music, theatre, and performance in England, Scotland, and Wales from 1100-1642. The REED London website allows users to examine performance events through collections of archival materials, including contemporary eyewitness accounts, municipal and guild documents, and financial and legal records.",
    }),
    shortDescription: translate({
      id: "dataset.reed-london-online.description.short",
      description: "REED London Online dataset short description",
      message:
        "REED London is a dynamic digital scholarly edition that brings together archival documents and editorial materials about pre-modern London performance.",
    }),
    keywords: [
      "Digital Humanities",
      "History",
      "London, UK",
      "Performance",
      "Place",
    ],
    buttons: {
      "RS Search": {
        label: translate({
          id: "dataset.reed-london-online.button.explore",
          description: "REED London Online dataset button label",
          message: "Explore REED London Online",
        }),
        link: "https://rs.lincsproject.ca/resource/search:usaskart",
      },
      Project: {
        label: translate({
          id: "dataset.reed-london-online.button.learn",
          description: "Carousel Learn Dataset label",
          message: "Learn about REED London Online",
        }),
        link: "/docs/explore-lod/project-datasets/reed-london-online/",
      },
      Website: {
        label: "To Project Website",
        link: "https://reedlondon.online/",
      },
      // "Application Profile": {
      //   label: "REED London Application Profile",
      //   link: "/docs/explore-lod/project-datasets/reed-london-online/reed-london-online-application-profile",
      // },
    },
  },

  "heresies-project": {
    id: "heresies-project",
    published: false,
    featured: false,
    status: "pre-transformation",
    page: "/docs/explore-lod/project-datasets/heresies-project/",
    images: {
      logo: {
        file: "https://leaf.bucknell.edu/sites/default/files/inline-images/heresies_banner_cropped.png",
        altText: "The Heresies Project logo.",
      },
    },
    title: "Heresies Project",
    longTitle: "The Heresies Project",
    description: translate({
      id: "dataset.heresies-project.description.long",
      description: "The Heresies Project dataset description",
      message:
        "The Heresies Project serves to curate and analyze Heresies, an innovative journal dedicated to feminist art and criticism. This collaborative project researches the lives and work of hundreds of women who were involved in the journal’s publication during the years of Second Wave Feminist activity (1977–1993), and through an intersectional framework, it brings the readings of Heresies into the 21st century.",
    }),
    shortDescription: translate({
      id: "dataset.heresies-project.description.short",
      description: "The Heresies Project dataset short description",
      message:
        "The Heresies Project serves to curate and analyze Heresies, an innovative 20th century journal dedicated to feminist art and criticism.",
    }),
    keywords: [],
    buttons: {
      "RS Search": {
        label: translate({
          id: "dataset.heresies-project.button.explore",
          description: "The Heresies Project dataset button label",
          message: "Explore The Heresies Project",
        }),
        link: "https://rs.lincsproject.ca/resource/search:heresies",
      },
      Project: {
        label: translate({
          id: "dataset.heresies-project.button.learn",
          description: "The Heresies Project Carousel Learn Dataset label",
          message: "Learn about The Heresies Project",
        }),
        link: "/docs/explore-lod/project-datasets/heresies-project/",
      },
      Website: {
        label: "To Project Website",
        link: "https://leaf.bucknell.edu/heresies",
      },
      // "Application Profile": {
      //   label: "REED London Application Profile",
      //   link: "/docs/explore-lod/project-datasets/heresies/heresies-application-profile",
      // },
    },
  },

  adarchive: {
    id: "adarchive",
    published: true,
    featured: true,
    status: "published",
    page: "/docs/explore-lod/project-datasets/adarchive/",
    images: {
      entity: {
        file: adarchiveImage,
        credits: "Heresies 4 Diana Press Publications advertisement",
        altText:
          "An old print advertisement for various female created works such as essays and songs.",
        entityLink:
          "https://rs.lincsproject.ca/resource/ThinkingFrames?uri=http://id.lincsproject.ca/LRK8dZhmUxZ",
      },
    },
    title: "AdArchive",
    longTitle: "AdArchive: Tracing Pre-Digital Networked Feminisms",
    description: translate({
      id: "dataset.adarchive.description.long",
      description: "AdArchive dataset description",
      message:
        "AdArchive expands feminist periodical scholarship with an innovative focus on advertisements from feminist-identified journals. To date, there is limited scholarship exploring advertisements in social movement periodicals and minimal exploration of how advertisements functioned to establish and sustain networks among publications, publishers, readers, and social movement organizations.",
    }),
    shortDescription: translate({
      id: "dataset.adarchive.description.short",
      description: "AdArchive dataset description",
      message:
        "AdArchive expands feminist scholarship with an innovative focus on advertisements from feminist-identified journals.",
    }),
    keywords: [
      "Advertisement",
      "Community",
      "Feminism",
      "Feminist movement",
      "Periodical",
      "Publishing",
    ],
    buttons: {
      Project: {
        label: translate({
          id: "dataset.adarchive.button.learn",
          description: "Learn about AdArchive Dataset label",
          message: "Learn about AdArchive",
        }),
        link: "/docs/explore-lod/project-datasets/adarchive/",
      },
      "RS Search": {
        label: translate({
          id: "dataset.adarchive.button.explore",
          description: "Explore Dataset label",
          message: "Explore AdArchive",
        }),
        link: "https://rs.lincsproject.ca/resource/search:adarchive",
      },
      "Application Profile": {
        label: "To Application Profile",
        link: "/docs/explore-lod/project-datasets/adarchive/adarchive-application-profile",
      },
      DOI: {
        label: "DOI",
        link: "https://doi.org/10.5683/SP3/SG2S4R",
      },
      Borealis: {
        label: "To Archived Data",
        link: "https://borealisdata.ca/dataset.xhtml?persistentId=doi:10.5683/SP3/SG2S4R",
      },
    },
  },
  "anthologia-graeca": {
    id: "anthologia-graeca",
    published: true,
    featured: true,
    status: "published",
    page: "/docs/explore-lod/project-datasets/anthologia-graeca/",
    images: {
      entity: {
        file: anthologiagraecaImage,
        entityLink:
          "https://digi.ub.uni-heidelberg.de/diglit/cpgraec23/0079/image,info", //preferred to have an RS link
        // "https://rs.lincsproject.ca/resource/ThinkingFrames?uri=http://temp.lincsproject.ca/anthologie/manuscript/1",
        credits:
          "Codex palatinus graecus 23, page 79 (first page of the palatine anthology)",
        altText: "Cod. Pal. graec. 23, p. 79",
      },
    },

    title: "Anthologia graeca",
    longTitle: translate({
      id: "dataset.anthologia-graeca.title.long",
      description: "Anthologia graeca dataset title",
      message: "Digital collaborative edition of the Greek anthology",
    }),
    description: translate({
      id: "dataset.anthologia-graeca.description.long",
      description: "Anthologia graeca dataset description",
      message:
        "Anthologia graeca gathers information and data on the Greek Anthology: more than 4000 pieces of Greek epigrammatic poetry from the classical to the Byzantine period written by 325 different authors. The platform includes different versions of the Greek texts, various translations of the texts into different languages, notes, commentaries, information on the authors, images of the codex (the palatinus graecus 23), keywords, and places cited.",
    }),
    shortDescription: translate({
      id: "dataset.anthologia-graeca.description.short",
      description: "Anthologia graeca dataset short description",
      message:
        "Anthologia graeca gathers information and data on the Greek Anthology: more than 4000 pieces of Greek epigrammatic poetry from the classical to the Byzantine period.",
    }),
    keywords: ["Digital edition", "Greek anthology", "Greek philology"],
    buttons: {
      Project: {
        label: translate({
          id: "dataset.anthologia-graeca.button.learn",
          description: "Anthologia graeca dataset project page",
          message: "Learn about Anthologia graeca",
        }),
        link: "/docs/explore-lod/project-datasets/anthologia-graeca/",
      },
      Website: {
        label: "To Project Website",
        link: "https://anthologiagraeca.org/",
      },
      "RS Search": {
        label: translate({
          id: "dataset.anthologia-graeca.button.explore",
          description: "Explore Dataset label",
          message: "Explore Anthologia graeca",
        }),
        link: "https://rs.lincsproject.ca/resource/search:anthologiaGraeca",
      },
      "Application Profile": {
        label: "To Application Profile",
        link: "/docs/explore-lod/project-datasets/anthologia-graeca/anthologia-graeca-application-profile",
      },
      DOI: {
        label: "DOI",
        link: "https://doi.org/10.5683/SP3/JLJGDH",
      },
      Borealis: {
        label: "To Archived Data",
        link: "https://borealisdata.ca/dataset.xhtml?persistentId=doi:10.5683/SP3/JLJGDH",
      },
    },
  },
  "yellow-nineties": {
    id: "yellow-nineties",
    published: true,
    featured: true,
    status: "published",
    page: "/docs/explore-lod/project-datasets/yellow-nineties/",
    images: {
      entity: {
        file: yellowNinetiesImage,
        altText: "",
      },
    },
    title: "Y90s",
    longTitle: "Yellow Nineties 2.0",
    description: translate({
      id: "dataset.yellow-nineties.description.long",
      description: "Yellow Nineties 2.0 dataset description",
      message:
        "Yellow Nineties 2.0 uses digital tools to advance knowledge of eight late-Victorian little magazines and the people who contributed to their production between 1889 and 1905. Each digital edition is accompanied by an editorial overview of the magazine as a whole and includes a critical introduction to every volume in its print run.",
    }),
    shortDescription: translate({
      id: "dataset.yellow-nineties.description.short",
      description: "Yellow Nineties 2.0 dataset description",
      message:
        "Yellow Nineties 2.0 uses digital tools to advance knowledge of eight late-Victorian little magazines and the people who contributed to their production between 1889 and 1905.",
    }),
    keywords: [
      "1837-1901",
      "1901-1910",
      "Aestheticism",
      "Art and Literature",
      "Authors and Publishers",
      "Great Britain",
      "Periodicals",
      "Private Presses",
    ],
    buttons: {
      Project: {
        label: translate({
          id: "dataset.yellow-nineties.button.learn",
          description: "Carousel Learn Yellow Nineties button label",
          message: "Learn about Y90s",
        }),
        link: "/docs/explore-lod/project-datasets/yellow-nineties/",
      },
      Website: {
        label: "To Project Website",
        link: "https://1890s.ca/",
      },
      "RS Search": {
        label: translate({
          id: "dataset.yellow-nineties.button.explore",
          description: "Explore Yellow Nineties button label",
          message: "Explore Y90s",
        }),
        link: "https://rs.lincsproject.ca/resource/search:yellowNineties",
      },
      "Application Profile": {
        label: "To Application Profile",
        link: "/docs/explore-lod/project-datasets/yellow-nineties/yellow-nineties-application-profile",
      },
      DOI: {
        label: "DOI",
        link: "https://doi.org/10.5683/SP3/2FTQXM",
      },
      Borealis: {
        label: "To Archived Data",
        link: "https://borealisdata.ca/dataset.xhtml?persistentId=doi:10.5683/SP3/2FTQXM",
      },
    },
  },
  orlando: {
    id: "orlando",
    published: false,
    featured: true,
    status: "published",
    page: "/docs/explore-lod/project-datasets/orlando/",
    images: {
      logo: {
        file: orlandoImage,
        altText: "Logo for Orlando.",
      },
      entity: {
        file: orlandoImage2,
        altText: "",
      },
    },
    title: "Orlando",
    longTitle: "Orlando Project",
    description: translate({
      id: "dataset.orlando.description.long",
      description: "Orlando dataset description",
      message:
        "The Orlando Project explores and harnesses the power of digital tools and methods to advance feminist literary scholarship.",
    }),
    shortDescription: translate({
      id: "dataset.orlando.description.short",
      description: "Orlando dataset description",
      message:
        "The Orlando Project explores and harnesses the power of digital tools and methods to advance feminist literary scholarship.",
    }),
    keywords: [],
    buttons: {
      Project: {
        label: translate({
          id: "dataset.orlando.button.learn",
          description: "Carousel Explore Orlando label",
          message: "Learn about Orlando",
        }),
        link: "/docs/explore-lod/project-datasets/orlando/",
      },
      Website: {
        label: "To Project Website",
        link: "https://www.artsrn.ualberta.ca/orlando/",
      },
      "RS Search": {
        label: translate({
          id: "dataset.orlando.button.explore",
          description: "Explore Orlando label",
          message: "Explore Orlando",
        }),
        link: "https://rs.lincsproject.ca/resource/search:orlando",
      },
      "Application Profile": {
        label: "To Application Profile",
        link: "/docs/explore-lod/project-datasets/orlando/orlando-application-profile",
      },
    },
  },
  histsex: {
    id: "histsex",
    published: true,
    status: "published",
    page: "/docs/explore-lod/project-datasets/histsex/",
    images: {
      logo: {
        file: histsexImage,
        altText:
          "LGBTQ+ march highlighting two people carring a flag labeled Gay Liberation front.",
      },
    },
    title: "HistSex",
    longTitle: "HistSex.Org",
    description: translate({
      id: "dataset.histsex.description.long",
      description: "HistSex dataset description",
      message:
        "HistSex is a freely-available, peer reviewed resource for information on the history of sexuality developed by sex educators, historians, and librarians. It includes a descriptive catalog of relevant digital projects on the history of sexuality; a searchable and annotated directory of LGBTQ+ research archives; a timeline of major events in the history of sexuality; and a bibliography of books tagged and searchable by research interest, reading level, topic, and more—all presented online in an open and easily accessible format.",
    }),
    shortDescription: translate({
      id: "dataset.histsex.description.short",
      description: "HistSex dataset description",
      message:
        "HistSex is a freely-available, peer reviewed resource for information on the history of sexuality developed by sex educators, historians, and librarians.",
    }),
    keywords: [],
    buttons: {
      Project: {
        label: translate({
          id: "dataset.histsex.button.learn",
          description: "histsex learn button label",
          message: "Learn about HistSex",
        }),
        link: "/docs/explore-lod/project-datasets/histsex/",
      },
      Website: {
        label: "To Project Website",
        link: "https://histsex.org/",
      },
      "RS Search": {
        label: translate({
          id: "dataset.histsex.button.explore",
          description: "histsex explore button label",
          message: "Explore HistSex",
        }),

        link: "https://rs.lincsproject.ca/resource/search:histSex",
      },
      "Application Profile": {
        label: "To Application Profile",
        link: "/docs/explore-lod/project-datasets/histsex/histsex-application-profile",
      },
      DOI: {
        label: "DOI",
        link: "https://doi.org/10.5683/SP3/20EVXK",
      },
      Borealis: {
        label: "To Archived Data",
        link: "https://borealisdata.ca/dataset.xhtml?persistentId=doi:10.5683/SP3/20EVXK",
      },
    },
  },
  ethnomusicology: {
    id: "ethnomusicology",
    published: false,
    status: "in review",
    page: "/docs/explore-lod/project-datasets/ethnomusicology/",
    images: {
      logo: {
        file: ethnomusicologyImage,
        altText: "The Canadian Centre for Ethnomusicology logo.",
      },
    },
    title: "Ethnomusicology",
    longTitle: "Canadian Centre for Ethnomusicology",
    description: translate({
      id: "dataset.ethnomusicology.description.long",
      description: "Ethnomusicology dataset description",
      message:
        "The Canadian Centre for Ethnomusicology is an archive and researchresource documenting musical and cultural traditions locally andinternationally. The collection includes diverse instruments and morethan 4000 titles in audio/video recordings. The Centre helps usersunderstand how people use music to connect, express, and createcommunity and identity. It is of value to students and faculty in thesocial sciences, humanities, education, and fine arts.",
    }),
    shortDescription: translate({
      id: "dataset.ethnomusicology.description.short",
      description: "Ethnomusicology dataset description",
      message:
        "The Canadian Centre for Ethnomusicology is an archive and research resource documenting musical and cultural traditions locally and internationally.",
    }),
    keywords: [],
    buttons: {
      Project: {
        label: translate({
          id: "dataset.ethnomusicology.button.learn",
          description: "ethnomusicology learn button label",
          message: "Learn about Ethnomusicology",
        }),
        link: "/docs/explore-lod/project-datasets/ethnomusicology/",
      },
      Website: {
        label: "To Project Website",
        link: "https://www.artsrn.ualberta.ca/ccewiki/index.php/The_Canadian_Centre_for_Ethnomusicology_(CCE)",
      },
      "RS Search": {
        label: translate({
          id: "dataset.ethnomusicology.button.explore",
          description: "Explore Ethnomusicology button label",
          message: "Explore Ethnomusicology",
        }),
        link: "https://rs.lincsproject.ca/resource/search:ethnomusicology",
      },
      "Application Profile": {
        label: "To Application Profile",
        link: "/docs/explore-lod/project-datasets/ethnomusicology/ethnomusicology-application-profile",
      },
    },
  },
};

export default datasets;
