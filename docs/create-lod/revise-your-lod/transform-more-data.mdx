---
sidebar_position: 10
title: "Transform More Data"
description: "LINCS Transformation Workflows — Transform More Data"
toc_min_heading_level: 2
toc_max_heading_level: 3 
---

import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';

## Introduction

Once you have made it through a full transformation workflow and published your data in [ResearchSpace](/docs/tools/researchspace/), what happens when you have more data to add? You have a few options depending on what is new about the new data.

## Transformation Options

### Edit your Data in ResearchSpace Review

If your new data is a relatively small number of new :Term[entities]{#entity} or connections between them, then editing directly in [ResearchSpace Review](/docs/tools/researchspace/researchspace-review-documentation) is a good option. 

ResearchSpace Review is the ResearchSpace environment with additional features for [editing data](/docs/tools/researchspace/researchspace-review-documentation#edit-an-entity) and [editing the project's landing page](/docs/tools/researchspace/researchspace-review-documentation#edit-your-dataset-project-page).

[Register for a ResearchSpace Review account](/docs/tools/researchspace/researchspace-review-documentation) to obtain editing permissions.

:::warning
When you edit your data directly in ResearchSpace Review, it creates a new version that diverges from your original untransformed source data. If you want those changes to apply to versions of your data outside of ResearchSpace, you will need to apply the same changes there.

If you plan on continuing to make significant changes to your original data and want those changes to appear in ResearchSpace, [contact LINCS](/docs/about-lincs/get-involved/contact-us) to discuss options.
:::

### Rerun a Transformation Workflow on New Data

This is the case where you have a new batch of data that follows the same structure and contains the same relationships as your originally transformed batch.

<Tabs groupId="transformation-workflows" queryString="workflow">
<TabItem value="structured" label="Structured Data" default>

An example here would be having new rows for the spreadsheet you transformed originally. 

Here is how each step will need to change and be repeated:

- [Export Data](/docs/create-lod/export-data)
    + Repeat the same process.
- [Clean Data](/docs/create-lod/clean-data)
    + If you used a script, then run it on the new data. 
    + If you made manual changes, you need to apply those to the new data. Note that if you used [OpenRefine](/docs/tools/openrefine), you may be able to open the original project and export the change history from the Undo/Redo tab.
- [Match Entities](/docs/create-lod/match-entities)
    + Any entities that did not appear in the first batch needs to be :Term[matched entities]{#entity-matching} externally.
    + Any entities that appear in the first batch and the new batch need to be matched against one another (i.e., use the same identifier for the same entity in both batches).
- [Develop Conceptual Mapping](/docs/create-lod/map-data/develop-conceptual-mapping)
    + Because the structure of your data has not changed, you can reuse the same conceptual mapping from your original Develop [Conceptual Mapping](/docs/create-lod/map-data/develop-conceptual-mapping) step.
- [Implement Conceptual Mapping](/docs/create-lod/map-data/implement-conceptual-mapping)
    + The script or template you used to implemented that conceptual mapping will need to be rerun.
    + If you used [3M](/docs/tools/x3ml/), then you only need to replace the input file for your 3M mapping project with your new data and hit run in 3M.
- [Validate and Enhance](/docs/create-lod/validate-and-enhance)
    + Again, either the script you used or the manual changes you made will need to be repeated.

</TabItem>
<TabItem value="semistructured" label="Semi-Structured Data">

The repeatability of this workflow varies because this workflow looks different for every project.

If you know you will continue to have new data of the same format, then ideally you will setup a repeatable set of steps that use automated scripts or templates. If that is not possible, then you will have to repeat the more manual process for each new batch of data.

</TabItem>
<TabItem value="tei" label="TEI Data">

This process is likely to take the same amount time as the original transformation.

New :Term[TEI data]{#tei-data} containing the same relationships can follow the same [LINCS XTriples](/docs/tools/xtriples) workflow as the first batch. Similar to the Structured Data Workflow, the rest of the steps will need to be redone—either by rerunning scripts, reusing the same tools, or redoing manual work.

</TabItem>
<TabItem value="natural" label="Natural Language Data">

This process is likely to take the same amount time as the original transformation.

New :Term[natural language data]{#natural-language-data} containing the same relationships can follow the same workflow as the first batch. Similar to the Structured Data Workflow, the rest of the steps will need to be redone—either by rerunning scripts, reusing the same tools, or redoing manual work.

</TabItem>
</Tabs>

:::info
For new data, you still need to match entities against external sources, but now you also need to match entities against your already transformed data.
:::

### Run a New Transformation Workflow on New Data

If you have new data that does not have the same starting structure as the data you originally transformed, or if it contains many new relationships, then you will need to repeat the appropriate transformation workflow on the new data. You may be able to use an edited version of your original transformation workflow if there are similarities between the batches of data.

## Publication Options

Your newly transformed data can be combined with data you already have in ResearchSpace so that it appears as a single project and :Term[named graph]{#named-graph} in the LINCS :Term[triplestore]{#triplestore}.

Alternatively, if it covers new subject matter and is part of a different research project, it can be published as a separate project in ResearchSpace and be stored in a different named graph.

{/*If the **research team** wants to add more data after the transformation process, LINCS can rerun the transformation if the new data has the exact same structure as the initial data. If the new data does not have the same structure, the transformation process needs to be started again from the beginning. New data can then be merged with the existing project or can be made into a new, separate project. If you have more data to transform, continue to the [Transform More Data](/docs/create-lod/revise-your-lod/transform-more-data) step. */}
