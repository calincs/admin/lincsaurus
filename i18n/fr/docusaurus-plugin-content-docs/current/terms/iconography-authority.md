---
id: iconography-authority
title: Iconography Authority (IA)
definition: L’un des cinq vocabulaires Getty contenant des identifiants de ressources uniformes (URI) pour les noms propres, les relations, les thèmes et les dates liés aux récits iconographiques, aux personnages légendaires ou fictifs, aux événements historiques, aux œuvres littéraires et aux arts du spectacle.
Machine Translation: True     
Translation Tool: DeepL 
Translated Added By: Humna Chaudhry(@humnq)
Last Translated: 2024-06-21
---

:::info
Cette page web a été traduite automatiquement par DeepL. Bien que nous nous efforcions d’être précis, nous vous informons que les traductions peuvent contenir des erreurs ou des inexactitudes. Pour obtenir les informations les plus précises, veuillez vous référer à la version originale.
:::

L'Iconography Authority (IA) est l'un des cinq vocabulaires Getty développés par le Getty Research Institute pour fournir des termes et des :Term[Uniform Resource Identifiers (URIs)]{#uniform-resource-identifier} pour les concepts et les objets liés aux arts. LINCS utilise l'IA pour fournir des URI pour les noms propres, les relations, les thèmes et les dates liés aux récits iconographiques, aux personnages légendaires ou fictifs, aux événements historiques, aux œuvres littéraires et aux arts du spectacle.

L'AI comprend des termes relatifs à :

- les événements, les thèmes et les récits de l'histoire, de la religion et de la mythologie
- les personnages légendaires et fictifs
- les lieux légendaires et fictifs
- Thèmes de la littérature et des arts du spectacle
- Noms non couverts par les autres vocabulaires Getty

L'AI exclut les noms couverts par les autres vocabulaires Getty.

## Autres ressources

- [Iconography Authority (IA)](https://www.getty.edu/research/tools/vocabularies/cona/index.html)
- J. Paul Getty Trust (2022) [“About CONA and IA”](https://www.getty.edu/research/tools/vocabularies/cona/about.html)
