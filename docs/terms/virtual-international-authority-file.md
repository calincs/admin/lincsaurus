---
id: virtual-international-authority-file
title: Virtual International Authority File (VIAF)
definition: A service that aggregates the catalogues of many national libraries and miscellaneous authority files.
---

The Virtual International Authority File (VIAF) is a service that aggregates the catalogues of many national libraries and miscellaneous :Term[authority records]{#authority-record} to provide convenient access to the world’s major name :Term[authority files]{#authority-file}. At LINCS, VIAF is used as a source of :Term[Uniform Resource Identifiers (URIs)]{#uniform-resource-identifier} for people and for bibliographic entities during :Term[entity matching]{#entity-matching}.

## Further Resources

- OCLC (2022) [“VIAF Overview”](https://www.oclc.org/en/viaf.html)
- [VIAF: The Virtual International Authority File](https://viaf.org/)
