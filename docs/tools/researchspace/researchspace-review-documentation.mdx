---
title: "ResearchSpace Review Documentation"
sidebar_position: 3
---



<div className="primary-button-row">
  <PrimaryButton
    link="https://rs-review.lincsproject.ca/"
    buttonName="To ResearchSpace Review"
    />
</div>

## Overview

LINCS maintains **ResearchSpace Review** as a distinct ResearchSpace environment for researchers with data in LINCS.  

**ResearchSpace Review** gives you the same features as the ResearchSpace environment with some additional functionality:

- View and [edit your own pre-published data](#edit-an-entity)
- Edit the information on the [dataset project page](#edit-your-dataset-project-page)

### Prerequisites
- Create an account in [Researchspace Review](http://rs-review.lincsproject.ca)
- [Contact LINCS](/docs/about-lincs/get-involved/contact-us) to request or confirm dataset editing permissions.

Consult the documentation for ResearchSpace to learn how to:

- [Create an account](/docs/tools/researchspace/researchspace-documentation#create-an-account)
- [Browse LINCS data](/docs/tools/researchspace/researchspace-documentation#browse-lincs-data)
- [Search LINCS data](/docs/tools/researchspace/researchspace-documentation#search-lincs-data)
- [SPARQL Search](/docs/tools/researchspace/researchspace-documentation#sparql-search)
- [Create a Knowledge Map](/docs/tools/researchspace/researchspace-documentation#create-a-knowledge-map)
- [Create a Semantic Narrative](/docs/tools/researchspace/researchspace-documentation#create-a-semantic-narrative)


## Edit an Entity

You can add, change, or delete properties from entities in your dataset with the **entity editing form**. Changes will be saved to the :Term[triplestore]{#triplestore}.


1. Find the entity you want to edit.
2. Look for the **pencil icon or edit link** on the entity summary page or entity card.

|<img src="/img/documentation/researchspace-entity-summary-edit-properties-(c-LINCS).png" title="edit entity link on entity summary page" alt="" height="" width="100%"/>|
|:--:|
|_Location of the **Edit this entity** link on the entity summary page_|

|<img src="/img/documentation/researchspace-entity-card-edit-properties-(c-LINCS).png" title="edit entity link on entity card" alt="" height="100%" width=""/>|
|:--:|
|_Location of the **Edit** icon on the entity card_|

3. The **entity editing form** opens. If you get an error message, we may need to adjust the permissions. Please [contact LINCS](/docs/about-lincs/get-involved/contact-us). 


|![search-within](/img/documentation/researchspace-entity-editing-form-(c-LINCS).png)|
|:--:|
|_Entity editing form_|

The input fields you see on the entity editing form depend on the type of entity. In the case of a Person entity, for example, you will see input fields for _was born_ and _is parent of_.  

Each input field on the form is mapped to a permitted property (i.e., predicate) and permitted value type (i.e., IRI, string, number, date, etc.). These set parameters on the kinds of edits you can make.

### Edit a String or Numeric Value

You can change an existing string or numeric value directly in the input field. In this example, you can change the string _Florence Nightingale_ to _Florence Martha Nightingale_. To save changes, scroll down to the bottom of the entity editing form and click **Save**.


|![search-within](/img/documentation/researchspace-entity-editing-form-edit-field-(c-LINCS).png)|
|:--:|
|_Edit a string or numeric value directly in the input field_|

### Remove or Replace an IRI

An IRI is not editable. It can only be removed using the “x” to the right of the input field. In this example, the only permissible change to _The Royal Society for the Prevention of Cruelty to Animals_ is to remove this IRI from the field.

|![search-within](/img/documentation/researchspace-entity-editing-form-delete-field-(c-LINCS).png)|
|:--:|
|_Remove an IRI_|

You can add a different IRI to this input field. When adding an IRI value, the autocomplete lookup allows you to type inside the input box, revealing a list of entity values from existing IRIs within ResearchSpace. 

It is always preferable to reuse an existing IRI from within ResearchSpace to enhance shared connections within and across datasets in ResearchSpace.

In this example, typing _dicky_ in the input field _is current or former member_ retrieves _Dicky Bird Society_. Select it, then scroll down to the bottom of the entity editing form to save the change.


|![search-within](/img/documentation/researchspace-entity-editing-form-replace-value-(c-LINCS).png)|
|:--:|
|_Enter an IRI into an input field_|

### Add an Input Field

To add a more values to a property, click **+Add** under any input field. In this example, five input fields were created to connect to all the groups Florence Nightingale belonged to.

|![search-within](/img/documentation/researchspace-entity-editing-form-add-new-value-(c-LINCS).png)|
|:--:|
|_Add an input field_|



### Match Entities an Entity

Use the **Same As** field in the entity editing form to match an entity. You can enter a LINCS URI or a URI from an external authority like Wikidata or Getty. Remember to save your changes.

## Create a New Entity

This is an advanced task that requires you to understand clearly what kind of entity you want to create and for what purpose. You can create a new entity connected to an existing entity or add an entirely new entity to your dataset.

### Create a New Entity Connected to an Existing Entity 

1. Find the entity in ResearchSpace that you want to connect a new entity to. Click **Edit this entity** on the entity summary page (or the **Edit** icon on the entity card). This opens an entity editing form  as described under [Edit an Entity](#edit-an-entity).
2. Choose the input field where you want to connect a new entity. There will either be a **+Create new** button to the right of the input field, or a **Create New** link under the input field.  

- **If there is a _+Create new_ button to the right of the input field**, the type of entity you can add is predetermined. Click the button to open an overlay editing form where you can create a new entity.

For example, if you want to create a parent for Florence Nightingale, you can only create a Person entity. On the entity editing form for Florence Nightingale find the _Has Parent_ property. Click the **+Create new** button to the right of this input field. 
|![search-within](/img/documentation/researchspace-entity-editing-form-create-new-button-(c-LINCS).png)|
|:--:|
|_Location of the **+Create new** button_|

The overlay editing form opens. Now you can create a new Person entity. Fill in the fields and save the entity. It will automatically connect to the primary entity you are editing.
|![search-within](/img/documentation/researchspace-entity-editing-form-create-new-entity-overlay-(c-LINCS).png)|
|:--:|
|_**+Create new** button opens an overlay form where you can create a new entity_|

- **If there is a _Create New_ link under the input field**, this indicates that you can create any type of entity. 

For example, the property _is about_ can connect to any type of entity. Click **Create New** to open the Entity Editor. Select an entity type, and continue by following steps 3-5 in the section below: [Add a New Entity to your Dataset](#add-a-new-entity-to-your-dataset). 

|![search-within](/img/documentation/researchspace-entity-editing-form-create-new-link-(c-LINCS).png)|
|:--:|
|_Location of the **Create New** link_|


3. Remember to save your changes. 

### Add a New Entity to your Dataset

In this case, you will be adding a new entity with the purpose of having it stand on its own as a completely new thing (i.e., adding a new work, person, or place, etc. to your dataset).

1. On the homepage, click **Create/Edit Entities** in the **Manage Data** box. 

    |![search-within](/img/documentation/researchspace-entity-editing-create-new-entity-from-homepage-(c-LINCS).png)|
   |:--:|
   | _Location of the **Create/ Edit Entities** link on the homepage_|

2. Select an entity type on the **Entity Editor** form. It is currently not possible to assign more than one entity type to a new entity through the ResearchSpace editing interface.

    |![search-within](/img/documentation/researchspace-entity-editor-form-select-entity-type-(c-LINCS).png)|
    |:--:|
    |_Select an entity type_|

3. If you have permission to edit more than one graph (i.e., dataset), you will see these graphs listed at the top of the editing form. Select the appropriate graph where you will save this new entity. 

    In the example below, a Person entity will be created and saved to the graph **Orlando**.

    |![search-within](/img/documentation/researchspace-entity-editing-select-graph-(c-LINCS).png)|
    |:--:|
    |_Choose the graph (dataset), where you will save this entity_|

4. Now select **Use External Entity** or **Create New Entity**.
    - **Use External Entity** i.e., search for an existing entity from an external source such as Wikidata or Getty. If using an external entity, the IRI and a label will be generated. 
    - **Create New Entity** to mint a new IRI.   

    |![search-within](/img/documentation/researchspace-create-new-entity-use-external-or-mint-new-(c-LINCS).png)|
    |:--:|
    |_Use an entity from an external source or mint a new one_|

5. Enter values into the input fields on the entity editing form. 

6. Remember to save your changes.

###  Delete an Entity

Use this action cautiously as it will permanently remove the entity from the :Term[triplestore]{#triplestore}.

1. Find the entity in ResearchSpace you want to delete. 
2. Click **Edit this entity** on the entity summary page (or the **Edit** icon on the entity card). This opens the entity editing form.
3. Click **Delete** at the bottom of the entity editing form.

## Edit Your Dataset Project Page

1. Under the **Manage Data** section on the homepage, click **Edit Dataset Information**.
2. Click **Edit** (pencil icon) on your dataset card.
3. Cick the **Use** button where it says **Metadata (Project/Dataset) Form**.
4. Edit the Metadata form as needed. (See details about the form below).
5. Click **Save**.

### Dataset Project Page Metadata Form 

These are the fields on the metadata form. Hover over the **?** icons on the form for explanations about each field. Fields marked with an asterisk (*) are required.

- **Named Graph** : [Contact LINCS](/docs/about-lincs/get-involved/contact-us) before editing this field.
- **Title**: [Contact LINCS](/docs/about-lincs/get-involved/contact-us) before editing this field.
- **Short Title**: [Contact LINCS](/docs/about-lincs/get-involved/contact-us) before editing this field. The Short Title is used to label tabs in ResearchSpace.
- **Description**: Use this field to briefly describe your dataset. Example: "The Orlando Project's collaborative experiment in feminist literary history started in 1995, published its flagship resource, Orlando: Women’s Writing in the British Isles, in 2006 with Cambridge University Press, and has produced an accompanying linked dataset for LINCS based on the semantic tagging in its 8+ million words of original scholarship about women writers’ lives, works, and cultures."
- **Short Description**: Use this field to describe your dataset in 1-2 sentences. Example: "The Orlando Project explores and harnesses the power of semantic tools and methods to advance feminist literary scholarship. View Orlando as LOD!"
- **Creator**: Add as many project creators as desired. Your can add, remove, or edit the list of creators. Use the **Edit** button to add details about each contributor. These details include:
  - **Type**: Person or Group/Organization
  - **Full Name**: Name Lastname
  - **Same As**: Use this field for personal identifiers like [ORCID](https://orcid.org/) or [VIAF](https://viaf.org/).
  - **Role Type**: Options include: Researcher, Literary Director, Director, Associate Director, Research Director, Founding Director, Technical Director, Contributor, Registrar
  - **Email Address**: Enter a valid email address.
  - **Contributor**: Add as many additional contributors as desired. You can add, remove, or edit the list of contributors. Contributors and creators are not distinguishable on the dataset landing page, so you can simply the use the Creator field for all project creators and contributors.
- **Status**: The status of your data may be draft, pre-published, or published. [Contact LINCS](/docs/about-lincs/get-involved/contact-us) before editing this field.
- **Language**: Use this field to add as many languages as are reflected in your dataset.
- **Source Dataset**: This field must be a URL.
- **Image**: [Contact LINCS](/docs/about-lincs/get-involved/contact-us) before editing this field.
- **Logo (Or Cover Image)**: [Contact LINCS](/docs/about-lincs/get-involved/contact-us) before editing this field.
- **Dataset Landing Page Carousel Images**: Your carousel includes three entities from your dataset. If you leave this field blank, ResearchSpace will randomly select three entities from your dataset. To choose the entities which will appear in the carousel, click **+Add dataset landing page carousel images**. If you know the IRI for the image enter it in the field. 
  You can find the IRI on the entity's [Entity Summary Page](/docs/tools/researchspace/researchspace-documentation#entity-summary-page). 

Under the Outgoing Statements/Entity as Subject tab, locate the **has representation** property and click the associated **value** link.

  |![search-within](/img/documentation/researchspace-has-representation-(c-LINCS).png)|
  |:--:|
  |_Location of the **Has representation** property link on the entity summary page_|
  
  Click the **copy** icon next to the Resource Identifier. A confirmation message will appear: "The IRI has been copied to clipboard!"

|![search-within](/img/documentation/researchspace-copy-IRI-(c-LINCS).png)|
|:--:|
|_Location of the Resource Identifier link_|

  Paste the IRI into the field on the metadata form.

   |![search-within](/img/documentation/researchspace-paste-IRI-(c-LINCS).png)|
   |:--:|
   |_Paste the Resource Identifier link into this field on the dataset project metadata form_|

- **Identifier**: Use this field to add an affiliated website, project search page, dataset source URL, archived dataset URL, or Transformed LOD Source URL.
- **Subject/ Topic**: Use this field to add IRIs representing the subject(s) of your dataset:
  - Click **+Add subject/topic** to search for an existing subject or topic. 
  - Scroll through the list of IRIs provided. In this example, the topic of the dataset is "feminism." This is how you decode the IRIs:
    |![search-within](/img/documentation/researchspace-example-IRI-(c-LINCS).png)|
    |:--:|
    | In the highlighted example, _feminism_ is the label, ```http://id.lincproject.ca/cwrc/feminism``` is the IRI, ```a http://cidoc-crm.org/cidoc-crm/E89_Propositional_Object``` is the type or class, and from ```http://id.lincsproject.ca/cwrc``` indicates the dataset this IRI derives from. |  
- **Temporal Coverage**: Use this field to indicate the date range covered in the dataset.
- **Disclaimer**: Use this field if you wish to add a disclaimer to the dataset landng page and/or on the entity pages.