---
sidebar_position: 4
title: "Data Mobilization Microgrants"
description: "Apply for LINCS research funding"
---

:::info

The Data Mobilization Microgrants program has ended.

In future, we hope to be able to provide funding to researchers who are interested in mobilizing their data as LOD. For more information about future funding opportunities [contact us](/docs/about-lincs/get-involved/contact-us).

We are still able to work with new projects. For more information about creating LOD, see our [transformation workflows](/docs/create-lod/get-started).

:::

## Data Mobilization Microgrants (2020–2023) [Program Ended]

### Program Details

LINCS invites applications to its microgrants program to provide funding and support to researchers who have datasets of cultural material they would like to transform into :Term[Linked Open Data (LOD)]{#linked-open-data} and contribute to the LINCS :Term[triplestore]{#triplestore}. These researchers and datasets can either be those that are already participating in LINCS or those that are not yet involved.

Individuals and groups applying must have datasets ready to be mobilized as LOD and must have time to commit to the [data cleaning](/docs/create-lod/clean-data/#clean-your-dataset) and [data mapping](/docs/create-lod/map-data/implement-conceptual-mapping#transform-your-data) process.

### Provided Resources

Researchers who receive support to prepare their datasets to be included in LINCS will be given a selection of the following, depending on their research and data needs:

- **Support meetings** with the LINCS core team for [data transformation](/docs/create-lod/get-started) and research applications
- **Funds** to support one student-semester (at home-institution rates) for data transformation and cleaning; funds can be used for one semester of student assistance (typically 10-12 hours/week) or can be parcelled out across multiple students or semesters
- **Training** related to data transformation and cleaning for the project lead, project staff, and research assistants
- **Free or reduced registration** to participate in LINCS workshops, conferences, and training events

### Researcher Commitment

Grant recipients will be expected to participate in training activities, join one or more [areas of inquiry](/docs/about-lincs/research#areas-of-inquiry), write a post for the [LINCS blog](/blog), present (remotely or in person) on their in-progress work or findings at a [LINCS conference or event](/docs/about-lincs/get-involved/events), and—where possible—take up the opportunity to publish in an open-access publication about research facilitated by LINCS.

### Eligibility

LINCS is ingesting a broad range of cultural data. The project is open to taking structured, TEI, and :Term[natural language]{#natural-language-data} datasets so long as the source data itself is readily available. For more information, see our [transformation workflows](/docs/create-lod/get-started).

Note that researchers must be Canadian, based at a Canadian institution, or working on a dataset that is predominantly Canadian. Projects related to geographically contiguous territorial and border Indigenous nations are encouraged to apply. Researchers from underrepresented groups or working with datasets related to underrepresented communities beyond Canada will also be given consideration. Datasets must fall within one of LINCS’s [areas of inquiry](/docs/about-lincs/research#areas-of-inquiry).

:::note

Note that LINCS stores only :Term[Linked Data (LD)]{#linked-data} and not source datasets. The source datasets for LD ingested into LINCS must be stored elsewhere. LINCS can advise on options for appropriate archiving of source datasets so they remain connected to the LD.

:::

## Submissions

To apply for a LINCS Data Mobilization Microgrant, we ask you to fill out a [form](https://docs.google.com/forms/d/e/1FAIpQLSdpU8jc9CuDK7Q2EePNvl-8S1DktmCWMtytEWq0L-zJNP20cg/viewform) with the following information:

- Project name
- List of applicants, with one applicant identified as the project lead
- Summary CV for each named applicant (maximum 2 pages)
- The Area of Inquiry group(s) into which the project fits
- A data sample (a small sample of the data in its current form, containing approximately 25 sets of relationships which would become LOD triples)
- Project proposal, including:
  - A **project summary** describing the research questions you want to ask if your data is turned into LOD (maximum 500 words)
  - A description of your **dataset**, including details on the size of the dataset, the data format, and where the data is currently located (maximum 250 words)
  - A **timeline** for data ingestion, with information about how much time you have to contribute to the project and how much work you estimate it will take to ingest and clean your data (maximum 250 words)
  - A **budget and justification**, noting the value of a student-semester at your institution’s current rates and how you would plan to distribute the student work (number of students, number of hours/week, period of time (maximum 250 words). Please note also whether you have other funding for your larger research project, and/or any additional resources to bring to this work.

:::note

If your dataset has already already been discussed for inclusion in LINCS at a dataset intake interview, we encourage you to reuse content from your dataset intake questionnaire for your application. If your dataset has not already been discussed for inclusion, or you have not had a dataset intake interview, please contact us if you would like to hold your interview or complete your dataset intake questionnaire before applying.

:::

### Contact

For more information, please [contact us](/docs/about-lincs/get-involved/contact-us).
