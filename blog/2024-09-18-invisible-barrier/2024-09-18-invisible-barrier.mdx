---
slug: invisible-barrier
title: "Invisible to the Sighted, Barrier to the Blind"
authors: Humna Chaudhry
tags: [UX]
hide_table_of_contents: true
image: /invisible-barrier-unsplash-(cc0).jpg
Last Translated: 2024-08-21
date: "2024-09-18"
description: "Ever wonder how accessible your website is for people with disabilities? This summer, Humna worked on improving the LINCS project site’s accessibility and discovered how small design choices can make a big difference for all users. Learn about key lessons in accessible web design and how you can help make the web more inclusive for everyone."
---

![](<./invisible-barrier-unsplash-(cc0).jpg>)

I have spent most of my summer improving the accessibility of the LINCS project website. During this process, I have learnt so much about how those with disabilities navigate the Internet. There has been a lot to do, and it's taken up a lot of my time—but it certainly doesn't have to be that way! If designers incorporate accessibility from the beginning, minimal extra effort makes for a better experience for all users, not only those with barriers.

{/* truncate */}

**27% of Canadians have one or more disabilities that hinder their daily activities according to the [Canadian Survey on Disability](https://www150.statcan.gc.ca/n1/daily-quotidien/231201/dq231201b-eng.htm).**

Furthermore, an estimated [1.5 million Canadians have sight loss, and 6 million more have a disease which could lead to sight loss.](https://www.cnib.ca/en/sight-loss-info/blindness/blindness-canada?region=on#:~:text=Today%2C%20an%20estimated%201.5%20Million,Canadian%20Survey%20on%20Disabilities%202017.) Without the use of screen readers, these individuals would find it difficult to, and in some cases have no way to, navigate the web. Think about how much you rely on your phone or computer for banking, contacting people, searching for information, booking appointments, finding jobs, creating calendars—the uses are endless. An inaccessible web would make it difficult or even impossible for millions of Canadians to perform a multitude of everyday tasks.

![Person using Google maps on their phone to look up coordinates.](invisible-barrier-maps-unsplash-(cc0).png)

When doing a small accessibility audit of the LINCS project’s home page, I discovered that the first heading level (H1) was being used all over the page. While it looked just fine to me, I came to realize just how important it is to have a logical descent of heading levels when navigating a webpage using a screen reader. I also discovered how important “alt text” is for those who cannot see the images—and I subsequently spent a lot of time adding alt text to photos on the LINCS site.

Although some accessibility issues are obvious, a lot go unnoticed. Barriers are often invisible to those with good vision, hearing, or dexterity. Fortunately, accessibility testing tools and software made for those with disabilities make it possible for designers to experience their own websites from the perspective of their users. My first action when auditing the LINCS website was to enable Windows’ screen reader and attempt to navigate with my eyes closed; it was a lot harder than you’d think! A large part of this difficulty was due to the overuse of H1 headings.

The following video demonstrates how a popular screen reading application, JAWS, announces the information on a well structured webpage. Notice how JAWS relies heavily on heading levels to convey importance and to group relevant information together.

<YoutubeEmbed id="auUwAuJ8P-I" />

Accessibility improvements make it possible for more users to access content or use tools, while simultaneously improving the experience of all users. Consider, for example, accessibility features for audio. More than 50% of Americans either use closed captioning some or all of the time [according to CBS](https://www.cbsnews.com/news/subtitles-why-most-people-turn-tv-captions-on), and this number is as high as 80% for those in Generation Z. As CBS explains, popular streaming services such as Netflix originally implemented captions for all of their content because of the advocacy of the National Association of the Deaf in 2010. Captions have since become an essential part of all streaming because they are useful to all: if you’re in a loud environment, have no headphones in a library, want to watch something made in another language, and so on. User retention has actually been seen to increase when closed captions are present, with a [joint study by Verizon and Publicis Media](https://www.3playmedia.com/blog/verizon-media-and-publicis-media-find-viewers-want-captions/) providing the following statistics after conducting a survey on U.S. consumers of videos and advertisement:

- 80% of viewers are more likely to finish the video if it has captions
- 69% view videos with sound disabled
- 50% state captions are important, commonly citing reasons such as being in a quiet place, no access to headphones, or for multitasking
- 37% of viewers were inclined to turn on sound if captions were available

Another example of accessibility benefiting everyone are the hands-free capabilities that people often use in the car or while busy with other tasks such as cooking or cleaning. Audiobooks, originally meant for those with sight issues, have boomed in popularity; the [Audio Publishers Association’s annual sales survey](https://www.publishersweekly.com/pw/by-topic/industry-news/audio-books/article/92444-the-audiobook-market-and-revenue-keeps-growing.html) revealed that sales figures have had double-digit growth for **11 years straight.**

This all proves the point that **accessible design is simply good design**. It is for that exact reason I have been so thankful for my first experience in the world of web development being focused on accessibility, as it is an essential foundation to have and has no doubt molded me into an inclusive and more effective web developer.

## What can you do to help?

Even if you aren’t a web developer or product designer, you can help to make the web more accessible by advocating for accessibility, making your voice heard, and asking for things such as closed captioning or transcripts. When you catch something that doesn’t feel accessible—for example, some hard to read text on a poster—provide feedback!

If you use social media such as Instagram, LinkedIn, Facebook or X, be sure to include alt text, a descriptive blurb explaining the contents of the photo being posted. For helpful tips on alt text, see [Tug’s Guide to Alt Text on Social Media](https://www.tugagency.com/tug-life/tug-blog/2021/09/15/social-media-accessibility-a-guide-to-alt-text-on-social-media/).

Also be sure to capitalize each new word in a hashtag! Screen readers have difficulty reading out one squished together sentence. For example, #ILoveAccessibleWebsites will read exactly how you intend, but #Iloveaccessiblewebsites might be attempted to be read as a single weird and very long word.

To learn even more about web accessibility, check out [this introduction](https://www.w3.org/WAI/fundamentals/accessibility-intro/) by W3.org.

Advocating for accessibility, providing feedback, and using inclusive practices on social media are simple yet impactful ways everyone can contribute. I look forward to making good, accessible designs in my career. Let’s all commit to making the web a place where no one is left behind.
