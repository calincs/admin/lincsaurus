import React from "react";
import toolData from "../tools.js";
import FunctionButton from "../buttons/ToolFunctionButton";
import ExpertiseButton from "../buttons/ToolExpertiseButton";
import CreatorButton from "../buttons/ToolCreatorButton.js";

const FunctionButtons = ({ functions, className }) => {
  return functions
    .sort((a, b) => a.localeCompare(b))
    .map((f) => (
      <li className="button-tag" key={f}>
        <FunctionButton buttonName={f} className={className} />
      </li>
    ));
};
const ExpertiseButtons = ({ levels, className }) => {
  const sortOrder = ["Beginner", "Intermediate", "Expert"];
  const sortedLevels = levels.sort(
    (a, b) => sortOrder.indexOf(a) - sortOrder.indexOf(b),
  );
  return sortedLevels.map((f) => (
    <li className="button-tag" key={f}>
      <ExpertiseButton buttonName={f} className={className} />
    </li>
  ));
};

const CreatorButtons = ({ creator, className }) => {
  if (!creator) return null;

  return (
    <li className="button-tag" key="creator">
      <CreatorButton creator={creator} className={className} />
    </li>
  );
};
const ToolButtons = ({ toolName }) => {
  const tool = toolData.find((t) => t["Tool Name"] === toolName);
  return (
    <div className="tool-button-row">
      <ul className="padding--none">
        <FunctionButtons functions={tool.Functions} />
        <ExpertiseButtons levels={tool.Level} />
        <CreatorButtons creator={tool.Creator} />
      </ul>
    </div>
  );
};
export { CreatorButtons, ToolButtons, ExpertiseButtons, FunctionButtons };
