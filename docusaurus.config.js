/** @format */

// @ts-check
// Note: type annotations allow type checking and IDEs autocompletion

// const tablePlugin = require('remark-grid-tables')
const { themes } = require("prism-react-renderer");
const lightCodeTheme = themes.github;
const darkCodeTheme = themes.dracula;
import glossyDirective from "./src/plugins/glossary/directive/index.js";

require("dotenv").config();

const TwitterSvg =
  '<svg alt="Twitter Logo" style="fill: #FFFFFF;" width="40" height="32.49" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M459.37 151.716c.325 4.548.325 9.097.325 13.645 0 138.72-105.583 298.558-298.558 298.558-59.452 0-114.68-17.219-161.137-47.106 8.447.974 16.568 1.299 25.34 1.299 49.055 0 94.213-16.568 130.274-44.832-46.132-.975-84.792-31.188-98.112-72.772 6.498.974 12.995 1.624 19.818 1.624 9.421 0 18.843-1.3 27.614-3.573-48.081-9.747-84.143-51.98-84.143-102.985v-1.299c13.969 7.797 30.214 12.67 47.431 13.319-28.264-18.843-46.781-51.005-46.781-87.391 0-19.492 5.197-37.36 14.294-52.954 51.655 63.675 129.3 105.258 216.365 109.807-1.624-7.797-2.599-15.918-2.599-24.04 0-57.828 46.782-104.934 104.934-104.934 30.213 0 57.502 12.67 76.67 33.137 23.715-4.548 46.456-13.32 66.599-25.34-7.798 24.366-24.366 44.833-46.132 57.827 21.117-2.273 41.584-8.122 60.426-16.243-14.292 20.791-32.161 39.308-52.628 54.253z"></path></svg>';

const YoutubeSvg =
  '<svg alt="Youtube Logo" style="fill: #FFFFFF;" width="40" height="32.49" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M549.655 124.083c-6.281-23.65-24.787-42.276-48.284-48.597C458.781 64 288 64 288 64S117.22 64 74.629 75.486c-23.497 6.322-42.003 24.947-48.284 48.597-11.412 42.867-11.412 132.305-11.412 132.305s0 89.438 11.412 132.305c6.281 23.65 24.787 41.5 48.284 47.821C117.22 448 288 448 288 448s170.78 0 213.371-11.486c23.497-6.321 42.003-24.171 48.284-47.821 11.412-42.867 11.412-132.305 11.412-132.305s0-89.438-11.412-132.305zm-317.51 213.508V175.185l142.739 81.205-142.739 81.201z"></path></svg>';

const GitLabSvg =
  '<svg alt="GitLab Logo" style="fill: #FFFFFF;" width="40" height="32.49" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M503.5 204.6L502.8 202.8L433.1 21.02C431.7 17.45 429.2 14.43 425.9 12.38C423.5 10.83 420.8 9.865 417.9 9.57C415 9.275 412.2 9.653 409.5 10.68C406.8 11.7 404.4 13.34 402.4 15.46C400.5 17.58 399.1 20.13 398.3 22.9L351.3 166.9H160.8L113.7 22.9C112.9 20.13 111.5 17.59 109.6 15.47C107.6 13.35 105.2 11.72 102.5 10.7C99.86 9.675 96.98 9.295 94.12 9.587C91.26 9.878 88.51 10.83 86.08 12.38C82.84 14.43 80.33 17.45 78.92 21.02L9.267 202.8L8.543 204.6C-1.484 230.8-2.72 259.6 5.023 286.6C12.77 313.5 29.07 337.3 51.47 354.2L51.74 354.4L52.33 354.8L158.3 434.3L210.9 474L242.9 498.2C246.6 500.1 251.2 502.5 255.9 502.5C260.6 502.5 265.2 500.1 268.9 498.2L300.9 474L353.5 434.3L460.2 354.4L460.5 354.1C482.9 337.2 499.2 313.5 506.1 286.6C514.7 259.6 513.5 230.8 503.5 204.6z"></path></svg>';

/** @type {import('@docusaurus/types').Config} */
const config = {
  title: "LINCS",
  tagline:
    "LINCS provides the tools and infrastructure to make humanities data more discoverable, searchable, and shareable. Discover how you can explore, create, and publish cultural data.",
  url: "https://lincsproject.ca",
  baseUrl: "/",
  onBrokenLinks: "throw",
  onBrokenMarkdownLinks: "warn",
  favicon: "img/favicon-(c-LINCS).ico",
  organizationName: "LINCS", // Usually your GitHub org/user name.
  projectName: "LINCS Portal", // Usually your repo name.

  customFields: {
    LightGalleryLicense: process.env.REACT_APP_LIGHTGALLERY_LICENSE_KEY,
    EmailJSPK: process.env.REACT_APP_EMAILJS_KEY,
  },

  i18n: {
    defaultLocale: "en",
    locales: ["en", "fr"],
    localeConfigs: {
      en: {
        label: "English",
      },
      fr: {
        label: "Français",
      },
    },
  },
  markdown: {
    mdx1Compat: {
      comments: false,
      admonitions: false,
      headingIds: false,
    },
  },
  plugins: [
    // "@docusaurus-terminology/parser",
    /*[
      "@docusaurus-terminology/parser",
      {
        glossaryFilepath: "./docs/learn-lod/glossary.md",
        termOfTheDayFilePath: "./src/json-generator/term.json",
        termsDir: "./docs/terms",
      },
    ],*/
    // [
    //   require.resolve("./src/plugins/terminology-parser-v2"),
    //   {
    //     termOfTheDayFilePath: "./src/plugins/terminology-parser-v2/term.json",
    //     frenchTermOfTheDayFilePath: "./src/plugins/terminology-parser-v2/frenchTerm.json",
    //   },
    // ],
    // path.resolve(__dirname, "./src/plugins/glossary"),
    [
      require.resolve("./src/plugins/glossary"),
      {
        id: "glossary-plugin",
        docsPluginId: "classic",
        glossaryFilePath: "/docs/learn-lod/glossary.md",
        termsDir: "/docs/terms",
        customCss: "./src/plugins/glossary/index.css",
      },
    ],
    // [
    //   "@docusaurus/plugin-ideal-image",
    //   {
    //     quality: 70,
    //     max: 1030, // max resized image's size.
    //     min: 640, // min resized image's size. if original is lower, use that size.
    //     steps: 3, // the max number of images generated between min and max (inclusive)
    //     disableInDev: false,
    //   },
    // ],
    [
      "@docusaurus/plugin-client-redirects",
      {
        redirects: [
          {
            from: "/docs/create-lod/reconcile-entities/reconcile-practice",
            to: "/docs/create-lod/match-entities/entity-matching-practice",
          },
          {
            from: "/docs/create-lod/reconcile-entities/reconcile-guide",
            to: "/docs/create-lod/match-entities/entity-matching-guide",
          },
          {
            from: "/docs/create-lod/reconcile-entities/",
            to: "/docs/create-lod/match-entities/",
          },
          {
            from: "/docs/terms/reconciliation",
            to: "/docs/terms/entity-matching",
          },
          {
            from: "/docs/terms/conversion",
            to: "/docs/terms/transformation",
          },
          {
            from: "/docs/create-lod/revise-your-lod/convert-more-data",
            to: "/docs/create-lod/revise-your-lod/transform-more-data",
          },
          {
            from: "/docs/about-lincs/get-involved/grants-menu",
            to: "/docs/about-lincs/grants-menu",
          },
          {
            from: "/docs/create-data/publish-data/publish-workflows/keywords-instructions",
            to: "/docs/create-lod/prepare-metadata#keywords-requirements",
          },
          {
            from: "/docs/create-data/publish-data",
            to: "/docs/create-lod/get-started",
          },
          {
            from: "/docs/create-data/publish-data/publish-clean/cleaning-guide",
            to: "/docs/create-lod/clean-data/data-cleaning-guide",
          },
          {
            from: ["/research", "/areas-of-inquiry"],
            to: "/docs/about-lincs/research",
          },
          {
            from: "/contributors",
            to: "/docs/about-lincs/people/collaborators",
          },
          {
            from: "/team",
            to: "/docs/about-lincs/people/team",
          },
          {
            from: "/events/lincs-conference-2021",
            to: "/docs/about-lincs/get-involved/events/2021-05-conference",
          },
          {
            from: "/events/making-links-2023",
            to: "/docs/about-lincs/get-involved/events/2023-05-conference",
          },
          {
            from: "/2023-conference-schedule",
            to: "/docs/about-lincs/get-involved/events/2023-05-conference",
          },
          {
            from: "/newsletters",
            to: "/docs/about-lincs/get-involved/newsletter",
          },
          {
            from: "/research-participants-needed",
            to: "/docs/about-lincs/get-involved/participate",
          },
          {
            from: "/marvels-of-api-communication-the-diffbot-nlp-vetting-tool",
            to: "/blog/marvels-of-api",
          },
          {
            from: "/development-in-steps-learning-to-collaborate-on-a-technical-project",
            to: "/blog/development-in-steps",
          },
          {
            from: "/researching-and-designing-for-actionability",
            to: "/blog/researching-and-designing",
          },
          {
            from: "/piecing-the-past-together-with-lod",
            to: "/blog/piecing-the-past",
          },
          {
            from: "/reconciliation-richard-of-cornwall-and-using-historical-sources",
            to: "/blog/reconciliation-richard",
          },
          {
            from: "/jessica-and-goliath-learning-3m-and-cidoc-crm",
            to: "/blog/jessica-goliath",
          },
          {
            from: "/expression-and-suppression-orlando-stories-and-censorship",
            to: "/blog/expression-suppression",
          },
          {
            from: "/the-ship-of-theseus-representing-nuance-in-humanities-data",
            to: "/blog/ship-of-theseus",
          },
          {
            from: "/the-good-enough-metadata-specialist",
            to: "/blog/metadata-specialist",
          },
          {
            from: "/speaking-in-different-languages-working-across-groups-and-disciplines",
            to: "/blog/speaking-in-different-languages",
          },
          {
            from: "/two-roads-diverged",
            to: "/blog/tube-map-diverged",
          },
          {
            from: "/digging-into-dh",
            to: "/blog/digging-into-dh",
          },
          {
            from: "/diving-into-large-scale-projects-as-a-complete-novice",
            to: "/blog/large-scale-projects",
          },
          {
            from: "/blog/transform-load",
            to: "/blog/extract-transform-load",
          },
          {
            from: "/what-is-extract-transform-load",
            to: "/blog/extract-transform-load",
          },
          {
            from: "/searching-for-harmony-questions-in-ontologies-for-music-data",
            to: "/blog/music-ontologies",
          },
          {
            from: "/connections-are-important-making-links-in-oral-history-using-linked-open-data",
            to: "/blog/oral-history-LOD",
          },
          {
            from: "/breaking-down-the-barriers-to-data-conversion",
            to: "/blog/breaking-down-barriers",
          },
          {
            from: "/design-frolics-and-demystifying-user-interface-design",
            to: "/blog/design-frolics",
          },
          {
            from: "/the-user-centric-approach-to-problem-solving",
            to: "/blog/user-problem-solving",
          },
        ],
      },
    ],
  ],
  scripts: [
    {
      src: "/matomo.js",
      async: "true",
    },
    {
      src: "https://kit.fontawesome.com/2d8ee6f374.js",
      crossorigin: "anonymous",
    },
  ],
  presets: [
    [
      "classic",
      /** @type {import('@docusaurus/preset-classic').Options} */
      ({
        docs: {
          sidebarPath: require.resolve("./sidebars.js"),
          editUrl: "https://gitlab.com/calincs/admin/docusaurus/-/tree/main/",
          // // remarkPlugins: [tablePlugin],
          remarkPlugins: [
            [
              glossyDirective,
              {
                termsDir: "/docs/terms",
                glossaryJSONPaths: {
                  en: "./static/glossary-en.json",
                  fr: "./static/glossary-fr.json",
                },
              },
            ],
          ],
        },
        blog: {
          showReadingTime: true,
          blogSidebarCount: "ALL",
          blogSidebarTitle: "All our posts",
          include: ["**/*.{md,mdx}"],
          editUrl: "https://gitlab.com/calincs/admin/docusaurus/-/tree/main/",
          remarkPlugins: [
            [
              glossyDirective,
              {
                termsDir: "/docs/terms",
                glossaryJSONPaths: {
                  en: "./static/glossary-en.json",
                  fr: "./static/glossary-fr.json",
                },
              },
            ],
          ],
        },
        theme: {
          customCss: require.resolve("./src/css/custom.css"),
        },
      }),
    ],
  ],
  themes: ["docusaurus-theme-search-typesense"],
  themeConfig:
    /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
    {
      announcementBar: {
        id: "Office-hours",
        content:
          'LINCS has office hours! <a href="mailto:lincs@uoguelph.ca?subject=Office%20Hours">Email us for the details!</a>',
        backgroundColor: "#fafbfc",
        textColor: "#091E42",
        isCloseable: true,
      },
      typesense: {
        typesenseCollectionName: "lincsaurus",
        typesenseServerConfig: {
          nodes: [
            {
              host: process.env.REACT_APP_HOST,
              port: process.env.REACT_APP_PORT,
              protocol: process.env.REACT_APP_PROTOCOL,
            },
          ],
          apiKey: process.env.REACT_APP_API_KEY,
        },
        // Optional: Typesense search parameters: https://typesense.org/docs/0.21.0/api/documents.md#search-parameters
        typesenseSearchParameters: {},
        // Optional
        contextualSearch: true,
      },
      tableOfContents: {
        minHeadingLevel: 2,
        maxHeadingLevel: 5,
      },
      docs: {
        sidebar: {
          hideable: true,
          autoCollapseCategories: true,
        },
      },
      colorMode: {
        defaultMode: "light",
        disableSwitch: true,
      },
      navbar: {
        title: "LINCS Portal",
        logo: {
          alt: "LINCS Logo",
          src: "img/lincs-logo-(c-LINCS).png",
          style: { height: "32px" },
        },
        items: [
          {
            type: "docSidebar",
            position: "left",
            sidebarId: "aboutLincsSidebar",
            label: "About LINCS",
            className: "text--truncate navbar-item",
          },
          {
            type: "docSidebar",
            position: "left",
            sidebarId: "learnLODSidebar",
            label: "Learn LOD",
            className: "text--truncate navbar-item",
          },
          {
            type: "docSidebar",
            position: "left",
            sidebarId: "exploreDataSidebar",
            label: "Explore LOD",
            className: "text--truncate navbar-item",
          },
          {
            type: "doc",
            position: "left",
            docId: "create-lod/create-lod",
            label: "Create LOD",
            className: "text--truncate navbar-item",
          },
          {
            type: "docSidebar",
            position: "left",
            sidebarId: "toolsSidebar",
            label: "Tools",
            className: "navbar-item",
          },
          {
            type: "dropdown",
            label: "Quick Links",
            position: "right",
            items: [
              {
                label: "ResearchSpace",
                href: "https://rs.lincsproject.ca",
              },
              {
                label: "ResearchSpace Review",
                href: "https://rs-review.lincsproject.ca/",
              },
              {
                label: "My LINCS Account",
                href: "https://keycloak.lincsproject.ca/realms/lincs/account/",
              },
            ],
          },
          {
            to: "/blog",
            label: "Blog",
            position: "right",
            className: "navbar-item",
          },
          {
            type: "localeDropdown",
            position: "right",
            className: "navbar-item",
          },
        ],
      },
      footer: {
        style: "dark",
        links: [
          {
            title: "Contact LINCS",
            items: [
              {
                label: "Form",
                href: "/docs/about-lincs/get-involved/contact-us",
              },
              {
                label: "Email",
                href: "mailto://lincs@uoguelph.ca",
              },
            ],
          },
          {
            title: "Community",
            items: [
              {
                label: "Sign up for the Newsletter",
                href: "http://eepurl.com/gZ0rzb",
              },
              {
                label: "Newsletter",
                href: "/docs/about-lincs/get-involved/newsletter",
              },
              {
                html: `
                <div class="svg-icons">
                  <a href="https://twitter.com/lincsproject" title="Twitter" target="_blank">${TwitterSvg}</Link>
                  <a href="https://www.youtube.com/channel/UCuLMD1dLD6_AAMeXlyZgzQQ" title="Youtube" target="_blank">${YoutubeSvg}</a>
                  <a href="https://gitlab.com/calincs" title="GitLab" target="_blank">${GitLabSvg}</a>
                </div>
              `,
              },
            ],
          },
          {
            title: "Policies",
            items: [
              {
                label: "Privacy Policy",
                href: "/docs/about-lincs/terms-and-conditions/privacy",
              },
              {
                label: "Terms and Conditions",
                href: "/docs/about-lincs/terms-and-conditions",
              },
            ],
          },
          {
            title: "Partners and Funding",
            items: [
              {
                html: `
                <div class="cfi-logo">
                  <a href="https://www.innovation.ca/" target="_blank" rel="noreferrer noopener" aria-label="CFI">
                    <img src="/img/logos/cfi-logo-new-(c-owner).svg" alt="Canada Foundation for Innovation (CFI) Logo" />
                  </a>
                </div>  
              `,
              },
              {
                label: "Funding Partners",
                href: "/docs/about-lincs/people/funding-partners",
              },
              {
                label: "Infrastructure Partners",
                href: "/docs/about-lincs/people/infrastructure-partners",
              },
            ],
          },
        ],
        copyright: `                <div class="land-acknowledgement">
                  <p>While members of LINCS gather together on-line from all across North America and beyond, our servers, which are also on Indigenous territories, produce the worlds we create and meet in. In particular we want to acknowledge that our servers are situated on the traditional lands of the WSÁNEĆ (Saanich), Lkwungen (Songhees), and Wyomilth (Esquimalt) peoples.</p>
                </div><a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons License" style="border-width:0;margin-right:2px;height:calc(1em - 4px);" src="https://i.creativecommons.org/l/by/4.0/80x15.png" /></a>This work is licensed under a <a rel="license" class="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.`,
      },
      prism: {
        theme: lightCodeTheme,
        darkTheme: darkCodeTheme,
        additionalLanguages: ["turtle", "sparql", "python", "cypher"],
      },
    },
};

module.exports = config;
