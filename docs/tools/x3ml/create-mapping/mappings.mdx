---
sidebar_position: 3
title: "Mappings" 
description: "How to map subjects, names, events, and places"
---

Ensure you have read the [3M Basics tab](/docs/tools/x3ml/create-mapping/3m-basics) of this walkthrough, in particular the section [Understanding the Data](/docs/tools/x3ml/create-mapping/3m-basics#understanding-the-data), before continuing.

It might also be useful to read [Interface of a Mapping Project](/docs/tools/x3ml/create-mapping/export-extras#interface-of-a-mapping-project) to understand the 3M interface as components of the interface will be referenced throughout the rest of this walkthrough.

The mappings portion of this walkthrough is broken down into the following sections:

1. [Mapping the Subject](#mapping-the-subject)
2. [Mapping Names](#mapping-names)
3. [Mapping Events](#mapping-events)
4. [Mapping Event Places](#mapping-event-places)
5. [Mapping Event Dates](#mapping-event-dates)
{/* Add Mapping Conditions and Membership in the future*/}

## Mapping the Subject

1. Select the mapping project you just made.

| ![alt=""](/img/documentation/x3ml-subject-1-(c-LINCS).png)|
|:--:|
| The **Project Mappings** page with the project **Yellow Nineties Sample Project** selected. |

2. Click **Open**. This will open your project. By default, whenever you open a project it will display the project's **Files Metadata** window.

| ![alt=""](/img/documentation/x3ml-subject-2-(c-LINCS).png)|
|:--:|
| The **Project Mappings** page with the **Open** button indicated. |

| ![alt=""](/img/documentation/x3ml-subject-3-(c-LINCS).png)|
|:--:|
| The **Files Metadata** window for the mapping project **Yellow Nineties Sample Project**. |

3. Close the **Files Metadata** window.

| ![alt=""](/img/documentation/x3ml-subject-4-(c-LINCS).png)|
|:--:|
| The **Files Metadata** window with the **Save & Close** button indicated. |

| ![alt=""](/img/documentation/x3ml-subject-5-(c-LINCS).png)|
|:--:|
| The mapping project **Yellow Nineties Sample Project**. |

4. Click the checkbox next to **Expand all mappings**. This will expand **Mapping #1**.

:::tip
You can also expand <UnderConstruction>each mapping</UnderConstruction> individually if you have <UnderConstruction>more than one mapping</UnderConstruction> in your mapping project. However, this will decrease searchability for that page. Mappings that are not expanded will not show up when the page is searched. This might hinder your work if you are looking for repeated use of a class, property, word, etc. Remember to **Expand all mappings** if you will be using the page search function (Ctrl + F).
:::

| ![alt=""](/img/documentation/x3ml-subject-6-(c-LINCS).png)|
|:--:|
| The mapping project **Yellow Nineties Sample Project** with **Expand all mappings** indicated. |

| ![alt=""](/img/documentation/x3ml-subject-7-(c-LINCS).png)|
|:--:|
| **Mapping #1** expanded. |

5. Click on the **Domain** to expand it.

| ![alt=""](/img/documentation/x3ml-subject-8-(c-LINCS).png)|
|:--:|
| **Mapping #1** with its **Domain** indicated. |

| ![alt=""](/img/documentation/x3ml-subject-9-(c-LINCS).png)|
|:--:|
| **Domain** expanded. |

6. Click on **Source Node** and select `/rdf:RDF/rdf:Description`.

| ![alt=""](/img/documentation/x3ml-subject-10-(c-LINCS).png)|
|:--:|
| **Source Node** drop-down menu with `/rdf:RDF/rdf:Description` selected. |

7. Click on **Target Entity** and select `E21_Person`. |

:::tip
You can search through the options by typing in the start of the option you are looking for. This applies to both the **Source Node** drop-down menu and the **Target Entity** dropdown menu.
:::

| ![alt=""](/img/documentation/x3ml-subject-11-(c-LINCS).png)|
|:--:|
| **Target Entity** drop-down menu with `E21_Person` selected. |

| ![alt=""](/img/documentation/x3ml-subject-12-(c-LINCS).png)|
|:--:|
| **Domain** with **Source Node** and **Target Entity** filled in. |

:::note
This means every instance of `/rdf:RDF/rdf:Description` in the source data will result in the creation of a `E21_Person` entity in the final Turtle file.
:::

8. Click on **Generator**. This will open the **Generator** window so you can add generators to the **Target Entity**.

| ![alt=""](/img/documentation/x3ml-subject-13-(c-LINCS).png)|
|:--:|
| **Target Entity** with the **Generator** button indicated. |

| ![alt=""](/img/documentation/x3ml-subject-14-(c-LINCS).png)|
|:--:|
| The **Generator** window. |

6. Select the instance generator **UriExistingOrNew**.

| ![alt=""](/img/documentation/x3ml-subject-15-(c-LINCS).png)|
|:--:|
| The **Generator** window with **UriExistingOrNew** selected. |

Recall that the source data looks like this:
```xml
<rdf:Description rdf:about="https://personography.1890s.ca/persons/yeats-w-b/">
<rdf:Description rdf:about="https://personography.1890s.ca/persons/smith-pamela-colman/">
<rdf:Description rdf:about="https://personography.1890s.ca/persons/yeast-john-butler/">
```

The goal of using **UriExistingOrNew** is to grab the URI that is stored in the attribute `rdf:about`. [Xpath](/docs/tools/x3ml/create-mapping/export-extras#xpath-cheatsheet) is used to select it. This way we can declare that each instance of the Yellow Nineties URI is an instance of CIDOC CRM `E21_Person`.

7. Fill out the **Instance Declaration** panel like this:

Argument #1: uri

Type: `Xpath`

Value: `@rdf:about`

...

Argument #2: text1

Type: `Constant`

Value: `person_uri`

...

Argument #3: uri_separator1
  
Type: `Constant`

Value: `/`

| ![alt=""](/img/documentation/x3ml-subject-16-(c-LINCS).png)|
|:--:|
| The **Instance Declaration** panel with all the fields filled out. |

:::tip
For **Argument #2: text1**, the value does not appear in the final Turtle file. Use it for your own reference. You can write a value that is different from this walkthrough's.
:::

8. Click **Save Instance Generator**.

| ![alt=""](/img/documentation/x3ml-subject-17-(c-LINCS).png)|
|:--:|
| The **Generator** window with **Save Instance Generator** indicated. |

9. Click the **Label** tab.

| ![alt=""](/img/documentation/x3ml-subject-18-(c-LINCS).png)|
|:--:|
| The **Generator** window with the **Label** tab indicated. |

| ![alt=""](/img/documentation/x3ml-subject-19-(c-LINCS).png)|
|:--:|
| The **Label** tab. |

10. Click on **Delcare new Label Generator** (plus button).

| ![alt=""](/img/documentation/x3ml-subject-20-(c-LINCS).png)|
|:--:|
| The **Label** tab with **Delcare new Label Generator** (plus button) indicated. |

| ![alt=""](/img/documentation/x3ml-subject-21-(c-LINCS).png)|
|:--:|
| The label generator definitions. |

11. Select **Literal**.

:::note
The instance generator picked out the URI for each person, but without a label the data is not human-readable and cannot be easily explored. Using the **Literal** label generator will allow you to create a rdfs:label for the URI.
:::

| ![alt=""](/img/documentation/x3ml-subject-22-(c-LINCS).png)|
|:--:|
| The **Generator** window with the **Literal** generator selected. |

12. Fill out the **Label Declaration** panel like this:

Argument #1: text

Type: `Xpath`

Value: `y90s_name/text()`

...

Argument #2: language

Type: `Constant`

Value: `en`

:::note
It's best practice in linked data for labels to have a language tag and thus **Argument #2: language** should always be filled in except in rare cases. For example, if the label is an integer then it would not need a language tag. In this case, all of the source data is in English so every rdfs:label should have the `@en` language tag. Other datasets might have labels in multiple languages and would thus require different laguage tags.
:::

| ![alt=""](/img/documentation/x3ml-subject-23-(c-LINCS).png)|
|:--:|
| The **Instance Declaration** panel with all the fields filled out. |

13. Click **Save Label Generator**.

| ![alt=""](/img/documentation/x3ml-subject-24-(c-LINCS).png)|
|:--:|
| The **Generator** window with **Save Instance Generator** indicated. |

| ![alt=""](/img/documentation/x3ml-subject-25-(c-LINCS).png)|
|:--:|
| The **Generator** window with **Label Generator Definitions**. |

14. Close the **Generator** window.

| ![alt=""](/img/documentation/x3ml-subject-26-(c-LINCS).png)|
|:--:|
| The **Generator** window with **Close** indicated. |

Now the `E21_Person` has a URI and a rdf:label. You have mapped the subject of this source data.

| ![alt=""](/img/documentation/x3ml-subject-27-(c-LINCS).png)|
|:--:|
| The **Domain** with generators on its **Target Entity**. |

### Check Your Work

To confirm you've done all the steps correctly, you can quickly produce and review some RDF triples to ensure that the result is what you expect.

1. Click on the arrow beside **Produce RDF**. This will open a menu of options.

| ![alt=""](/img/documentation/x3ml-subject-28-(c-LINCS).png)|
|:--:|
| The arrow beside **Produce RDF** is indicated. |

| ![alt=""](/img/documentation/x3ml-subject-29-(c-LINCS).png)|
|:--:|
| The drop-down menu for **Produce RDF** is indicated. |

2. Select **Produce Turtle**.

| ![alt=""](/img/documentation/x3ml-subject-30-(c-LINCS).png)|
|:--:|
| The option **Produce Turtle** is indicated. |

| ![alt=""](/img/documentation/x3ml-subject-31-(c-LINCS).png)|
|:--:|
| The option **Produce RDF** button has changed to **Produce Turtle**. |

3. Click the **Produce Turtle** button. This will open the **Transformed Output** window.

| ![alt=""](/img/documentation/x3ml-subject-32-(c-LINCS).png)|
|:--:|
| The **Produce Turtle** button is indicated. |

| ![alt=""](/img/documentation/x3ml-subject-33-(c-LINCS).png)|
|:--:|
| The **Transformed Output** window. |

4. Double check that the contents of your Turtle file match the contents below:

```turtle
<https://personography.1890s.ca/persons/smith-pamela-colman/>
        a           crm:E21_Person ;
        rdfs:label  "Smith, Pamela Colman"@en .

<https://personography.1890s.ca/persons/yeast-john-butler/>
        a           crm:E21_Person ;
        rdfs:label  "Yeats, John Butler"@en .

<https://personography.1890s.ca/persons/yeats-w-b/>
        a           crm:E21_Person ;
        rdfs:label  "Yeats, W. B."@en .
```

If they don't match, retrace your steps and see where you might have strayed from the walkthrough.

If they match then congratulations! You have successfully mapped the subject. Now that you are more familiar with the 3M interface, the rest of this walkthrough will be more succinct and only feature key steps or new concepts with the assumption you will refer back to this [Mapping the Subject](#mapping-the-subject) section if you get stuck.

## Mapping Names

Recall that the source XML file has the following data related to names:

```xml
 <rdf:Description rdf:about="https://personography.1890s.ca/persons/yeats-w-b/">
    <y90s_name>Yeats, W. B.</y90s_name>
    <y90s_birth_name>Yeats, William Butler</y90s_birth_name>
  </rdf:Description>
  <rdf:Description rdf:about="https://personography.1890s.ca/persons/smith-pamela-colman/">
    <y90s_name>Smith, Pamela Colman</y90s_name>
    <y90s_aka>P. C. S.</y90s_aka>
  </rdf:Description>
  <rdf:Description rdf:about="https://personography.1890s.ca/persons/yeast-john-butler/">
    <y90s_name>Yeats, John Butler</y90s_name>
  </rdf:Description>
```

There are three types of names: name they used, name they were given at birth, and name they are also known as.

{/*TODO: Update these vocab URLs after new vocabularies have been integrated into data. */}
By working with the Yellow Nineties researchers, these names have been typed as: personal name (http://id.lincsproject.ca/cwrc/personalName), birth name (http://id.lincsproject.ca/cwrc/birthname), and additional name (http://id.lincsproject.ca/cwrc/additionalName) using the CWRC vocabularly.

The mapping pattern for names when applied to this source data looks like this:

{/*TODO: ADD CAPTIONS TO IMAGES*/}

| ![alt=""](/img/documentation/x3ml-name-1-(c-LINCS).png)|
|:--:|

The goal of this section is to translate the informal mapping diagram above into machine-readable RDF triples with the relevant predicates / properties and entities / classes.

Let's start by mapping personal names.

1. **Add New Link**.

| ![alt=""](/img/documentation/x3ml-name-2-(c-LINCS).png)|
|:--:|

2. Under **Target Relation**, click **Add Intermediate**.

| ![alt=""](/img/documentation/x3ml-name-3-(c-LINCS).png)|
|:--:|

3. Select the following for each field:

Source Relation: `y90s_name`

1st Target Relation: `P1_is_identified_by`

1st Target Entity: `E33_E41_Linguistic_Appellation`

2nd Target Relation: `P190_has_symbolic_content`

2nd Target Entity: `Literal`

:::tip
Remember you can search for / prompt the options by typing a part of the option.
:::

| ![alt=""](/img/documentation/x3ml-name-4-(c-LINCS).png)|
|:--:|

This link ensures that every time there is a `<y90s_name>` element in the source data, then the predicate `P1_is_identified_by` and the object `E33_E41_Linguistic_Appellation` will be created. Attached to that `E33_E41_Linguistic_Appellation` is the predicate `P190_has_symbolic_content` which will then have an rdfs:literal that contains the actual content of the `E33_E41_Linguistic_Appellation`. It established the relationships required for you to map this section of the pattern:

| ![alt=""](/img/documentation/x3ml-name-5-(c-LINCS).png)|
|:--:|

3. Add the instance generator **ConcatMultipleTerms** to the 1st **Target Entity**.

| ![alt=""](/img/documentation/x3ml-name-6-(c-LINCS).png)|
|:--:|

4. Fill in the **Instance Declaration** panel like below and then **Save Instance Generator**.

Argument #1: prefix

Type: `Constant`

Value: `http://temp.lincsproject.ca/`

...

Argument #2: sameTermsDelim

Type: `Constant`

Value: `/`

...

Argument #3: diffTermsDelim

Type: `Constant`

Value: `/`

...

Argument #4: text1

Type: `Constant`

Value: `name`

...

Argument #4: text2

Type: `Xpath`

Value: `text()`

| ![alt=""](/img/documentation/x3ml-name-7-(c-LINCS).png)|
|:--:|

This makes a temporary LINCS URI for the `E33_E41_Linguistic_Appellation` that will be replaced by a proper minted LINCS URI in post-processing. The temp LINCS URI is constructed to be as unique or as generalized as it needs to be for that specific pattern. In this case, each instance of `E33_E41_Linguistic_Appellation` should be unique to each `E21_Person`. Hence why the value for `text2` refers to the value within the element `<y90s_name>`. The value for `text1` will appear in the Turtle file produced by 3M, but it won't appear in the data that is ingested into the linked data triplestore. Your value for `text1` can be different from this walkthrough's, it is for your reference only.

5. Add the label generator **name** after switching to the **Label** tab. Fill out the **Label Declaration** like below and **Save Label Generator**.

Argument #1: name

Type: `Xpath`

Value: `text()`

...

Argument #2: language

Type: `Constant`

Value: `en`

| ![alt=""](/img/documentation/x3ml-name-8-(c-LINCS).png)|
|:--:|

Notice how the pattern under **name** is `Name of {name}`. This means the label generator has the words "Name of" built into its pattern and the variable `{name}` will be filled with whatever value you put in for **Argument #1: name**. In this case, we want the rdfs:label to say "Name of `<y90s_name>`" for each `E33_E41_Linguistic_Appellation`, hence why the Xpath is `text()`. (The **Source Relation** is already `y90s_name` so we don't need to move to a different node.)

Now let's address the rdfs:literal which is the object for the `P190_has_symbolic_content` predicate.

| ![alt=""](/img/documentation/x3ml-name-9-(c-LINCS).png)|
|:--:|

6. Add the instance generator **Literal** to the **Target Entity** `Literal`. Fill out the **Instance Declaration** panel like below and **Save Instance Generator**.

Argument #1: text

Type: `Xpath`

Value: `text()`

...

Argument #2: language

Type: `Constant`

Value: `en`

| ![alt=""](/img/documentation/x3ml-name-10-(c-LINCS).png)|
|:--:|

| ![alt=""](/img/documentation/x3ml-name-11-(c-LINCS).png)|
|:--:|

:::note
There isn't a label generator because the <UnderConstruction link="/docs/terms/range" comment="Not sure if this supposed to link to the glossary term or documentation for rdfs:literal, if so replace">range</UnderConstruction> of `P190_has_symbolic_content` is already a rdfs:literal.
:::

7. Much like how you [checked your work](#check-your-work) for mapping the subject, **Produce Turtle** again to check that the resulting Turtle file has triples that matches the ones below:

```turtle
<https://personography.1890s.ca/persons/yeats-w-b/>
        a                        crm:E21_Person ;
        rdfs:label               "Yeats, W. B."@en ;
        crm:P1_is_identified_by  <http://temp.lincsproject.ca/name/Yeats, W. B.> .

<http://temp.lincsproject.ca/name/Yeats, W. B.>
        a                              crm:E33_E41_Linguistic_Appellation ;
        rdfs:label                     "Name of Yeats, W. B."@en ;
        crm:P190_has_symbolic_content  "Yeats, W. B."@en .
```

The above triples apply to W. B. Yeats. Ensure that Pamela Colman Smith and John Butler Yeats also have triples in this pattern.

| ![alt=""](/img/documentation/x3ml-name-12-(c-LINCS).png)|
|:--:|

| ![alt=""](/img/documentation/x3ml-name-13-(c-LINCS).png)|
|:--:|

Now we need to address the `E55_Type` for the names mapping pattern.

| ![alt=""](/img/documentation/x3ml-name-14-(c-LINCS).png)|
|:--:|

8. Click **Additional** to add a constant entity to `E33_E41_Linguistic_Appellation`. Select the following for this new constant entity:

Relation: `P2_has_type`

Entity: `E55_Type`

This means every time this `E33_E41_Linguistic_Appellation` appears, a `E55_Type` wil be attached to it.

| ![alt=""](/img/documentation/x3ml-name-15-(c-LINCS).png)|
|:--:|

Recall from the beginning of [Mapping Names](#mapping-names) that personal name has been typed as `http://id.lincsproject.ca/cwrc/personalName`so that is the URI we want to use for this `E55_Type`.

9. Add the instance generator `UriExistingOrNew` to this `E55_Type`. Fill out the **Instance Declaration** panel like below and **Save Instance Generator**.

Argument #1: uri

Type: `Constant`

Value: `http://id.lincsproject.ca/cwrc/personalName`

...

Argument #2: text1

Type: `Constant`

Value: `cwrc_personalName`

...

Argument #3: uri_separator1

Type: `Constant`

Value: `/`

| ![alt=""](/img/documentation/x3ml-name-16-(c-LINCS).png)|
|:--:|

| ![alt=""](/img/documentation/x3ml-name-17-(c-LINCS).png)|
|:--:|

10. Add the label generator **Literal** after switching to the **Label** tab. Fill out the **Label Declaration** like below and **Save Label Generator**.

Argument #1: text

Type: `Constant`

Value: `personal name`

...

Argument #2: language

Type: `Constant`

Value: `en`

| ![alt=""](/img/documentation/x3ml-name-18-(c-LINCS).png)|
|:--:|

11. Check your work again by clicking **Produce Turtle**. Your triples should look like this now for W. B. Yeats:

```turtle
<https://personography.1890s.ca/persons/yeats-w-b/>
        a                        crm:E21_Person ;
        rdfs:label               "Yeats, W. B."@en ;
        crm:P1_is_identified_by  <http://temp.lincsproject.ca/name/Yeats, W. B.> .

<http://temp.lincsproject.ca/name/Yeats, W. B.>
        a                              crm:E33_E41_Linguistic_Appellation ;
        rdfs:label                     "Name of Yeats, W. B."@en ;
        crm:P190_has_symbolic_content  "Yeats, W. B."@en ;
        crm:P2_has_type                <http://id.lincsproject.ca/cwrc/personalName> .

<http://id.lincsproject.ca/cwrc/personalName>
        a           crm:E55_Type ;
        rdfs:label  "personal name"@en .

```

Ensure there are also appropriate triples for the other two entities.

:::tip
There should only be one `<http://id.lincsproject.ca/cwrc/personalName>` entity. Each `E33_E41_Linguistic_Appellation` should refer to that URI / `E55_Type`. By referring to the same `E55_Type`, the data is then linked together and can be searched by this `E55_Type`.
:::

Recall from the beginning of [Mapping Names](#mapping-names) that there are two other name types, birth name (http://id.lincsproject.ca/cwrc/birthname), and additional name (http://id.lincsproject.ca/cwrc/additionalName). 

Mapping those types will be easier because you have the framework laid out. Let's start by mapping birth names.

12. Clone **Link #1** by clicking **Clone this link**. This will create a copy of **Link #1** called **Link #2**.

| ![alt=""](/img/documentation/x3ml-name-19-(c-LINCS).png)|
|:--:|

13. Change the **Source Relation** and **Source Node** of **Link #2** to `y90s_birth_name`.

| ![alt=""](/img/documentation/x3ml-name-20-(c-LINCS).png)|
|:--:|

14. Edit the instance generator and label generator for the `E55_Type` in **Link #2** so that it directs to `http://id.lincsproject.ca/cwrc/birthname`.

:::warning
DO NOT switch to new generator **Definitions**, only edit the **Declarations**.
:::

| ![alt=""](/img/documentation/x3ml-name-21-(c-LINCS).png)|
|:--:|

15. Edit the instance generator for `E33_E41_Linguistic_Appellation` like below and **Save Instance Generator**.

Argument #4: text1

Type: `Constant`

Value: `birth_name`

...

Argument #5: text2

Type: `Xpath`

Value: `../y90s_name/text()`

| ![alt=""](/img/documentation/x3ml-name-22-(c-LINCS).png)|
|:--:|

| ![alt=""](/img/documentation/x3ml-name-23-(c-LINCS).png)|
|:--:|

16. Click on **Open Generator Definitions Manager**.

| ![alt=""](/img/documentation/x3ml-name-24-(c-LINCS).png)|
|:--:|

17. Click **Add New Generator Definition**.

| ![alt=""](/img/documentation/x3ml-name-25-(c-LINCS).png)|
|:--:|

18. Switch the **Type** to `label` and fill in the rest like below and **Save**.

Name: `birth_name`

Pattern: `Birth name of {name}`

| ![alt=""](/img/documentation/x3ml-name-26-(c-LINCS).png)|
|:--:|

19. Switch the label generator for `E33_E41_Linguistic_Appellation` to the `birth_name` generator that you just made. Fill out the **Label Declaration** like below and **Save Label Generator**.

Argument #1: name

Type: `Xpath`

Value: `../y90s_name/text()`

...

Argument #2: language

Type: `Constant`

Value: `en`

| ![alt=""](/img/documentation/x3ml-name-27-(c-LINCS).png)|
|:--:|

| ![alt=""](/img/documentation/x3ml-name-28-(c-LINCS).png)|
|:--:|

20. Check your work by clicking **Produce Turtle**. The relevant triples for W. B. Yeats' birth name should look like this:

```turtle
<https://personography.1890s.ca/persons/yeats-w-b/>
        a                        crm:E21_Person ;
        rdfs:label               "Yeats, W. B."@en ;
        crm:P1_is_identified_by  <http://temp.lincsproject.ca/birth_name/Yeats, W. B.> , <http://temp.lincsproject.ca/name/Yeats, W. B.> .

<http://temp.lincsproject.ca/birth_name/Yeats, W. B.>
        a                              crm:E33_E41_Linguistic_Appellation ;
        rdfs:label                     "Birth name of Yeats, W. B."@en ;
        crm:P190_has_symbolic_content  "Yeats, William Butler"@en ;
        crm:P2_has_type                <http://id.lincsproject.ca/cwrc/birthname> .

<http://id.lincsproject.ca/cwrc/birthname>
        a           crm:E55_Type ;
        rdfs:label  "birth name"@en .
```

Out of the three entities, only W. B. Yeats has a birth name so his triples are the only ones you need to check.

21. Repeat Steps 12 - 20 for additional name (http://id.lincsproject.ca/cwrc/additionalName) which addresses the element `<y90s_aka>`. This should result in **Link #3** looking like this:

| ![alt=""](/img/documentation/x3ml-name-29-(c-LINCS).png)|
|:--:|

The relevant Turtle triples should look like this:

```turtle
<https://personography.1890s.ca/persons/smith-pamela-colman/>
        a                        crm:E21_Person ;
        rdfs:label               "Smith, Pamela Colman"@en ;
        crm:P1_is_identified_by  <http://temp.lincsproject.ca/additional_name/Smith, Pamela Colman> , <http://temp.lincsproject.ca/name/Smith, Pamela Colman> .

<http://temp.lincsproject.ca/additional_name/Smith, Pamela Colman>
        a                              crm:E33_E41_Linguistic_Appellation ;
        rdfs:label                     "Additional name of Smith, Pamela Colman"@en ;
        crm:P190_has_symbolic_content  "P. C. S."@en ;
        crm:P2_has_type                <http://id.lincsproject.ca/cwrc/additionalName> .

<http://id.lincsproject.ca/cwrc/additionalName>
        a           crm:E55_Type ;
        rdfs:label  "additional name"@en .
```

Out of the three entities, only Pamela Colman Smith has an additional name so her triples are the only ones you need to check.

Now you know how to map names, including different name types. Next you will learn how to map events.

## <UnderConstruction>Mapping Events</UnderConstruction>

## <UnderConstruction>Mapping Event Places</UnderConstruction>

## <UnderConstruction>Mapping Event Dates</UnderConstruction>


