---
id: care-principles-for-indigenous-data-governance
title: Principes de CARE pour la gouvernance des données autochtones
definition: Un ensemble de principes (bénéfice collectif, autorité de contrôle, responsabilité et éthique) pour faire progresser les droits collectifs et individuels en matière de données dans le mouvement des données ouvertes.
Machine Translation: True     
Translation Tool: DeepL 
Translated Added By: Humna Chaudhry(@humnq)
Last Translated: 2024-06-21
---

:::info
Cette page web a été traduite automatiquement par DeepL. Bien que nous nous efforcions d’être précis, nous vous informons que les traductions peuvent contenir des erreurs ou des inexactitudes. Pour obtenir les informations les plus précises, veuillez vous référer à la version originale.
:::

Les [Principes CARE pour la gouvernance des données autochtones](https://www.gida-global.org/s/dsj-1158_carroll.pdf) sont un ensemble de principes visant à promouvoir les droits collectifs et individuels en matière de données dans le contexte de la [Déclaration des Nations Unies sur les droits des peuples autochtones (UNDRIP)](https://www.un.org/development/desa/indigenouspeoples/wp-content/uploads/sites/19/2018/11/UNDRIP_E_web.pdf) et du mouvement des données ouvertes : bénéfice collectif, autorité de contrôle, responsabilité et éthique.

Les principes actuels en matière de données ouvertes, tels que :Term[FAIR Principles]{#fair-principles}, ignorent les différences de pouvoir et les contextes historiques. Les principes CARE ont été élaborés par l'International Indigenous Data Sovereignty Interest Group pour encourager les mouvements de données à garantir le respect de la gouvernance autochtone sur les données.

CARE signifie :

- Les systèmes de données doivent être conçus pour permettre des résultats équitables pour les peuples autochtones.
- Les droits et les intérêts des peuples autochtones sur leurs données doivent être reconnus et leur autorité sur leurs données doit être renforcée.
- **Responsabilité:** Les personnes qui travaillent avec des données autochtones doivent développer des relations positives et respectueuses et s'assurer que les données sont utilisées pour soutenir l'autodétermination des peuples autochtones.
- **Éthique:** Les droits et le bien-être des populations autochtones doivent être au cœur des préoccupations en matière de gouvernance des données.

## Autres Ressources

- Australian Research Data Commons (2024) [“CARE Principles”](https://ardc.edu.au/resource/the-care-principles/)
- [CARE Principles for Indigenous Data Governance (Wikipedia)](https://en.wikipedia.org/wiki/CARE_Principles_for_Indigenous_Data_Governance)
- Global Indigenous Data Alliance (2024) ["CARE Principles for Indigenous Data Governance"](https://www.gida-global.org/care)
- Research Data Alliance International Indigenous Data Sovereignty Interest Group (2019) ["CARE Principles for Indigenous Data Governance"](https://static1.squarespace.com/static/5d3799de845604000199cd24/t/6397b363b502ff481fce6baf/1670886246948/CARE%2BPrinciples_One%2BPagers%2BFINAL_Oct_17_2019.pdf)
- Russo Carroll, et al. (2020) [“The CARE Principles for Indigenous Data Governance”](https://datascience.codata.org/articles/10.5334/dsj-2020-043)
- St. Lawrence Global Observatory (2024) [“CARE Principles”](https://www.ogsl.ca/en/care-principles/)