---
id: edge
title: Edge
definition: A line that connects one node to another in a graph database, representing a relationship between the nodes.
---

An edge is a line that connects one :Term[node]{#node} to another in a :Term[graph database]{#graph-database}, representing a relationship between them. Edges can be directed or undirected. When they are directed they have different meanings depending on their direction.

## Examples

- The following image shows a :Term[Resource Description Framework (RDF)]{#resource-description-framework} graph highlighting the edges that connect three nodes.

![An edge is visually displayed as a line between two nodes, and represents a relationship. Nodes can have multiple edges.](</img/documentation/glossary-edge-example-(c-LINCS).jpg>)
