import React from "react";
import Translate from "@docusaurus/Translate";
import { translate } from "@docusaurus/Translate";

// import conference2024Image from "@site/static/img/documentation/events-forward-linking-2024-banner-horizontal-(c-LINCS).png";
import conference2021Image from "@site/static/img/documentation/events-lincs-conference-2021-poster-vertical-(c-LINCS).jpg";
import conference2024Image from "@site/static/img/documentation/events-forward-linking-2024-logo-(c-LINCS).png";
import conference2023Image from "@site/static/img/documentation/events-making-links-2023-(c-LINCS).png";
import dhguelph2024Image from "@site/static/img/documentation/events-dh-summer-workshops-2024-logo-(c-LINCS).png";

const eventSchema = {
  title: "string",
  eventType: "conference|workshop|meeting|other",
  startDate: "string",
  endDate: "string",
  datePretty: "string",
  location: "string",
  description: "string",
  link: "string",
  image: "string",
};

const events = {
  "2024 Conference": {
    title: <Translate>Forward Linking Conference and Workshops</Translate>,
    eventType: "conference",
    startDate: "2024-05-05",
    endDate: "2024-05-08",
    datePretty: translate({ message: "May 5-8, 2024" }),
    location: translate({ message: "University of Ottawa" }),
    description: translate({
      message:
        "The Forward Linking conference and workshops on linked open data for cultural scholarship and cultural memory is taking place at the University of Ottawa 5–8 May 2024 (conference 6–7 May and workshops on 8 May)",
    }),
    link: "https://hsscommons.ca/en/groups/forward_linking/events/2024conference",
    image: conference2024Image,
  },
  "DH@Guelph 2024": {
    title: <Translate>Making Connections: LINCS workshop, DH@Guelph</Translate>,
    eventType: "workshop",
    startDate: "2024-05-14",
    endDate: "2024-05-17",
    datePretty: translate({ message: "May 14-17, 2024" }),
    location: translate({ message: "University of Guelph" }),
    description: translate({
      message:
        "The DH@Guelph team is thrilled to announce the 2024 Summer Workshops, which will take place in person in the McLaughlin Library at the University of Guelph from May 14th- 17th.",
    }),
    link: "https://www.uoguelph.ca/arts/research/centres-institutes-and-labs/digital-humanities-guelph/dh-events/summer-workshops/2024-6",
    image: dhguelph2024Image,
  },
  "2023 Conference": {
    title: <Translate>Making Links: Connections, Cultures, Contexts conference</Translate>,
    eventType: "conference",
    startDate: "2023-05-05",
    endDate: "2023-05-07",
    datePretty: translate({ message: "May 5-7, 2023" }),
    location: translate({ message: "University of Guelph" }),
    description: translate({
      message:
        "The Making Links: Connections, Cultures, Contexts conference (May 5-7, 2023 at the University of Guelph) addresses a wide range of topics relevant to creating and using linked data for research on cultural scholarship and cultural memory in the Canadian context and beyond. It marks the beta launch of the Linked Infrastructure for Networked Cultural Scholarship (LINCS) suite of tools for the creation, exploration, and use of linked open data.",
    }),
    link: "/docs/about-lincs/get-involved/events/2023-05-conference",
    image: conference2023Image,
  },
  "2021 Conference": {
    title: <Translate>LINCS conference</Translate>,
    eventType: "conference",
    startDate: "2021-04-29",
    endDate: "2021-05-06",
    datePretty: translate({ message: "April 29-May 6, 2021" }),
    location: translate({ message: "Online" }),
    description: translate({
      message:
        "The 2021 Linked Infrastructure for Networked Cultural Scholarship (LINCS) conference (29 April-6 May, 2023 online) features tool demos, technical and research talks, and social gatherings.",
    }),
    link: "/docs/about-lincs/get-involved/events/2021-05-conference",
    image: conference2021Image,
  },
};

export default events;
