---
title: "Documentation Context Explorer"
Machine Translation: True
Translation Tool: DeepL
Translated Added By: Humna Chaudhry(@humnq)
Last Translated: 2024-06-16
---

:::info

Cette page web a été traduite automatiquement par DeepL. Bien que nous nous efforcions d’être précis, nous vous informons que les traductions peuvent contenir des erreurs ou des inexactitudes. Pour obtenir les informations les plus précises, veuillez vous référer à la version originale.

:::

## Conditions préalables

### Installer l'explorateur

Pour installer l'explorateur:

1. Naviguez jusqu'à la [page Context Explorer] (https://chrome.google.com/webstore/detail/lincs-entity-search-tool/ingihipdallefgbcbnkanjnhlkdkjecf) sur le Chrome Webstore:

`https://chrome.google.com/webstore/detail/lincs-entity-search-tool/ingihipdallefgbcbnkanjnhlkdkjecf`

2. Cliquez sur **Ajouter à Chrome**, puis sur **Ajouter une extension**.
3. Cliquez sur **Extensions** (icône en forme de pièce de puzzle, située sur le côté droit de la barre de menu de Chrome) et épinglez l'explorateur à votre navigateur.

:::note

Il n'y a actuellement pas de support français pour l'explorateur contextuel.

:::

## Utiliser l'explorateur

Pour utiliser l'explorateur, naviguez jusqu'à la page web que vous souhaitez analyser et cliquez sur l'icône LINCS que vous avez épinglée à la barre de menu de votre navigateur. Il existe deux options de numérisation:

- Cliquez sur **Scan Highlighted** pour numériser une section de texte surlignée sur la page.
- Cliquez sur **Scanner toute la page** pour scanner toute la page Web, y compris ses :Term[metadata]{#metadata}

L'explorateur de contexte met alors en évidence le :Term[entités]{#entity} (personnes, lieux, groupes ou œuvres) sur la page web. Si vous survolez une entité en surbrillance, l'explorateur recherchera des correspondances possibles dans le :Term[triplestore]{#triplestore} du LINCS et dressera la liste des correspondances dans une barre latérale droite. L'explorateur essaiera d'afficher d'abord la correspondance la plus pertinente. Pour afficher des informations supplémentaires, sélectionnez une correspondance dans la liste des options proposées.

Lorsque vous cliquez sur l'une des correspondances suggérées, un arbre contextuel s'ouvre. Cet arbre montre comment l'entité sélectionnée est liée à d'autres entités du triplestore. Cliquez sur l'icône de page à droite de la barre latérale pour afficher des informations sur l'entité. Cliquez sur **Voir plus** pour accéder à d'autres informations, y compris un lien vers l'entité dans [ResearchSpace](https://rs.lincsproject.ca). Si les correspondances proposées ne vous conviennent pas, cliquez sur **Retour aux résultats** en haut de la barre latérale pour revenir à la liste des correspondances et sélectionner une autre option.

Pour quitter l'arbre contextuel d'une entité, cliquez sur la **touche d'échappement** ou cliquez ailleurs sur la page. Vous pouvez également cliquer sur le bouton **X** dans le coin supérieur droit de la barre latérale de l'explorateur pour fermer l'analyse.

## Personnaliser l'explorateur

Vous pouvez personnaliser l'explorateur de contexte.

### Modifier les points forts

Cliquez sur l'icône **Explorateur de contexte** dans la barre de menu de votre navigateur pour ouvrir la fenêtre principale de l'explorateur. Dans cette fenêtre, cliquez sur la roue des paramètres dans le coin supérieur droit pour modifier les paramètres de mise en évidence de l'explorateur.

- Sélectionnez **Afficher uniquement la première occurrence** pour que l'explorateur ne mette en évidence que la première mention d'une entité. Notez que si la première occurrence d'une entité se trouve dans les métadonnées de la page web (qui ne sont pas affichées sur votre écran), ou si deux entités différentes sont orthographiées de la même manière (par exemple, la page fait référence à la fois à une personne et à un lieu nommé _Virginia_), certaines entités ne seront pas mises en évidence.
- Le **Score de pertinence de la mise en évidence** modifie le score nécessaire pour que l'explorateur mette en évidence une entité dans le texte. Si le score de pertinence est trop élevé, peu d'entités seront mises en évidence.

### Edit Filters

Vous pouvez ajouter des filtres à l'explorateur de contexte afin qu'il ne mette en évidence que les entités d'un certain type :Term[Named Entity Recognition (NER)]{#named-entity-recognition} ou les entités d'un ensemble de données LINCS spécifique. Par exemple, si vous sélectionnez _les personnes_, l'explorateur ne mettra en évidence que les entités qui sont des personnes. Si vous sélectionnez _Orlando_, l'explorateur ne mettra en évidence que les entités trouvées dans l'ensemble de données Orlando.

Il est possible de filtrer en fonction des types de NER et des ensembles de données ci-dessous:

|                        NER Type                        |       Dataset       |
| :----------------------------------------------------: | :-----------------: |
|                   Événements nommés                    |      AdArchive      |
| Installations (bâtiments, aéroports, autoroutes, etc.) |  Anthologia graeca  |
|                  Pays, villes, états                   |   Ethnomusicology   |
|          Documents nommés ayant force de loi           |       HistSex       |
| Lieux hors de l'État (montagnes, étendues d'eau, etc.) |        MoEML        |
|     Nationalités, groupes religieux ou politiques      |       Orlando       |
|                     Organisations                      |      USask Art      |
|                     Les personnes                      |   Yellow Nineties   |
|            Objets, véhicules, aliments, etc.           |                     |
|       Œuvres d'art (livres, chansons, peintures)       |                     |

:::warning

L'explorateur de contexte est fréquemment mis à jour. Votre navigateur web peut ou non mettre à jour les plugins automatiquement en fonction de vos paramètres. Il est conseillé de s'assurer que vous utilisez toujours la version la plus récente. Vous pouvez vérifier si votre version de l'explorateur de contexte est à jour en cliquant sur le bouton **extensions** (pièce de puzzle) dans la barre de menu de Chrome et sur **Gérer les extensions** en bas de la fenêtre qui s'ouvre, ou en visitant la page de l'explorateur de contexte sur la boutique en ligne de Chrome.

:::
