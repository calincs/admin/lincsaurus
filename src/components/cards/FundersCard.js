import React from 'react';
import Link from '@docusaurus/Link';

function FundersCard({ src, alt, link }) {
    return (
        <div>
            <Link to={link}>
                <img src={src} alt={alt} />
            </Link>
        </div>
    );
}

export default FundersCard;