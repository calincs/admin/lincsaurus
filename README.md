# The LINCS portal

This website is built using [Docusaurus 3](https://docusaurus.io/), a modern static website generator that uses [MDX](https://mdxjs.com/).

The site is deployed to <https://portal.lincsproject.ca/> and is also available at <https://lincsproject.ca/>.

## Installation

You will need Git (with an ssh key set up to connect to GitLab), NodeJS (v18 or newer), and a code editor (VS Code is recommended)  to develop this website.

```bash
git clone git@gitlab.com:calincs/admin/lincsaurus.git
cd lincsaurus
```

## Local development

```bash
npm install
npm start
```

This command starts a local development server at `http://localhost:3000/` and opens up a browser window. Most changes to `src/` are reflected live without having to restart the server, but for most changes to markdown files, the server will have restarted.

After all your changes are reflected on `http://localhost:3000/` it's a good idea to test the build.

1. Ctrl-C to stop your current local development server
2. `npm run build` - This does a full rebuild of the site
3. `npm run serve` - This starts local development server at `http://localhost:3000/`
4. Review any errors that may have shown up and revise. You can also test by using the docker compose file if you have docker set up.

This is good to do before committing to ensure a broken build isn't pushed to GitLab.

### View Translation Locally

To see the converted ISO locally at `http://localhost:3000/`, run the following command in your Docusaurus repo:

```bash
npm run start -- --locale your_iso_code
```

If you want to be able to see both the English version and your ISO code translations, run `npm run build` and then `npm run serve`.

The translations are not always perfect, please double check your files have the correct translation after conversion before committing your changes.

### Making Edits

Prior to making any edits for the first time, you must review the [Project Documentation Guide](https://gitlab.com/calincs/admin/lincsaurus/-/wikis/home), particularly the [Accessibility](https://gitlab.com/calincs/admin/lincsaurus/-/wikis/accessibility) & [Style](https://gitlab.com/calincs/admin/lincsaurus/-/wikis/style-guide) Guide for guidelines on writing documentation.

See the [Maintenance Guide](https://gitlab.com/calincs/admin/lincsaurus/-/wikis/maintenance-guide) for instructions on how to get started making edits.

Filename extensions must `mdx` if using any custom components. (Generally recommended to use mdx in case you decide to later add buttons or other things)

## Plugins Used

- @docusaurus/plugin-client-redirects - used to handle a few redirects from old site
- glossary directive - custom glossary plugin using Text Directives 

## Developer Practices

### Recommended Resources

- [Docusaurus Guides](https://docusaurus.io/docs/category/guides)
  - [Styling & Layout](https://docusaurus.io/docs/styling-layout)
  - [MDX & React](https://docusaurus.io/docs/markdown-features/react)
  - [Static Assets](https://docusaurus.io/docs/static-assets)
  - [Translate your React Code](https://docusaurus.io/docs/i18n/tutorial#translate-your-react-code)
- [Docusaurus's Source code for their Docusaurus Site](https://github.com/facebook/docusaurus/tree/main/website)
- [MDN CSS Guide](https://developer.mozilla.org/en-US/docs/Learn/CSS/Building_blocks)
- [LINCS Wiki - Accessibility Guide](https://gitlab.com/calincs/admin/lincsaurus/-/wikis/accessibility)

### Best Practices

- This project uses CSS modules to organize its CSS, see <https://docusaurus.io/docs/styling-layout#css-modules>
  - When styling a custom component, look at the implementation for the component to find the particular CSS class that should be used
- Code should be formatted using Prettier settings found in package.json
- Avoid absolute styling when possible, have responsiveness in mind.
  - Remember to keep translation in mind, French words tend to be longer
  - We also have French assets that should be used where available on French pages
- After creating new components that will be used in multiple places, document expected usage in [Snippets](https://gitlab.com/calincs/admin/lincsaurus/-/snippets)

## Rebuilding the search index

We use a _Typesense_ index for searching the Lincsaurus site. The index is hosted on LINCS infrastructure and is updated with the CI pipeline. If you want to host an index locally, you'd need to rebuild it manually if we wish to update it.

Start the Typesense server:

```bash
./typesense.sh
```

Then run the scraper in a new terminal, which will index `https://portal.lincsproject.ca` with the current configuration and write the index to your local typesense server:

```bash
./scraper.sh
```

## Redirects

The following urls are being redirected:

- <https://lincsproject.ca/research> --> /docs/about-lincs/research
- <https://lincsproject.ca/areas-of-inquiry> --> /docs/about-lincs/research
- <https://lincsproject.ca/contributors> --> /docs/about-lincs/people/collaborators
- <https://lincsproject.ca/team> --> /docs/about-lincs/people/team
- <https://lincsproject.ca/2023-conference-schedule/> --> /docs/about-lincs/get-involved/events/2023-05-conference
- <https://lincsproject.ca/events/making-links-2023> --> /docs/about-lincs/get-involved/events/2023-05-conference
- <https://lincsproject.ca/events/lincs-conference-2021/> --> /docs/about-lincs/get-involved/events/2021-05-conference
- <https://lincsproject.ca/newsletters/> --> /docs/about-lincs/get-involved/newsletter
- <https://lincsproject.ca/research-participants-needed/> --> /docs/about-lincs/get-involved/participate
- <https://lincsproject.ca/docs/create-data/publish-data/publish-workflows/keywords-instructions> --> /docs/create-lod/prepare-metadata
- <https://lincsproject.ca/docs/about-lincs/get-involved/grants-menu> --> /docs/about-lincs/grants-menu


Blog posts:

- <https://lincsproject.ca/marvels-of-api-communication-the-diffbot-nlp-vetting-tool/> --> /blog/marvels-of-api
- <https://lincsproject.ca/development-in-steps-learning-to-collaborate-on-a-technical-project/> --> /blog/development-in-steps
- <https://lincsproject.ca/researching-and-designing-for-actionability/> --> /blog/researching-and-designing
- <https://lincsproject.ca/piecing-the-past-together-with-lod/> --> /blog/piecing-the-past
- <https://lincsproject.ca/reconciliation-richard-of-cornwall-and-using-historical-sources/> --> /blog/reconciliation-richard
- <https://lincsproject.ca/jessica-and-goliath-learning-3m-and-cidoc-crm/> --> /blog/jessica-goliath
- <https://lincsproject.ca/expression-and-suppression-orlando-stories-and-censorship/> --> /blog/expression-suppression
- <https://lincsproject.ca/the-ship-of-theseus-representing-nuance-in-humanities-data/> --> /blog/ship-of-theseus
- <https://lincsproject.ca/the-good-enough-metadata-specialist/> --> /blog/metadata-specialist
- <https://lincsproject.ca/speaking-in-different-languages-working-across-groups-and-disciplines/> --> /blog/speaking-in-different-languages
- <https://lincsproject.ca/two-roads-diverged/> --> /blog/tube-map-diverged
- <https://lincsproject.ca/digging-into-dh/> --> /blog/digging-into-dh
- <https://lincsproject.ca/diving-into-large-scale-projects-as-a-complete-novice/> --> /blog/large-scale-projects
- <https://lincsproject.ca/what-is-extract-transform-load/> --> /blog/extract-transform-load
- <https://lincsproject.ca/searching-for-harmony-questions-in-ontologies-for-music-data/> --> /blog/music-ontologies
- <https://lincsproject.ca/connections-are-important-making-links-in-oral-history-using-linked-open-data/> --> /blog/oral-history-LOD
- <https://lincsproject.ca/breaking-down-the-barriers-to-data-conversion/> --> /blog/breaking-down-barriers
- <https://lincsproject.ca/design-frolics-and-demystifying-user-interface-design/> --> /blog/design-frolics
- <https://lincsproject.ca/the-user-centric-approach-to-problem-solving/> --> /blog/user-problem-solving
