---
id: simple-knowledge-organization-system
title: Système d’organisation simple des connaissances (SKOS)
definition: Une norme qui fournit un moyen de représenter des thésaurus, des taxonomies et des vocabulaires contrôlés conformément au cadre de description des ressources (RDF).
---

Le système d’organisation simple des connaissances (SKOS) est une norme qui fournit un moyen de représenter :Term[thesaurus]{#thesaurus}, :Term[taxonomies]{#taxonomy}, et :Term[vocabulaires contrôlés]{#controlled-vocabulary} suivant le :Term[Cadre de description des ressources (RDF)]{#resource-description-framework}.

SKOS permet aux vocabulaires LINCS d’être connectés à d’autres vocabulaires et ensembles de données RDF. Tous les termes de vocabulaire déclarés dans SKOS au LINCS sont définis comme des instances du type de classe E55 dans :Term[CIDOC CRM]{#cidoc-crm} et reçoivent leur propre :Term[Uniform Resource Identifier (URI)]{#uniform-resource-identifier}.

## Autres ressources

- [Simple Knowledge Organization System (Wikipedia)](https://en.wikipedia.org/wiki/Simple_Knowledge_Organization_System)
- W3C (2012) [“Introduction to SKOS”](https://www.w3.org/2004/02/skos/)
- Zaytseva & Durco (2020) [“Controlled Vocabularies and SKOS”](https://campus.dariah.eu/resource/posts/controlled-vocabularies-and-skos)
