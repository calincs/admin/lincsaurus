---
description: "Manage Digital Humanities (DH) projects."
title: "Corpora"
Last Translated: 2023-07-26
image: /img/documentation/corpora-overview-logo-(c-owner).png
---

<ToolButtons toolName="Corpora"/>

Corpora is a web-based application with a robust database system for :Term[Digital Humanities (DH)]{#digital-humanities} projects. You can use Corpora to perform :Term[Optical Character Recognition (OCR)]{#optical-character-recognition} on uploaded documents, ascribe :Term[Uniform Resource Identifiers (URIs)]{#uniform-resource-identifier} and Corpora content types to :Term[entities]{#entity}, build network visualizations, and more.

<div className="banner">
  <img src="/img/documentation/corpora-overview-logo-(c-owner).png" alt="" />
</div>

<div className="primary-button-row">
  <PrimaryButton
    link="https://corpora.dh.tamu.edu/scholar?next=/"
    buttonName="To the Tool"
    />
  <PrimaryButton
    link="https://corpora-docs.dh.tamu.edu/"
    buttonName="To the Documentation"
    />
  <PrimaryButton
    link="https://github.com/bptarpley/corpora"
    buttonName="To GitHub"
    />
</div>

## Corpora and LINCS

In collaboration with LINCS, Corpora is being used to ascribe URIs to named entities in the [Advanced Research Consortium (ARC)](https://arc.dh.tamu.edu/) catalogue and transform its data into :Term[triples]{#triple} so that it can be :Term[ingested]{#ingestion} into the LINCS :Term[triplestore]{#triplestore}.

Corpora lets users identify and assign URIs to entities in ARC, and will soon incorporate functionality from LINCS’s :Term[Natural Language Processing (NLP)]{#natural-language-processing} tools such 
as [NERVE](/docs/tools/nerve). Corpora is also associated with the [Rich Prospect Browser (RPB)](/docs/tools/rich-prospect-browser), an in-development visualization tool for :Term[Linked Data (LD)]{#linked-data} that allows users to browse between and within linked databases. Once complete, the RPB will be integrated into Corpora in place of the current network visualization tool.

At present, Corpora is tailored to working with bibliographic data in traditional DH projects that are focused on individual artifacts and entities. Corpora is particularly suited to transforming these types of datasets.

Corpora can be used online or the tool itself can also be downloaded, running and saving data locally. While Corpora will make backups of uploaded datasets when used online, it is not committed to long term data storage.

## Prerequisites

- You need to come with their own dataset
- You need to create a [user account](https://corpora.dh.tamu.edu/scholar?register=y)
  - A [GitLab](https://gitlab.com/) or [GitHub](https://github.com/) account can also be used to import a repository directly to Corpora.
- A basic understanding of [Python](https://www.python.org/) and :Term[JSON]{#json} is required to access full functionality

Corpora supports the following inputs and outputs:

- **Input:** PDF, JPEG, MARC, XML, and more
- **Output:** JSON

## Resources

To learn more about Corpora, see the following resources:

- LINCS (2021) [“Corpora Demo”](/docs/about-lincs/get-involved/events/2021-05-conference#corpora-demo)
- LINCS (2021) [“Corpora Demo”](https://www.youtube.com/watch?v=2X8gr3RoH18) [Video]

Information about the team that developed [Corpora](/docs/about-lincs/credits/tools-credits#corpora) is available on the Tool Credits page.