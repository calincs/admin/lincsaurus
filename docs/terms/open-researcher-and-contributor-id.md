---
id: open-researcher-and-contributor-id
title: Open Researcher and Contributor ID (ORCID)
definition: A not-for-profit organization that provides free Uniform Resource Identifiers (URIs) to researchers so they can be connected to their scholarship and bibliographic output.
Last Translated: 2024-06-21
---

Open Researcher and Contributor ID (ORCID) is a not-for-profit organization that provides free :Term[Uniform Resource Identifiers (URIs)]{#uniform-resource-identifier} for researchers so they can be connected to their scholarship and bibliographic output. In addition to providing identifiers, ORCID also provides records for researchers that aggregate their scholarly output and :Term[Application Programming Interfaces (APIs)]{#application-programming-interface} to allow researchers to connect their identifier with their affiliations and contributions.

ORCID URIs can be used for :Term[entity matching]{#entity-matching}. Those participates in LINCS are encouraged to register for one and add it to their account information.

## Examples

- [Susan Brown (0000-0002-0267-7344)](https://orcid.org/0000-0002-0267-7344)
- [Sarah Roger (0000-0001-9979-0353)](https://orcid.org/0000-0001-9979-0353)

## Further Resources

- [ORCID](https://orcid.org/)
- [ORCID (Wikipedia)](https://en.wikipedia.org/wiki/ORCID)
