---
id: tei-data
title: TEI Data
definition: Data that is conformant against a schema generated from a customization of the Guidelines of the Text Encoding Initiative (TEI).
Last Translated: 2024-06-21
---

TEI data is a document that is conformant against a schema generated from a customization of the Guidelines of the :Term[Text Encoding Initiative (TEI)]{#text-encoding-initiative}. All TEI documents are valid against a customization's schema, even where that is 'TEI All' (a customization that contains the whole of the TEI). In addition conformant TEI data obeys the TEI's normative 'abstract model' descibed in the prose of the [TEI Guidelines](https://www.tei-c.org/release/doc/tei-p5-doc/en/html/index.html).

## Examples

- The following example shows some fragmentary TEI data about the person Joan Braderman:

```xml
<person xml:id="h-nr254">
   <idno type="Wikidata">http://www.wikidata.org/entity/Q28911659</idno>
   <persName>
      <name>Braderman, Joan</name>
   </persName>
   <persName type="preferred">
      <forename>Joan</forename>
      <surname>Braderman</surname>
   </persName>
   <floruit>
      <date>1977</date>
   </floruit>
   <occupation cert="high">Film maker</occupation>
   <affiliation cert="high">Heresies Collective</affiliation>
</person>
```

- The following example show a complete valid TEI document

```xml
<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://www.tei-c.org/release/xml/tei/custom/schema/relaxng/tei_all.rng" 
   type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<?xml-model href="http://www.tei-c.org/release/xml/tei/custom/schema/relaxng/tei_all.rng" 
   type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
   <teiHeader>
      <fileDesc>
         <titleStmt>
            <!-- Every TEI document requires a title -->
            <title>A Sample TEI Document</title>
            <author ref="#JamesC">James Cummings</author>
         </titleStmt>
         <publicationStmt>
            <publisher>A variety of information about the publisher can go
               here.</publisher>
            <authority>Information about the authority responsible for the
               document can go here</authority>
         </publicationStmt>
         <sourceDesc>
            <p>A variety of bibliographic references about the source file, even
               if born-digital, can go here.</p>
         </sourceDesc>
      </fileDesc>
   </teiHeader>
   <standOff>
      <listPerson>
         <!-- Information about people mentioned in the document can go here -->
         <person xml:id="JamesC">
            <persName type="main">
               <forename>James</forename>
               <surname>Cummings</surname>
            </persName>
            <affiliation>Newcastle University</affiliation>
         </person>
      </listPerson>
   </standOff>
   <text>
      <body>
         <div>
            <head>About the TEI</head>
            <p> The Text Encoding Initiative (TEI) is a membership consortium
               which, amongst many other things, maintains a community-developed
               standard for the representation of texts in digital form. Its
               chief deliverable is a set of Guidelines which specify encoding
               methods for machine-readable texts, chiefly in the humanities,
               social sciences and linguistics. </p>
         </div>
         <div>
            <head>About the TEI Guidelines</head>
            <p>The <ref
                  target="https://www.tei-c.org/release/doc/tei-p5-doc/en/html/index.html"
                  >TEI Guidelines</ref> are a very generalised encoding
               framework which enables many and disparate uses across a wide
               range of (usually Humanities-based) domains. TEI projects
               customise their use of the TEI framework through a TEI ODD
               customisation, often created through web tools such as <ref
                  target="https://roma.tei-c.org/">Roma</ref>.</p>
            <!-- More documentation here... -->
         </div>
      </body>
   </text>
</TEI>

```
