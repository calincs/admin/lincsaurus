---
id: text-encoding-initiative
title: Text Encoding Initiative (TEI)
definition: A membership consortium and international community which which, amongst many other things, maintains a community-developed standard for the representation of texts in digital form. Its chief deliverable is a set of Guidelines which specify encoding methods for machine-readable texts, chiefly in the humanities, social sciences and linguistics.
---

The [Text Encoding Initiative](https://www.tei-c.org/) (TEI) is a membership consortium and international community which, amongst many other things, maintains a community-developed standard for the representation of texts in digital form. Its chief deliverable is a set of Guidelines which specify encoding methods for machine-readable texts, chiefly in the humanities, social sciences and linguistics. The TEI framework supports the detailed encoding of complex documents and is used extensively by most text encoding :Term[Digital Humanities (DH)]{#digital-humanities} projects. As an encoding language, it is used to describe both the physical characteristics of a text (line breaks, pagination, special characters, etc.), classify subjects, tag people, places, and events, provide linguistic markup, and to add commentary. The [TEI Guidelines](https://www.tei-c.org/release/doc/tei-p5-doc/en/html/index.html) very generalised encoding framework which enables many and disparate uses across a wide range of (usually Humanities-based) domains. TEI projects customise their use of the TEI framework through a TEI ODD customisation, often created through web tools such as [Roma](https://roma.tei-c.org/).

## Examples
- [Map of Early Modern London](https://mapoflondon.uvic.ca/)
- [Orlando](http://www.artsrn.ualberta.ca/orlando/)
- [William Godwin's Diary](http://godwindiary.bodleian.ox.ac.uk/index2.html)
- [Union Catalogue of Manuscripts from the Islamicate World](https://www.fihrist.org.uk/)

## Further Resources
- [The TEI P5 Guidelines](https://www.tei-c.org/release/doc/tei-p5-doc/en/html/index.html) 
- Crompton, et al. (2021), [“Mapping and Vetting: Converting TEI to Linked Data”](https://www.dropbox.com/s/lj94h40odvrlnuk/Crompton-Zafar-Canning-Lipski-Defours-LINCS-TEI-2021.pdf?dl=0) [PowerPoint]
- Crompton (2022) [“What the Computer Doesn’t Know: Representing Primary Source Documents in TEI”](http://constancecrompton.com/2015/cin/CromptonTEI.pdf) [PowerPoint]
- Text Encoding Initiative (2022) [“Projects Using the TEI”](https://tei-c.org/activities/projects/)
