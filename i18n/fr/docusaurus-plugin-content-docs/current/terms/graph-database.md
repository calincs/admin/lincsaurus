---
id: graph-database
title: Base de données graphique
definition: Une base de données qui structure les informations sous forme de graphique ou de réseau, où un ensemble de ressources, ou nœuds, sont reliés entre eux par des arêtes qui décrivent les relations entre chaque ressource.
---

Une base de données de graphes structure les informations sous forme de graphe ou de réseau, où un ensemble de ressources, ou :Term[nœuds]{#node}, sont reliés entre eux par :Term[edges]{#edge} qui décrivent les relations entre chaque ressource. Les bases de données de graphes sont un type de :Term[Base de données NoSQL]{#nosql-database} et représentent :Term[Données liées (LD)]{#linked-data} en tant que nœuds, arêtes et :Term[propriétés]{#property}.

| Base de données graphique                                          | Triplestore                             |
| ------------------------------------------------------------------ | --------------------------------------- |
| Prend en charge une variété de langages de requête tels que Cypher | Utilise SPARQL comme langage de requête |
| Stocke divers types de graphiques                                  | Stocke des rangées de triples           |
| Centré sur les nœuds/propriétés                                    | Centré sur les bords                    |
| Ne fournit pas d’inférences sur les données                        | Fournit des inférences sur les données  |
| Moins académique                                                   | Plus synonyme de “web sémantique”     |

## Exemples

- [ArangoDB](https://www.arangodb.com/)
- [Amazon (Web Services) Neptune](https://aws.amazon.com/neptune/)
- [JanusGraph](https://janusgraph.org/)
- [Neo4j](https://neo4j.com/)
- [OrientDB](https://orientdb.com/)

## Autres ressources

- [Graph Database (Wikipedia)](https://en.wikipedia.org/wiki/Graph_database)
- Neo4j (2022) [“Introduction to Graph Databases Video Series”](https://neo4j.com/developer/intro-videos/)
- Robinson, Webber, Eifrem (2013) _[Graph Databases](https://hura.hr/wp-content/uploads/2016/10/Graph_Databases_2e_Neo4j-5.pdf)_
