---
id: optical-character-recognition
title: Reconnaissance optique de caractères (OCR)
definition: La conversion automatique d’images de mots en un fichier texte que les utilisateurs peuvent ensuite rechercher et modifier.
---

La reconnaissance optique de caractères est la conversion d’images de mots en un fichier texte que les utilisateurs peuvent ensuite rechercher et modifier. Il peut être utilisé pour créer des fichiers texte à partir d’images de texte manuscrit, de documents numérisés, d’images contenant des mots ou de toute autre forme de document non textuel contenant visuellement des mots.

## Exemples

- Numérisation pour créer du texte ou des versions consultables de livres ([Project Gutenberg](https://www.gutenberg.org/), [Google Books](https://books.google.com/))
- Prendre une photo d’un chèque pour le déposer
- Reconnaissance automatique des plaques d’immatriculation des véhicules pour le péage électronique ([Highway 407](https://www.on407.ca/en/tolls/tolls/tolls-explained.html))

## Autres ressources

- Computerphile (2017) [“Optical Character Recognition (OCR)”](https://www.youtube.com/watch?v=ZNrteLp_SvY) [Vidéo]
- Khandelwal (2020) [“An Introduction to Optical Character Recognition for Beginners”](https://towardsdatascience.com/an-introduction-to-optical-character-recognition-for-beginners-14268c99d60)
- [Optical Character Recognition (Wikipedia)](https://en.wikipedia.org/wiki/Optical_character_recognition)
