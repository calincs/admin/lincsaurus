---
slug: researching-and-designing
title: "Recherche et conception pour l'actionnabilité"
authors: Jordan Lum
tags: [UX]
hide_table_of_contents: true
image: ./researching-and-designing-unsplash-(cc0).jpg
Machine Translation: True
Translation Tool: DeepL
Translated Added By: Humna Chaudhry(@humnq)
Last Translated: 2024-06-15
---

![](<./researching-and-designing-unsplash-(cc0).jpg>)

:::info

Cette page web a été traduite automatiquement par DeepL. Bien que nous nous efforcions d’être précis, nous vous informons que les traductions peuvent contenir des erreurs ou des inexactitudes. Pour obtenir les informations les plus précises, veuillez vous référer à la version originale.

:::

L'équipe UX a mené des recherches auprès des utilisateurs sur les outils LINCS afin de les préparer à passer du développement à la production. Jusqu'à présent, nous avons effectué des tris de cartes, des tests de convivialité, des enquêtes et des entretiens. Ces recherches nous ont fourni une mine d'informations. Cependant, si nous voulons traduire ce que nous avons appris en changements significatifs et productifs dans la conception des outils, nous devons garder l'actionnabilité au centre de notre pratique de recherche...{/* truncate */}

Pendant mes études d'UX, j'ai suivi un cours sur les techniques de recherche utilisateur. Je pensais que les méthodes de recherche UX étaient assez simples : parlez à vos utilisateurs, synthétisez les données que vous avez recueillies au cours de ces conversations dans des personas et des cartes de parcours, et voilà - vous avez terminé votre recherche utilisateur. Malheureusement, je me trompais. Dans le cadre de l'un des travaux pratiques du cours, mes camarades de classe et moi-même avons été invités à observer des participants pendant qu'ils effectuaient une tâche d'achat. Lorsque nous avons constaté qu'ils éprouvaient des difficultés, notre professeur a simplement demandé : "D'accord, mais comment ?". Lors de nos premières tentatives de recherche sur les utilisateurs, nous nous sommes contentés de chercher le problème ; nous n'avions pas encore compris que nous devions comprendre exactement comment et pourquoi les participants avaient du mal à accomplir leur tâche. Si nous n'approfondissions pas nos recherches pour comprendre les objectifs et les comportements de nos utilisateurs, nos résultats n'étaient pas exploitables, ce qui signifiait que nous ne pouvions pas tirer de solutions de nos recherches.

![Carte des actions de recherche/navigation avec un chemin d'accès mis en évidence et des notes sur les actions qui ont posé problème aux utilisateurs.](<./researching-and-designing-mockup-(c-LINCS).jpg>)

Cette leçon m'a appris la différence entre la recherche et une _bonne_ recherche. Les nouveaux concepteurs effectuent souvent des recherches pour les cocher sur une liste de contrôle, afin de montrer que les utilisateurs ont été consultés. Les concepteurs expérimentés mènent des recherches pour comprendre l'utilisateur, et ils transforment cette compréhension en action. Lorsque les besoins des utilisateurs ne sont pas clairs, la recherche ne sert pas la conception, ce qui aboutit à des solutions qui ne sont pas réellement centrées sur l'utilisateur.

Au cours de mon séjour à LINCS, j'ai constaté qu'il était essentiel de réfléchir en profondeur à l'actionnabilité en travaillant sur l'un de ses outils - ResearchSpace - au fur et à mesure qu'il évoluait dans le processus de conception. Les tests d'utilisabilité à l'échelle du site pour ResearchSpace, menés par Robin Bergart (bibliothécaire UX) et Evan Rees (concepteur UX), venaient juste de commencer lorsque j'ai pris mes fonctions. J'ai eu le privilège de faciliter quelques-uns des tests utilisés pour déterminer comment les utilisateurs comprenaient les fonctionnalités de LINCS ResearchSpace. Tout au long des tests et de l'analyse des résultats, j'ai réfléchi à l'actionnabilité à la fois à un niveau bas et à un niveau élevé.

Au niveau le plus bas, j'ai essayé de faire de chaque observation une donnée exploitable. Pendant les tests, j'ai noté des observations détaillées afin de relier les actions des participants à leurs processus de pensée tels qu'ils les décrivaient. Lorsque j'observais les participants accomplir une tâche, je notais leurs actions spécifiques, qu'il s'agisse d'éléments sur lesquels ils cliquaient ou de fonctionnalités qu'ils notaient comme étant les plus remarquables pour eux. Par exemple, nous avons demandé aux participants de rechercher un :Term[entité]{#entity} dans l'Espace Recherche. De nombreux participants ont réussi à trouver des entités en accédant au contenu d'un ensemble de données ; ce faisant, ils ont négligé la boîte de recherche dans la barre de navigation, qu'ils auraient également pu utiliser. En observant ce que les utilisateurs faisaient et ne faisaient pas, et en décrivant leurs actions au fur et à mesure qu'ils les accomplissaient, nous avons découvert des données riches qui nous ont permis de synthétiser nos résultats en modèles et thèmes significatifs.

Une fois que nous avons obtenu ces résultats, nous avons dû trouver un moyen d'étendre l'actionnabilité à d'autres personnes en dehors de l'équipe UX. Par exemple, nous devions partager nos résultats avec les développeurs avec lesquels nous collaborions pour apporter des améliorations significatives et réalisables aux outils. En pratique, cela signifiait que nous devions trouver un moyen de convertir notre tableau blanc UX virtuel désordonné, parsemé d'idées sur des notes autocollantes numériques, dans un format que l'équipe LINCS au sens large serait en mesure de lire et de comprendre. Pour ce faire, nous avons rédigé un rapport qui définissait chaque conclusion en mettant en évidence les points de douleur et nos observations clés correspondantes. Ce processus nous a aidés à faire passer nos idées, mais avec le recul, je pense que nous aurions pu être encore plus efficaces si, au lieu de nous contenter de présenter nos résultats à la fin du processus de synthèse, nous avions invité un plus large éventail de parties prenantes à participer au processus de tableau blanc. Cette stratégie aurait permis à l'équipe de mieux comprendre les résultats et aurait invité un plus grand nombre de personnes à réfléchir à des solutions.

Même si le chemin a été semé d'embûches, nos conclusions ont permis de trouver des pistes de solution. Par exemple, j'ai conçu une amélioration de la navigation en permettant aux utilisateurs d'accéder aux fonctions de l'Espace recherche directement à partir de l'écran d'accueil. Les besoins des utilisateurs ont influencé la présentation de mon projet, car l'étude des utilisateurs m'a montré que nous avions besoin d'une conception qui permette aux utilisateurs de trouver et d'utiliser rapidement la myriade de ressources de l'Espace de recherche.

![Le site web LINCS ResearchSpace beta, présentant les trois principales fonctionnalités sur la page d'accueil.](<./researching-and-designing-website-(c-LINCS).png>)

Ainsi, en gardant à l'esprit l'actionnabilité, notre recherche sur les utilisateurs a directement alimenté nos solutions de conception et a contribué à résoudre les problèmes réels des utilisateurs. J'ai trouvé passionnant de mettre en pratique les méthodes de recherche sur l'utilisateur que je n'avais apprises qu'en théorie. J'ai hâte de voir ce que nous réserve la poursuite de nos recherches et de nos conceptions !
