---
id: qname
title: QName
definition: Chaîne abrégée utilisée pour remplacer une référence d’identificateur de ressource uniforme.
Machine Translation: True     
Translation Tool: DeepL 
Translated Added By: Humna Chaudhry(@humnq)
Last Translated: 2024-06-21
---

:::info
Cette page web a été traduite automatiquement par DeepL. Bien que nous nous efforcions d’être précis, nous vous informons que les traductions peuvent contenir des erreurs ou des inexactitudes. Pour obtenir les informations les plus précises, veuillez vous référer à la version originale.
:::

Un QName, ou "nom qualifié", est une chaîne de caractères abrégée utilisée pour remplacer une référence :Term[Uniform Resource Identifier (URI)]{#uniform-resource-identifier}. Les QNames sont composés d'un préfixe attribué à un :Term[namespaces]{#namespace} d'un deux-points et d'un nom local. Les QNames permettent d'écrire les URI dans un format plus lisible.

## Exemples

- `crm:P2_has_type` est un QName pour `http://www.cidoc-crm.org/cidoc-crm/P2_has_type`, étant donné que le préfixe `crm` est défini comme l'utilisation de l'URI de l'espace de noms `http://www.cidoc-crm.org/cidoc-crm/`

## Autres ressources

- [QName (Wikipedia)](https://en.wikipedia.org/wiki/QName)
- W3C (2009) [_Namespaces in XML 1.0 (Third Edition)_](https://www.w3.org/TR/xml-names/)
- W3C (2014) [_RDF 1.1 XML Syntax_](https://www.w3.org/TR/rdf-syntax-grammar/#section-Syntax-intro)