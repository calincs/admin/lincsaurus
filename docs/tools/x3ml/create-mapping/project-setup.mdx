---
sidebar_position: 2
title: "Project Setup" 
description: "How to setup a new 3M project"
Last Translated: 2024-06-18
---

## Project Setup

Follow the instructions below to set up your mapping project.

1. Click the **Add New** button. This will open a window with the **Mapping Information** step.

| ![alt=""](/img/documentation/x3ml-setup-1-(c-LINCS).png)|
|:--:|
| The **Project Mappings** page with the **Add New** button indicated. |

2. Type in the **Title** of the mapping project along with a **Description**. Only the **Title** will be visible on the **Project Mappings** page. This **Title** will also be visible to all other users in the LINCS 3M instance.

:::note
Title and description are for your reference only and won’t be included in the transformed data. [LINCS has suggested title and description standards](/docs/tools/x3ml/create-mapping/recommendations).
:::

| ![alt="Title: Yellow Nineties Sample Project Description: Sample mapping project for 3m tutorial"](/img/documentation/x3ml-setup-2-(c-LINCS).png)|
|:--:|
| The **Mapping Information** step with the **Title** and **Description** filled out for the Yellow Nineties dataset. |

3. Click **Next** to move on to the **Source Input** step.

| ![alt=""](/img/documentation/x3ml-setup-3-(c-LINCS).png)|
|:--:|
| The **Mapping Information** step with the **Next** button indicated. |

4. Click **Add a new file** (plus button). This will create an empty panel to upload your source input file.

| ![alt=""](/img/documentation/x3ml-setup-4-(c-LINCS).png)|
|:--:|
| The **Source Input** step with **Add a new file** (plus button) indicated. |

| ![alt=""](/img/documentation/x3ml-setup-5-(c-LINCS).png)|
|:--:|
| The **Source Input** step with with an empty panel for uploading the source input. |

6. Upload the <UnderConstruction link="?">source data XML file</UnderConstruction> by dragging and dropping it into the panel or by clicking **Browse** and navigating to it in your file explorer. The **Title** of the source input file will automatically fill in based on the name of the file you uploaded. The **Title** will be visible in the <UnderConstruction link="?">**Files’ Metadata window**</UnderConstruction> and when you **[Export](/docs/tools/x3ml/create-mapping/export-extras) all files to zip**.

| ![alt="Title:3m_walkthrough_source_data"](/img/documentation/x3ml-setup-6-(c-LINCS).png)|
|:--:|
| The **Source Input** step with the uploaded source data file. |

7. Click **Next** to move on to the **Target Schema** step.

| ![alt="Title:3m_walkthrough_source_data"](/img/documentation/x3ml-setup-7-(c-LINCS).png)|
|:--:|
| The **Source Input** step with the **Next** button indicated. |

| ![alt=""](/img/documentation/x3ml-setup-8-(c-LINCS).png)|
|:--:|
| The **Target Schema** step. |

8. Select **CIDOC CRM (7.1.1)** from the list of options. This is the <UnderConstruction link="?">base CIDOC CRM ontology file</UnderConstruction>. 

:::note
The other preloaded Target Schema options feature different ontologies or extentions of the CIDOC CRM ontology. They are used for more complicated mappings such as full datasets and are beyond the scope of this walkthrough. For documentation on their use within LINCS see the [LINCS Application Profile](/docs/explore-lod/understand-lincs-data/application-profiles-main/) and [dataset-specific application profiles](/docs/explore-lod/project-datasets/).
:::

| ![alt=""](/img/documentation/x3ml-setup-9-(c-LINCS).png)|
|:--:|
| The **Target Schema** step with the **CIDOC CRM (7.1.1)** option selected. |

9. Click **Next** to move on to the **URI Generator Policy** step.

| ![alt=""](/img/documentation/x3ml-setup-10-(c-LINCS).png)|
|:--:|
| The **Target Schema** step with the **Next** button indicated. |

| ![alt=""](/img/documentation/x3ml-setup-11-(c-LINCS).png)|
|:--:|
| The **URI Generator Policy** step. |

10. Select **LINCS Generator Policy (1.1)**. This is the <UnderConstruction link="?">base generator policy</UnderConstruction> used by LINCS.

:::note
You can also write and upload your own generator policy, but that is beyond the scope of this walkthrough. For more information on generators and generator policies, go to the <UnderConstruction link="?" comment="This page doesn't exist?">Generator page</UnderConstruction> of this walkthrough or the offical [3M Generator Manual](https://mapping.d4science.org/3M/Manuals/en/X3ML_Generators_Manual.pdf).
:::

| ![alt=""](/img/documentation/x3ml-setup-12-(c-LINCS).png)|
|:--:|
| The **URI Generator Policy** step with the **LINCS Generator Policy (1.1)** option selected. |

11. Click **Next** to move on to the **Confirmation** step.

| ![alt=""](/img/documentation/x3ml-setup-13-(c-LINCS).png)|
|:--:|
| The **URI Generator Policy** step with the **Next** button indicated. |

| ![alt=""](/img/documentation/x3ml-setup-14-(c-LINCS).png)|
|:--:|
| The **Confirmation** step. |

12. Review your setup. The **Title** and **Description** might differ from the screenshot above, but ensure that your **Source Input**, **Target Schema**, and **Generator Policy** match the following:

Source Input: 3m_walkthrough_source_data (3m_walkthrough_source_data.xml)

Target Schema: CIDOC CRM v7.1.1 (CIDOC_CRM_v7.1.1.rdfs)

Generator Policy: LINCS Generator Policy v1.1 (lincs_gen_policy_v1.xml)

If your **Confirmation** step does not match the above, click the **Back** button and redo the relevant step.

| ![alt=""](/img/documentation/x3ml-setup-15-(c-LINCS).png)|
|:--:|
| The **Confirmation** step with the **Back** button indicated. |

13. Click **Finish** to finish setting up your mapping project.

| ![alt=""](/img/documentation/x3ml-setup-16-(c-LINCS).png)|
|:--:|
| The **Confirmation** step with the **Finish** button indicated. |

You're ready to start [mapping your data](/docs/tools/x3ml/create-mapping/mappings).

