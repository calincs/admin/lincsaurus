---
id: sparql-protocol-and-rdf-query-language
title: Protocole SPARQL et langage de requête RDF (SPARQL)
definition: Un langage de requête pour les triplestores qui traduit les données graphiques en données tabulaires normalisées avec des lignes et des colonnes.
---

SPARQL Protocol and RDF Query Language (SPARQL) est un langage de requête qui vous permet d’interroger :Term[triplestores]{#triplestore}. Il traduit les données graphiques en données tabulaires normalisées avec des lignes et des colonnes. Il est utile de considérer une requête SPARQL comme une Mad Lib, un ensemble de phrases contenant des blancs. La base de données prendra cette requête et trouvera chaque ensemble d’instructions correspondantes qui remplissent correctement ces blancs. Ce qui rend SPARQL puissant, c’est la possibilité de créer des requêtes complexes qui référencent de nombreux :Term[variables]{#query-variable} à la fois.

## Exemples

- Lincoln (2015) [“Using SPARQL to Access Linked Open Data”](https://programminghistorian.org/en/lessons/retired/graph-databases-and-SPARQL) : La requête suivante indique à la base de données de rechercher tous valeurs de `?painting` qui complètent correctement le :Term[Resource Description Framework (RDF)]{#resource-description-framework} déclaration `<has medium><oil on canvas>` . `?painting` représente le(s) nœud(s) que la base de données renverra.

```sparql
  SELECT ?painting
  WHERE {
     ?painting <has medium> <oil on canvas> .
   }
```

- La requête suivante a une seconde variable : `?artist`. La base de données renverra toutes les combinaisons correspondantes de `?artist` et `?painting` qui remplissent ces deux déclarations.

```sparql
     SELECT ?artist ?painting
     WHERE {
        ?artist <has nationality> <Dutch> .
        ?painting <was created by> ?artist .
     }
```

- CWRC Linked Data (2022) [“CWRC SPARQL Endpoint”](https://yasgui.lincsproject.ca/#) : La requête SPARQL sur le lien ci-dessus renvoie toutes les personnes dans le LODset actuel de CWRC (actuellement la première extraction de l’ensemble de données d’Orlando sur l’écriture des femmes britanniques) nées au XIXe siècle, classées par date de naissance.

## Autres ressources

- Lincoln (2015) [“Using SPARQL to Access Linked Open Data”](https://programminghistorian.org/en/lessons/retired/graph-databases-and-SPARQL)
- [Semantic Queries (Wikipedia)](https://en.wikipedia.org/wiki/Semantic_query)
- [SPARQL (Wikipedia)](https://en.wikipedia.org/wiki/SPARQL)
- Wikibooks (2018) [“XQuery/SPARQL Tutorial”](https://en.wikibooks.org/wiki/XQuery/SPARQL_Tutorial)
