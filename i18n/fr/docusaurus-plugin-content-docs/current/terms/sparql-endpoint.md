---
id: sparql-endpoint
title: Point de terminaison SPARQL
definition: Emplacement sur Internet identifié par une URL (Uniform Resource Locator) et capable de recevoir et de traiter des requêtes SPARQL, permettant aux utilisateurs d’accéder à une collection de triplets.
---

Un point de terminaison SPARQL est un emplacement sur Internet, plus formellement décrit comme un point de présence sur un réseau HTTP, qui est identifié par une :Term[Uniform Resource Locator (URL)]{#uniform-resource-locator} et est capable de recevoir et de traiter :Term[Requêtes SPARQL]{#sparql-protocol-and-rdf-query-language}. Un utilisateur peut accéder à la collection de :Term[triples]{#triple} que vous avez dans une :Term[triplestore]{#triplestore} via un point de terminaison SPARQL : c’est la porte qui connecte les gens à vos données. Une clause SERVICE dans le corps d’une requête SPARQL peut également être utilisée pour appeler un point de terminaison SPARQL distant.

## Exemples

- [CWRC SPARQL Endpoint](https://yasgui.lincsproject.ca/#)
- [Wikidata List of SPARQL Endpoints](https://www.wikidata.org/wiki/Wikidata:Lists/SPARQL_endpoints)
- [W3C List of SPARQL Endpoints](https://www.w3.org/wiki/SparqlEndpoints)

## Autres ressources

- [SPARQL (Wikipedia)](https://en.wikipedia.org/wiki/SPARQL)
- Uyi Idehen (2018) [“What is a SPARQL Endpoint, and Why is it Important?”](https://medium.com/virtuoso-blog/what-is-a-sparql-endpoint-and-why-is-it-important-b3c9e6a20a8b)
