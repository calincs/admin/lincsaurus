---
sidebar_position: 7
title: "Conceptual Objects Application Profile"
description: "Represent conceptual objects"
---

## Purpose

To document how various facets of LINCS data are modelled, along with reference :Term[authorities]{#authority-file} for the populating :Term[vocabularies]{#vocabulary}. This will provide a basis for instruction for how to model data in a LINCS-compatible manner, as well as aid in navigation and discovery.

“Conceptual Objects” describes patterns that are unique or specific to representing information about conceptual objects, including bibliographic and digital records, and includes reference to their relationship to physical things.

This document introduces the concepts as used by LINCS, and are not complete definitions of the :Term[CIDOC CRM]{#cidoc-crm} ontology class or :Term[property]{#property} concepts. Consult [CIDOC CRM v. 7.1.1 documentation](https://cidoc-crm.org/Version/version-7.1.1) for full class descriptions and property descriptions.

## Acronyms

**Ontology Acronyms:**

* CRM - [CIDOC Conceptual Reference Model](http://cidoc-crm.org/)
* FRBRoo - [Object-Oriented Functional Requirements for Bibliographic Records](http://www.cidoc-crm.org/frbroo/home-0)
* LRMoo - [Object-Oriented Library Reference Model](http://www.cidoc-crm.org/frbroo/ModelVersion/lrmoo-f.k.a.-frbroo-v.0.7)
* CRMtex - [Model for the study of ancient texts](https://cidoc-crm.org/crmtex/)

**Vocabulary and Authority Acronyms:**

* AAT - [Getty Art & Architecture Thesaurus](https://www.getty.edu/research/tools/vocabularies/aat/)
* Event - [The LINCS Event Vocabulary](https://vocab.lincsproject.ca/Skosmos/event/en/)
* LINCS - LINCS minted entities
* LOC - [Library of Congress](https://id.loc.gov/)
* Wikidata - [Wikimedia Knowledge Base](https://www.wikidata.org/wiki/Wikidata:Main_Page)

## Bibliographic Records: WEMI, FRBRoo, and LRMoo

Bibliographic records are modeled using FRBRoo, which extends the sections of CIDOC CRM related to Conceptual Objects. Due to differences between FRBRoo and LRMoo and the expectation of the adoption of LRMoo when available, the majority of data are currently modeled using F2_Expression; the only exception to this is in the [Orlando](https://cwrc.ca/orlando) dataset where the data requires an additional level of complexity. This maintains the use of no classes planned for depreciation, while also providing a data model of minimal complexity as required by the data.

## Written and Ancient Texts: CRMtex

CRMtex is used for domain-specific requirements of written and ancient texts. Like FRBRoo, it extends sections of CIDOC CRM; CRMtex concerns itself with Physical as well as Conceptual Objects. LINCS primarily uses CRMtex to model physical specificities regarding written ancient texts. For more on this, see the [Written and Ancient Texts (CRMtex) section of the Physical Objects Application Profile](/docs/explore-lod/understand-lincs-data/application-profiles-main/physical-objects#written-and-ancient-texts-crmtex).

## Main Classes

<table>
  <tr>
    <td>
      <strong>Entity type</strong>
    </td>
    <td>
      <strong>Class</strong>
    </td>
    <td>
      <strong>Declaration Snippet (TTL)</strong>
    </td>
  </tr>
  <tr>
    <td>Bibliographic record (e.g., publication)</td>
    <td>frbroo:F2_Expression</td>
    <td>
      ```turtle
      <publication> a frbroo:F2_Expression ;
        rdfs:label “<publication_descriptor>” .
      ```
    </td>
  </tr>
  <tr>
    <td>Language</td>
    <td>crm:E56_Language</td>
    <td>
      ```turtle
      <language> a crm:E56_Language ;
        rdfs:label “<language>” .
      ```
    </td>
  </tr>
  <tr>
    <td>Linguistic object</td>
    <td>crm:E33_Linguistic_Object</td>
    <td>
      ```turtle
      <linguistic_object> a crm:E33_Linguistic_Object ;
        rdfs:label “<linguistic_object>” .
      ```
    </td>
  </tr>
  <tr>
    <td>Visual Item</td>
    <td>crm:E36_Visual_Item</td>
    <td>
      ```turtle
      <image> a crm:E36_Visual_Item ;
        rdfs:label “Image of <entity>” .
      ```
    </td>
  </tr>
  <tr>
    <td>Performance</td>
    <td>frbroo:F31_Performance</td>
    <td>
      ```turtle
      <performance> a frbroo:F31_Performance ;
        rdfs:label “<performance>” .
      ```
    </td>
  </tr>
  <tr>
    <td>Digital object</td>
    <td>crm:E73_Information_Object</td>
    <td>
      ```turtle
      <digital> a crm:E73_Information_Object .
      ```
    </td>
  </tr>
</table>

## Overview Diagram

Below is an image of the application profile overview diagram. Follow this link for a [zoomable, more readable version](https://drive.google.com/file/d/1e0s9BgpE8CJ5jUWsXc40_lJBHZCaDLsn/view?usp=share_link). The segments below align with the document sections.

![Application profile overview diagram.](/img/documentation/application-profile-conceptual-overview-(c-LINCS).jpg)

## Nodes

### Conceptual Texts, Images, and Ideas; Physical Objects

Conceptual objects comprise non-material products of our minds and other human produced data. They can exist on more than one carrier at the same time, such as paper, electronic signals, marks, audio media, paintings, photos, human memories, etc. Their existence ends when the last carrier and the last memory are lost. These carriers are physical things. Physical things consist of all persistent physical items with a relatively stable form, human-made or natural.

### Physical Carriers of Conceptual Things

Physical things can be carriers of conceptual elements: for example, a physical book carries its text, or a physical collection carries the intellectual contents of that collection.

![Application profile nodes](/img/documentation/application-profile-conceptual-nodes-(c-LINCS).png)

<table>
  <tr>
    <td rowspan="2">
      <strong>Pattern/Structure Values</strong>
    </td>
    <td>
      <strong>Definition</strong>
    </td>
    <td>
      This pattern declares that a physical thing serves as a carrier for
      conceptual things.
    </td>
  </tr>
  <tr>
    <td>
      <strong>Abstraction</strong>
    </td>
    <td>
      crm:E18_Physical Thing → crm:P128_carries →{" "}
      <strong>crm:E73_Information_Object</strong>
    </td>
  </tr>
  <tr>
    <td rowspan="3">
      <strong>Content Values</strong>
    </td>
    <td>
      <strong>Type of Value</strong>
    </td>
    <td>Uniform Resource Identifier (URI)</td>
  </tr>
  <tr>
    <td>
      <strong>Expected Value</strong>
    </td>
    <td>
      URI from project dataset, existing linked data authority, or minted by
      LINCS
    </td>
  </tr>
  <tr>
    <td>
      <strong>Format/Requirements for the Value</strong>
    </td>
    <td>URI (preferably dereferenceable)</td>
  </tr>
  <tr>
    <td rowspan="2">
      <strong>Case Examples</strong>
    </td>
    <td>
      <strong>Typical Example & Abstraction</strong>
    </td>
    <td>
      <p>
        The HistSex dataset states that the Northwest Lesbian and Gay History
        Project archive carries the intellectual contents of that archival
        collection.
      </p>
      <p>
        <code>
          &lt;lincs:Northwest_Lesbian_and_Gay_History_Project> →
          crm:P128_carries{" "}
          <strong>
            &lt;lincs:Northwest_Lesbian_and_Gay_History_Project_Contents>
          </strong>
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td>
      <strong>Edge Case Example & Abstraction</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Resource Links</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Discussion Elements Pertaining to This Pattern</strong>
    </td>
    <td>
      crm:E18_Physical Thing is not directly used in the data, but represents
      here subclasses such as crm:E22_Human-Made_Object,
      crm:E78_Curated_Holding, crmtex:T1_Written_Text, and
      crmtex:TX7_Written_Text_Fragment that are used for this pattern.
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Projects Following This Pattern</strong>
    </td>
    <td>Anthologia graeca, Canadian Centre for Ethnomusicology, HistSex</td>
  </tr>
</table>


**Pattern in TTL:**

```turtle
<object> a crm:E18_Physical_Thing ; 
    rdfs:label "<object>" ;
    crm:P128_carries <conceptual_thing> .

<conceptual_thing> a crm:E73_Information_Object ; 
    rdfs:label "<conceptual_thing>" .
```

### Transcribing Written Texts (CRMtex)

![Application profile transcribing text](/img/documentation/application-profile-conceptual-transcribingtext-(c-LINCS).png)

<table>
  <tr>
    <td rowspan="2">
      <strong>Pattern/Structure Values</strong>
    </td>
    <td>
      <strong>Definition</strong>
    </td>
    <td>This pattern declares that a written text has a transcription.</td>
  </tr>
  <tr>
    <td>
      <strong>Abstraction</strong>
    </td>
    <td>
      <p>
        <code>
          crmtex:TX7_Written_Text_Segment → crm:P16i_was_used_for →
          TX6_Transcription, crm:E65_Creation
          <br />→ crm:P94_has_created → **crm:E33_Linguistic_Object**
          <br />→ crm:P190_has_symbolic_content → rdfs:Literal
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td rowspan="3">
      <strong>Content Values</strong>
    </td>
    <td>
      <strong>Type of Value</strong>
    </td>
    <td>Uniform Resource Identifier (URI); literal value (text)</td>
  </tr>
  <tr>
    <td>
      <strong>Expected Value</strong>
    </td>
    <td>
      URI minted by LINCS; literal value (text) from Anthologia graeca dataset
    </td>
  </tr>
  <tr>
    <td>
      <strong>Format/Requirements for the Value</strong>
    </td>
    <td>URI (preferably dereferenceable); rdfs:literal</td>
  </tr>
  <tr>
    <td rowspan="2">
      <strong>Case Examples</strong>
    </td>
    <td>
      <strong>Typical Example & Abstraction</strong>
    </td>
    <td>
      The Anthologia graeca dataset declares that a passage was transcribed.
      <p>
        <code>
          &lt;lincs:passage/realization/1298> → crm:P16i_was_used_for →
          TX6_Transcription, crm:E65_Creation
          <br />→ crm:P94_has_created → <strong>&lt;lincs:text/4392></strong>
          <br />→ crm:P190_has_symbolic_content →{" "}
          <strong>
            “En jouant de la trompette, Marcus le fluet émit un tout petit
            souffle et, tout droit, tête première, il s’en alla dans l’Hadès!”
          </strong>
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td>
      <strong>Edge Case Example & Abstraction</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Resource Links</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Discussion Elements Pertaining to This Pattern</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Projects Following This Pattern</strong>
    </td>
    <td>Anthologia graeca</td>
  </tr>
</table>

**Pattern in TTL:**

```turtle
<written_text> a crmtex:TX1_Written_Text ;
    rdfs:label "<written_text>" ; 
    crm:P16i_was_used_for <transcription_event> .

<transcription_event> a crmtex:TX6_Transcription, crm:E65_Creation ;
    rdfs:label "Transcription of <written_text>" ;
    crm:P94_has_created <text> .

<text> a crm:E33_Linguistic_Object ;
    rdfs:label "Transcription text of <written_text>" ; 
    crm:P190_has_symbolic_content "<text>" .
```

### Talking about Entities: References to, and Representations of, Conceptual Things

For more on references to and representations of things, see the [Talking about Entities section of the Basic Patterns Application Profile](/docs/explore-lod/understand-lincs-data/application-profiles-main/basic-patterns#talking-about-entities).

#### Descriptions and Other Comments

Different types of references are differentiated by the P2_has_type → E55_Type pattern associated with them. For more on this, see the [Types section of the Basic Patterns Application Profile](/docs/explore-lod/understand-lincs-data/application-profiles-main/basic-patterns). For example:

* General description: `http://vocab.getty.edu/aat/300411780`
* Descriptive note: `http://vocab.getty.edu/aat/300435416`
* Comment or note: `http://vocab.getty.edu/aat/300027200`

![Application profile descriptions](/img/documentation/application-profile-conceptual-descriptions-(c-LINCS).png)

<table>
  <tr>
    <td rowspan="2">
      <strong>Pattern/Structure Values</strong>
    </td>
    <td>
      <strong>Definition</strong>
    </td>
    <td>This pattern declares that an entity has a note.</td>
  </tr>
  <tr>
    <td>
      <strong>Abstraction</strong>
    </td>
    <td>
      <p>
        <code>
          crm:E1_CRM_Entity → crm:P67i_is_referred_to_by →
          crm:E33_Linguistic_Object
          <br />→ crm:P2_has_type → <strong>crm:E55_Type</strong>
          <br />→ crm:P190_has_symbolic_content → <strong>rdfs:literal</strong>
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td rowspan="3">
      <strong>Content Values</strong>
    </td>
    <td>
      <strong>Type of Value</strong>
    </td>
    <td>Uniform Resource Identifier (URI); literal value (text)</td>
  </tr>
  <tr>
    <td>
      <strong>Expected Value</strong>
    </td>
    <td>
      URI from project dataset, existing linked data authority, or minted by
      LINCS; literal value from project dataset
    </td>
  </tr>
  <tr>
    <td>
      <strong>Format/Requirements for the Value</strong>
    </td>
    <td>URI (preferably dereferenceable); rdfs:literal</td>
  </tr>
  <tr>
    <td rowspan="2">
      <strong>Case Examples</strong>
    </td>
    <td>
      <strong>Typical Example & Abstraction</strong>
    </td>
    <td>
      The Canadian Centre for Ethnomusicology dataset states that there is a
      text that has a statement describing it.
      <p>
        <code>
          &lt;cce:pub/1970.4> → crm:P67i_is_referred_to_by →
          crm:E33_Linguistic_Object
          <br />→ crm:P2_has_type → <strong>aat:300411780 </strong>
          <br />→ crm:P190_has_symbolic_content →{" "}
          <strong>
            “Translations of 100 Ukranian wedding songs, and one musical
            transcription. Notes from interviews with Dan Chomlak (August 17,
            1970), Mrs. Shuchuk (December 11, 1970) and with Father D. Luchak
            (August 7, 1970). Miscellaneous field notes, lists of performers,
            and other commentary.”
          </strong>
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td>
      <strong>Edge Case Example & Abstraction</strong>
    </td>
    <td>
      The Map of Early Modern London Gazetteer states that St. Saviour
      (Southwark) is the main subject of a text.
      <p>
        <code>
          &lt;moeml:STSA1> → crm:P129i_is_subject_of → crm:E33_Linguistic_Object
          <br />→ crm:P2_has_type → <strong>&lt;lincs:description></strong>
          <br />→ crm:P190_has_symbolic_content →{" "}
          <strong>
            “St. Saviour (Southwark) dates back at least to 1106. It was
            originally known by the name St. Mary Overies, with Overies
            referring to its being over the Thames, that is, on its southern
            bank. After the period of the Dissolution, the church was
            rededicated and renamed St. Saviour (Sugden 335). St. Saviour
            (Southwark) is visible on the Agas map along New Rents street in
            Southwark. It is marked with the label S. Mary Owber.”
          </strong>
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Resource Links</strong>
    </td>
    <td>
      The Getty Research Institute. (2017, March 7).{" "}
      <em>Art & Architecture Thesaurus Online</em>.{" "}
      <a href="https://www.getty.edu/research/tools/vocabularies/aat">
        https://www.getty.edu/research/tools/vocabularies/aat/
      </a>
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Discussion Elements Pertaining to This Pattern</strong>
    </td>
    <td>
      When using E33_Linguistic_Object, use at least one (1) E55_Type on each
      instance specifying what kind of text it is.
      <p>
        This pattern can be used for any information item that discusses the
        entity in question. When seeking to say that the entity is not just
        discussed or referenced by the text statement, but is the main subject
        of it, P129i_is_subject_of can be used instead.
      </p>
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Projects Following This Pattern</strong>
    </td>
    <td>All projects</td>
  </tr>
</table>

**Pattern in TTL:**

```turtle
<entity> a crm:E1_CRM_Entity ; 
    rdfs:label "<entity>" ;
    crm:P67i_is_referred_to_by <entity_note> .

<entity_note> a crm:E33_Linguistic_Object ; 
    rdfs:label "<type> statement for <entity>" ;
    crm:P2_has_type <type> ; 
    crm:P190_has_symbolic_content “<note>” .

<type> a crm:E55_Type ; 
    rdfs:label "<type>" .
```

#### Visual Representation

For more on how to say that an object has a visual representation, see the [Visual Representations of an Entity section of the Basic Patterns Application Profile](/docs/explore-lod/understand-lincs-data/application-profiles-main/basic-patterns#visual-representations-of-an-entity).

![Application profile visual representation](/img/documentation/application-profile-conceptual-visualrepresentation-(c-LINCS).png)

<table>
  <tr>
    <td rowspan="2">
      <strong>Pattern/Structure Values</strong>
    </td>
    <td>
      <strong>Definition</strong>
    </td>
    <td>This pattern declares that an entity is represented by an image.</td>
  </tr>
  <tr>
    <td>
      <strong>Abstraction</strong>
    </td>
    <td>
      crm:E1_CRM_Entity → crm:P138i_is_represented_by →{" "}
      <strong>crm:E36_Visual_Item</strong>
    </td>
  </tr>
  <tr>
    <td rowspan="3">
      <strong>Content Values</strong>
    </td>
    <td>
      <strong>Type of Value</strong>
    </td>
    <td>Uniform Resource Identifier (URI)</td>
  </tr>
  <tr>
    <td>
      <strong>Expected Value</strong>
    </td>
    <td>
      URI from project dataset, existing linked data authority, or minted by
      LINCS
    </td>
  </tr>
  <tr>
    <td>
      <strong>Format/Requirements for the Value</strong>
    </td>
    <td>URI, preferably dereferenceable</td>
  </tr>
  <tr>
    <td rowspan="2">
      <strong>Case Examples</strong>
    </td>
    <td>
      <strong>Typical Example & Abstraction</strong>
    </td>
    <td>
      The AdArchive dataset states that there is an advertisement that has an
      image which is a visual representation of it.
      <p>
        <code>
          &lt;lincs:LRK8dZhmUxZ> → crm:P138i_has_representation →{" "}
          <strong>
            &lt;https://iiif.archivelab.org/iiif/heresies_04$127/1308,1791,1067,1362/full/0/default.jpg>
          </strong>
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td>
      <strong>Edge Case Example & Abstraction</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Resource Links</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Discussion Elements Pertaining to This Pattern</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Projects Following This Pattern</strong>
    </td>
    <td>
      AdArchive, Anthologia graeca, HistSex, University of Saskatchewan Art
      Gallery
    </td>
  </tr>
</table>

**Pattern in TTL:**

```turtle
<entity> a crm:E1_CRM_Entity ; 
    rdfs:label "<entity>" ;
    crm:P138i_has_representation <image> .

<image> a crm:E36_Visual_Item ; 
    rdfs:label "Image of <entity>" .

```

### Identifiers

For more on identifiers, see the [Identifiers section of the Basic Patterns Application Profile](/docs/explore-lod/understand-lincs-data/application-profiles-main/basic-patterns#identifiers).

#### Unique Identifiers (e.g., Call Numbers)

Different types of identifiers are differentiated by the P2_has_type → E55_Type pattern associated with them. For more on this, see the [Types section of the Basic Patterns Application Profile)](/docs/explore-lod/understand-lincs-data/application-profiles-main/basic-patterns#types). For example:

> Call number: `http://vocab.getty.edu/aat/300311706`

![Application profile identifiers](/img/documentation/application-profile-conceptual-identifiers-(c-LINCS).png)

<table>
  <tr>
    <td rowspan="2">
      <strong>Pattern/Structure Values</strong>
    </td>
    <td>
      <strong>Definition</strong>
    </td>
    <td>This pattern declares that an entity has an identifier.</td>
  </tr>
  <tr>
    <td>
      <strong>Abstraction</strong>
    </td>
    <td>
      <code>
        crm:E1_CRM_Entity → crm:P1_is_identified_by → crm:E42_Identifier
        <br />→ crm:P2_has_type → <strong>crm:E55_Type[aat:300404012]</strong>
        <br />→ crm:P190_has_symbolic_content → <strong>rdfs:literal</strong>
      </code>
    </td>
  </tr>
  <tr>
    <td rowspan="3">
      <strong>Content Values</strong>
    </td>
    <td>
      <strong>Type of Value</strong>
    </td>
    <td>Uniform Resource Identifier (URI); literal value (text)</td>
  </tr>
  <tr>
    <td>
      <strong>Expected Value</strong>
    </td>
    <td>
      URI from project dataset, existing linked data authority, or minted by
      LINCS; literal value from project dataset
    </td>
  </tr>
  <tr>
    <td>
      <strong>Format/Requirements for the Value</strong>
    </td>
    <td>URI (preferably dereferenceable); rdfs:literal</td>
  </tr>
  <tr>
    <td rowspan="2">
      <strong>Case Examples</strong>
    </td>
    <td>
      <strong>Typical Example & Abstraction</strong>
    </td>
    <td>
      The Canadian Centre for Ethnomusicology dataset states that there is a
      publication that is identified by the call number “2020.008.008.”
      <p>
        <code>
          &lt;cce:pub/P1970.4> → crm:P1_is_identified_by → crm:E42_Identifier
          <br />→ crm:P2_has_type → **&lt;aat:300404012>, &lt;aat:300311706>, &lt;lincs:cce_identifier>**
          <br />→ crm:P190_has_symbolic_content → <strong>“P1970.4”</strong>
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td>
      <strong>Edge Case Example & Abstraction</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Resource Links</strong>
    </td>
    <td>
      The Getty Research Institute. (2017, March 7).{" "}
      <em>Art & Architecture Thesaurus Online</em>.{" "}
      <a href="https://www.getty.edu/research/tools/vocabularies/aat/">
        https://www.getty.edu/research/tools/vocabularies/aat/
      </a>
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Discussion Elements Pertaining to This Pattern</strong>
    </td>
    <td>
      Use at least two (2) E55_Types on each identifier: generic (from existing
      linked data vocabulary) or project-specific (minted for project). Always
      use the{" "}
      <a href="http://vocab.getty.edu/aat/300404012">
        Getty AAT term for unique identifiers
      </a>{" "}
      for generic identifiers. Additional are optional for precision (e.g., the{" "}
      <a href="http://vocab.getty.edu/aat/300312355">
        Getty AAT term for accession numbers
      </a>
      ).
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Projects Following This Pattern</strong>
    </td>
    <td>All projects</td>
  </tr>
</table>

**Pattern in TTL:**

```turtle
<entity> a crm:E1_CRM_Entity ;  
    rdfs:label "<entity>" ;
    crm:P1_is_identified_by <entity_identifier> .

<entity_identifier> a crm:E42_Identifier ;
    rdfs:label "Unique identifier of <entity>" 
    crm:P2_has_type <http://vocab.getty.edu/aat/300404012>, 
        <project_identifier_type> ; 
    crm:P190_has_symbolic_content "<entity_identifier>" .

<http://vocab.getty.edu/aat/300404012> a crm:E55_Type ; 
    rdfs:label "unique identifiers" . 

<project_identifier_type> a crm:E55_Type ; 
    rdfs:label "<project> identifiers" .
```

#### Titles

Different types of identifiers are differentiated by the P2_has_type → E55_Type pattern associated with them. For more on this, see the [Types section of the Basic Patterns Application Profile)](/docs/explore-lod/understand-lincs-data/application-profiles-main/basic-patterns#types). For example:

* Title: `http://vocab.getty.edu/aat/300417193`
* Variant title: `http://id.loc.gov/ontologies/bibframe/VariantTitle`
* Short title: `http://purl.org/ontology/bibo/shortTitle`
* Subtitle: `http://www.wikidata.org/entity/Q1135389`

![Application profile titles](/img/documentation/application-profile-conceptual-titles-(c-LINCS).png)

<table>
  <tr>
    <td rowspan="2">
      <strong>Pattern/Structure Values</strong>
    </td>
    <td>
      <strong>Definition</strong>
    </td>
    <td>This pattern declares that an entity has a linguistic identifier.</td>
  </tr>
  <tr>
    <td>
      <strong>Abstraction</strong>
    </td>
    <td>
      <p>
        <code>
          crm:E1_CRM_Entity → crm:P1_is_identified_by →
          crm:E33_E41_Linguistic_Appellation
          <br />
          crm:P2_has_type → <strong>crm:E55_Type</strong>
          <br />
          crm:P190_has_symbolic_content → <strong>rdfs:literal</strong>
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td rowspan="3">
      <strong>Content Values</strong>
    </td>
    <td>
      <strong>Type of Value</strong>
    </td>
    <td>Uniform Resource Identifier (URI); literal value (text)</td>
  </tr>
  <tr>
    <td>
      <strong>Expected Value</strong>
    </td>
    <td>
      URI from project dataset, existing linked data authority, or minted by
      LINCS; literal value from project dataset
    </td>
  </tr>
  <tr>
    <td>
      <strong>Format/Requirements for the Value</strong>
    </td>
    <td>URI (preferably dereferenceable); rdfs:literal</td>
  </tr>
  <tr>
    <td rowspan="2">
      <strong>Case Examples</strong>
    </td>
    <td>
      <strong>Typical Example & Abstraction</strong>
    </td>
    <td>
      The Canadian Centre for Ethnomusicology dataset states that there is an
      object with the title “Bibliography and Bibliographic Essay about
      Traditional Music in Ghana.”
      <p>
        <code>
          &lt;cce:pub/P1995.15> → crm:P1_is_identified_by →
          crm:E33_E41_Linguistic_Appellation
          <br />→ crm:P2_has_type → <strong>&lt;aat:300417193></strong>
          <br />→ crm:P190_has_symbolic_content →{" "}
          <strong>
            “Bibliography and Bibliographic Essay about Traditional Music in
            Ghana”
          </strong>
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td>
      <strong>Edge Case Example & Abstraction</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Resource Links</strong>
    </td>
    <td>
      The Getty Research Institute. (2017, March 7).{" "}
      <em>Art & Architecture Thesaurus Online</em>.{" "}
      <a href="https://www.getty.edu/research/tools/vocabularies/aat/">
        https://www.getty.edu/research/tools/vocabularies/aat/
      </a>
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Discussion Elements Pertaining to This Pattern</strong>
    </td>
    <td>
      Use at least one (1) E55_Type on each linguistic identifier specifying
      what it is (e.g., name, title, etc.).
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Projects Following This Pattern</strong>
    </td>
    <td> All projects</td>
  </tr>
</table>

**Pattern in TTL:**

```turtle
<entity> a crm:E1_CRM_Entity ; 
    rdfs:label "<entity>" ; 
    crm:P1_is_identified_by <entity_name_or_title> .

<entity_name_or_title> a crm:E33_E41_Linguistic_Appellation ; 
    rdfs:label "Linguistic identifier of <entity>" ; 
    crm:P2_has_type <type> ; 
    crm:P190_has_symbolic_content "<entity_name_or_title>" .

<type> a crm:E55_Type ; 
    rdfs:label "<type>" .
```

### Categories and Classifications

For more on types, categorization, and classification, as well as vocabularies, see the [Types section of the Basic Patterns Application Profile)](/docs/explore-lod/understand-lincs-data/application-profiles-main/basic-patterns#types).

![Application profile categories](/img/documentation/application-profile-conceptual-categories-(c-LINCS).png)

<table>
  <tr>
    <td rowspan="2">
      <strong>Pattern/Structure Values</strong>
    </td>
    <td>
      <strong>Definition</strong>
    </td>
    <td>
      This pattern declares that a conceptual object has a classification.
    </td>
  </tr>
  <tr>
    <td>
      <strong>Abstraction</strong>
    </td>
    <td>
      crm:E28_Conceptual_Object → crm:P2_has_type →{" "}
      <strong>crm:E55_Type</strong>
    </td>
  </tr>
  <tr>
    <td rowspan="3">
      <strong>Content Values</strong>
    </td>
    <td>
      <strong>Type of Value</strong>
    </td>
    <td>Uniform Resource Identifier (URI)</td>
  </tr>
  <tr>
    <td>
      <strong>Expected Value</strong>
    </td>
    <td>
      URI from project dataset, existing linked data authority, or minted by
      LINCS
    </td>
  </tr>
  <tr>
    <td>
      <strong>Format/Requirements for the Value</strong>
    </td>
    <td>URI (preferably dereferenceable)</td>
  </tr>
  <tr>
    <td rowspan="2">
      <strong>Case Examples</strong>
    </td>
    <td>
      <strong>Typical Example & Abstraction</strong>
    </td>
    <td>
      The AdArchive dataset states that there is a text in Semiotext(e) Vol. 3
      No. 2 with the type “fiction”.
      <p>
        <code>
          &lt;lincs:KHPYoAIuzdj> → crm:P2_has_type →{" "}
          <strong>&lt;wikidata:Q8253></strong>
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td>
      <strong>Edge Case Example & Abstraction</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Resource Links</strong>
    </td>
    <td>
      The Getty Research Institute. (2017, March 7).{" "}
      <em>Art & Architecture Thesaurus Online</em>.{" "}
      <a href="https://www.getty.edu/research/tools/vocabularies/aat/">
        https://www.getty.edu/research/tools/vocabularies/aat/
      </a>
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Discussion Elements Pertaining to This Pattern</strong>
    </td>
    <td>
      This pattern connects entities to vocabularies. For more on this, see the
      Vocabularies section of the Basic Patterns Application Profile.
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Projects Following This Pattern</strong>
    </td>
    <td>All projects</td>
  </tr>
</table>

**Pattern in TTL:**

```turtle
<conceptual_object> a crm:E28_Conceptual_Object ;
    rdfs:label "<conceptual_object>" ;
    crm:P2_has_type <type>.

<type> a crm:E55_Type ;
    rdfs:label "<type>".
```

### Creation

A creation date adds temporal information to the creation event. For creation date, see the [Time-Spans of Activities section of the Basic Patterns Application Profile](/docs/explore-lod/understand-lincs-data/application-profiles-main/basic-patterns#time-spans-of-activities).

A creation place adds location information to the creation event. For creation place, see the [Locations of Activities section of the Basic Patterns Application Profile](/docs/explore-lod/understand-lincs-data/application-profiles-main/basic-patterns#locations-of-activities).

For information about how to say that an actor was involved in or carried out an activity, see the [Roles section of the Basic Patterns Application Profile](/docs/explore-lod/understand-lincs-data/application-profiles-main/basic-patterns#roles).

![Application profile creation](/img/documentation/application-profile-conceptual-creation-(c-LINCS).png)

<table>
  <tr>
    <td rowspan="2">
      <strong>Pattern/Structure Values</strong>
    </td>
    <td>
      <strong>Definition</strong>
    </td>
    <td>This pattern declares that a conceptual object was created.</td>
  </tr>
  <tr>
    <td>
      <strong>Abstraction</strong>
    </td>
    <td>
      <code>
        crm:E28_Conceptual_Object → crm:P94i_was_created_by →{" "}
        <strong>crm:E65_Creation</strong>
      </code>
    </td>
  </tr>
  <tr>
    <td rowspan="3">
      <strong>Content Values</strong>
    </td>
    <td>
      <strong>Type of Value</strong>
    </td>
    <td>Uniform Resource Identifier (URI)</td>
  </tr>
  <tr>
    <td>
      <strong>Expected Value</strong>
    </td>
    <td>
      URI from project dataset, existing linked data authority, or minted by
      LINCS
    </td>
  </tr>
  <tr>
    <td>
      <strong>Format/Requirements for the Value</strong>
    </td>
    <td>URI (preferably dereferenceable)</td>
  </tr>
  <tr>
    <td rowspan="2">
      <strong>Case Examples</strong>
    </td>
    <td>
      <strong>Typical Example & Abstraction</strong>
    </td>
    <td>
      The AdArchive dataset states that there is a text in Semiotext(e) Vol. 3
      No. 2 that was created through a creation event.
      <p>
        <code>
          &lt;lincs:KHPYoAIuzdj> → crm:P94i_was_created_by →{" "}
          <strong>&lt;lincs:4u1GJIZMP6H></strong>
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td>
      <strong>Edge Case Example & Abstraction</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Resource Links</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Discussion Elements Pertaining to This Pattern</strong>
    </td>
    <td>
      When using E65_Creation, use at least one (1) E55_Type on each instance
      specifying what kind of creation activity it is.
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Projects Following This Pattern</strong>
    </td>
    <td>
      AdArchive, Anthologia graeca, Canadian Centre for Ethnomusicology,
      HistSex, Yellow Nineties
    </td>
  </tr>
</table>

**Pattern in TTL:**

```turtle
<conceptual_object> a crm:E28_Conceptual_Object ;
    rdfs:label "<conceptual_object>" ;
    crm:P94i_was_created_by <creation_event> .

<creation_event> a crm:E65_Creation ; 
    rdfs:label "Creation event of <object>" ; 
    crm:P2_has_type <type> . 

<type> a crm:E55_Type ; 
    rdfs:label "<type>" .
```
### Groups and Other Part-Whole Relationships

#### Number of Conceptual Parts

![Application profile number of conceptual parts](/img/documentation/application-profile-conceptual-number-(c-LINCS).png)

<table>
  <tr>
    <td rowspan="2">
      <strong>Pattern/Structure Values</strong>
    </td>
    <td>
      <strong>Definition</strong>
    </td>
    <td>
      This pattern declares that a conceptual object has a number of conceptual
      parts.
    </td>
  </tr>
  <tr>
    <td>
      <strong>Abstraction</strong>
    </td>
    <td>
      <p>
        <code>
          crm:E28_Conceptual_Object → crm:P43_has_dimension → crm:E54_Dimension
          <br />→ crm:P2_has_type → <strong>crm:E55_Type</strong>
          <br />→ crm:P90_has_value →<strong> rdfs:literal</strong>
          <br />→ crm:P91_has_unit → <strong>crm:E58_Measurement_Unit</strong>
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td rowspan="3">
      <strong>Content Values</strong>
    </td>
    <td>
      <strong>Type of Value</strong>
    </td>
    <td>Uniform Resource Identifier (URI); literal value (numeric)</td>
  </tr>
  <tr>
    <td>
      <strong>Expected Value</strong>
    </td>
    <td>
      URI from project dataset, existing linked data authority, or minted by
      LINCS; literal value from project dataset
    </td>
  </tr>
  <tr>
    <td>
      <strong>Format/ Requirements for the Value</strong>
    </td>
    <td>URI (preferably dereferenceable); rdfs:literal</td>
  </tr>
  <tr>
    <td rowspan="2">
      <strong>Case Examples</strong>
    </td>
    <td>
      <strong>Typical Example & Abstraction</strong>
    </td>
    <td>
      The Canadian Centre for Ethnomusicology dataset states that there is a
      recording with two tracks.
      <p>
        <code>
          &lt;cce:CCE2000.2.71> → crm:P43_has_dimension → crm:E54_Dimension
          <br />→ crm:P2_has_type → <strong>&lt;wikidata:Q7302866></strong>
          <br />→ crm:P90_has_value →<strong> “2”</strong>
          <br />→ crm:P91_has_unit → <strong>&lt;wikidata:Q614112></strong>
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td>
      <strong>Edge Case Example & Abstraction</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Resource Links</strong>
    </td>
    <td>
      The Wikimedia Foundation. (2021). <em>Wikidata</em>.{" "}
      <a href="https://www.wikidata.org/wiki/Wikidata:Main_Page">
        https://www.wikidata.org/
      </a>
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Discussion Elements Pertaining to This Pattern</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Projects Following This Pattern</strong>
    </td>
    <td>Canadian Centre for Ethnomusicology</td>
  </tr>
</table>

**Pattern in TTL:**

```turtle
<conceptual_object> a crm:E28_Conceptual_Object ;
    rdfs:label "<conceptual_object>" ; 
    crm:P43_has_dimension <dimension> . 

<dimension> a crm:E54_Dimension ; 
    rdfs:label "<dimension_type> of <conceptual_object>" ; 
    crm:P2_has_type <dimension_type> ; 
    crm:P90_has_value <rdfs:literal> ; 
    crm:P91_has_unit <measurement_unit> .

<dimension_type> a crm:E55_Type ; 
    rdfs:label "<dimension_type>" .

<measurement_unit> a crm:E58_Measurement_Unit ; 
    rdfs:label "<measurement_unit>" .
```

#### Conceptual Objects as Parts of other Conceptual Objects

![Application profile conceptual objects as parts of other conceptual objects](/img/documentation/application-profile-conceptual-partofother-(c-LINCS).png)

<table>
  <tr>
    <td rowspan="2">
      <strong>Pattern/Structure Values</strong>
    </td>
    <td>
      <strong>Definition</strong>
    </td>
    <td>
      This pattern declares that a conceptual object is part of another
      conceptual object.
    </td>
  </tr>
  <tr>
    <td>
      <strong>Abstraction</strong>
    </td>
    <td>
      <code>
        crm:E89_Propositional_Object → crm:P148i_is_component_of →
        crm:E89_Propositional_Object
      </code>
    </td>
  </tr>
  <tr>
    <td rowspan="3">
      <strong>Content Values</strong>
    </td>
    <td>
      <strong>Type of Value</strong>
    </td>
    <td>Uniform Resource Identifier (URI)</td>
  </tr>
  <tr>
    <td>
      <strong>Expected Value</strong>
    </td>
    <td>
      URI from project dataset, existing linked data authority, or minted by
      LINCS
    </td>
  </tr>
  <tr>
    <td>
      <strong>Format/Requirements for the Value</strong>
    </td>
    <td>URI (preferably dereferenceable)</td>
  </tr>
  <tr>
    <td rowspan="2">
      <strong>Case Examples</strong>
    </td>
    <td>
      <strong>Typical Example & Abstraction</strong>
    </td>
    <td>
      The AdArchive dataset states that there is an article that is part of an
      issue.
      <p>
        <code>
          &lt;lincs:LNsuJtPxtq2> → crm:P148i_is_component_of →{" "}
          <strong>&lt;lincs:GZuqK657aSU></strong>
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td>
      <strong>Edge Case Example & Abstraction</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Resource Links</strong>
    </td>
    <td>
      The Getty Research Institute. (2017, March 7).{" "}
      <em>Art & Architecture Thesaurus Online</em>.{" "}
      <a href="https://www.getty.edu/research/tools/vocabularies/aat/">
        https://www.getty.edu/research/tools/vocabularies/aat/
      </a>
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Discussion Elements Pertaining to This Pattern</strong>
    </td>
    <td>
      crm:E89_Propositional_Object is a subclass of crm:E28_Conceptual_Object.
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Projects Following This Pattern</strong>
    </td>
    <td>
      AdArchive, Anthologia graeca, Canadian Centre for Ethnomusicology,
      HistSex
    </td>
  </tr>
</table>

**Pattern in TTL:**

```turtle
<conceptual_object_part> a crm:E89_Propositional_Object ;
    rdfs:label "<conceptual_object_part>" ; 
    crm:P148i_is_component_of <conceptual_object_whole> . 

<conceptual_object_whole> a crm:E89_Propositional_Object ;
    rdfs:label "<conceptual_object_whole>" .
```

#### Linguistic Objects as Parts of Conceptual Objects

![Application profile linguistic objects](/img/documentation/application-profile-conceptual-linguistic-(c-LINCS).png)

<table>
  <tr>
    <td rowspan="2">
      <strong>Pattern/Structure Values</strong>
    </td>
    <td>
      <strong>Definition</strong>
    </td>
    <td>
      This pattern declares that a conceptual object incorporates a linguistic
      component.
    </td>
  </tr>
  <tr>
    <td>
      <strong>Abstraction</strong>
    </td>
    <td>
      <code>
        crm:E73_Information_Object → crm:P165_incorporates →
        crm:E33_Linguistic_Object
      </code>
    </td>
  </tr>
  <tr>
    <td rowspan="3">
      <strong>Content Values</strong>
    </td>
    <td>
      <strong>Type of Value</strong>
    </td>
    <td>Uniform Resource Identifier (URI)</td>
  </tr>
  <tr>
    <td>
      <strong>Expected Value</strong>
    </td>
    <td>
      URI from project dataset, existing linked data authority, or minted by
      LINCS
    </td>
  </tr>
  <tr>
    <td>
      <strong>Format/Requirements for the Value</strong>
    </td>
    <td>URI (preferably dereferenceable)</td>
  </tr>
  <tr>
    <td rowspan="2">
      <strong>Case Examples</strong>
    </td>
    <td>
      <strong>Typical Example & Abstraction</strong>
    </td>
    <td>
      The AdArchive dataset states that there is an advertisement that
      incorporates a text (as advertising copy).
      <p>
        <code>
          &lt;lincs:LRK8dZhmUxZ> → crm:P165_incorporates →{" "}
          <strong>&lt;lincs:PHP16y4evtg></strong>
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td>
      <strong>Edge Case Example & Abstraction</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Resource Links</strong>
    </td>
    <td>
      The Getty Research Institute. (2017, March 7).{" "}
      <em>Art & Architecture Thesaurus Online</em>.{" "}
      <a href="https://www.getty.edu/research/tools/vocabularies/aat/">
        https://www.getty.edu/research/tools/vocabularies/aat/
      </a>
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Discussion Elements Pertaining to This Pattern</strong>
    </td>
    <td>
      crm:E73_Information_Object is a subclass of crm:E28_Conceptual_Object.
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Projects Following This Pattern</strong>
    </td>
    <td>AdArchive</td>
  </tr>
</table>

**Pattern in TTL:**

```turtle
<conceptual_object> a crm:E73_Information_Object ;
    rdfs:label "<conceptual_object>" ; 
    crm:P165_incorporates <text> . 

<text> a crm:E33_Linguistic_Object ;
    rdfs:label "<text>" .
```
### Contents of a Work

#### “Aboutness” and Content Subjects

![Application profile aboutness](/img/documentation/application-profile-conceptual-aboutness-(c-LINCS).png)

<table>
  <tr>
    <td rowspan="2">
      <strong>Pattern/Structure Values</strong>
    </td>
    <td>
      <strong>Definition</strong>
    </td>
    <td>This pattern declares that a conceptual object has a main subject.</td>
  </tr>
  <tr>
    <td>
      <strong>Abstraction</strong>
    </td>
    <td>
      <code>
        crm:E89_Propositional_Object → crm:P129_is_about →{" "}
        <strong>crm:E1_CRM_Entity</strong>
      </code>
    </td>
  </tr>
  <tr>
    <td rowspan="3">
      <strong>Content Values</strong>
    </td>
    <td>
      <strong>Type of Value</strong>
    </td>
    <td>Uniform Resource Identifier (URI); literal value (text)</td>
  </tr>
  <tr>
    <td>
      <strong>Expected Value</strong>
    </td>
    <td>
      URI from project dataset, existing linked data authority, or minted by
      LINCS; literal value from project dataset
    </td>
  </tr>
  <tr>
    <td>
      <strong>Format/Requirements for the Value</strong>
    </td>
    <td>URI (preferably dereferenceable); rdfs:literal</td>
  </tr>
  <tr>
    <td rowspan="2">
      <strong>Case Examples</strong>
    </td>
    <td>
      <strong>Typical Example & Abstraction</strong>
    </td>
    <td>
      The AdArchive dataset states that an advertisement is about the journal
      that it is advertising.
      <p>
        <code>
          &lt;lincs:LYKoOOxS1EP> → crm:P129_is_about →{" "}
          <strong>&lt;wikidata:Q1404511></strong>
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td>
      <strong>Edge Case Example & Abstraction</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Resource Links</strong>
    </td>
    <td>
      Library of Congress. (2021, July 26).{" "}
      <em>Library of Congress Catalog Works</em>.{" "}
      <a href="https://id.loc.gov/resources/works.html">
        https://id.loc.gov/resources/works.html
      </a>
      <p>
        The Wikimedia Foundation. (2021). <em>Wikidata</em>.{" "}
        <a href="https://www.wikidata.org/wiki/Wikidata:Main_Page">
          https://www.wikidata.org/
        </a>
      </p>
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Discussion Elements Pertaining to This Pattern</strong>
    </td>
    <td>
      crm:E89_Propositional_Object is a subclass of crm:E28_Conceptual_Object.
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Projects Following This Pattern</strong>
    </td>
    <td>AdArchive, Anthologia graeca, HistSex, Orlando</td>
  </tr>
</table>

**Pattern in TTL:**

```turtle
<conceptual_object> a crm:E89_Propositional_Object ; 
    rdfs:label "<conceptual_object>" ;
    crm:P129_is_about <main_subject> .

<main_subject> a crm:E1_CRM_Entity; 
    rdfs:label "<main_subject>" .
```

#### Referring to Other Entities

![Application profile referring to other entities](/img/documentation/application-profile-conceptual-referentities-(c-LINCS).png)

<table>
  <tr>
    <td rowspan="2">
      <strong>Pattern/Structure Values</strong>
    </td>
    <td>
      <strong>Definition</strong>
    </td>
    <td>This pattern declares that a conceptual object refers to things.</td>
  </tr>
  <tr>
    <td>
      <strong>Abstraction</strong>
    </td>
    <td>
      <code>
        crm:E89_Propositional_Object → crm:P67_refers_to →{" "}
        <strong>crm:E1_CRM_Entity</strong>
      </code>
    </td>
  </tr>
  <tr>
    <td rowspan="3">
      <strong>Content Values</strong>
    </td>
    <td>
      <strong>Type of Value</strong>
    </td>
    <td>Uniform Resource Identifier (URI)</td>
  </tr>
  <tr>
    <td>
      <strong>Expected Value</strong>
    </td>
    <td>
      URI from project dataset, existing linked data authority, or minted by
      LINCS
    </td>
  </tr>
  <tr>
    <td>
      <strong>Format/Requirements for the Value</strong>
    </td>
    <td>URI (preferably dereferenceable)</td>
  </tr>
  <tr>
    <td rowspan="2">
      <strong>Case Examples</strong>
    </td>
    <td>
      <strong>Typical Example & Abstraction</strong>
    </td>
    <td>
      The HistSex dataset states that a publication refers to time periods and
      locations.
      <p>
        <code>
          &lt;histsex:The_Question_of_Gender__Joan_W._Scott_s_Critical_Feminism>
          → crm:P67_refers_to →{" "}
          <strong>&lt;locsubjects:sh85139024>, &lt;geonames:6252001></strong>
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td>
      <strong>Edge Case Example & Abstraction</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Resource Links</strong>
    </td>
    <td>
      Library of Congress. (2021, August 2).{" "}
      <em>Library of Congress Subject Headings</em>.{" "}
      <a href="https://id.loc.gov/authorities/subjects.html">
        https://id.loc.gov/authorities/subjects.html
      </a>
      <p>
        Unxos GmbH. (2020, March 8). <em>GeoNames</em>.{" "}
        <a href="https://www.geonames.org/about.html">
          https://www.geonames.org/about.html
        </a>
      </p>
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Discussion Elements Pertaining to This Pattern</strong>
    </td>
    <td>
      crm:E89_Propositional_Object is a subclass of crm:E28_Conceptual_Object.
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Projects Following This Pattern</strong>
    </td>
    <td>
      Anthologia graeca, Canadian Centre for Ethnomusicology, HistSex, Orlando
    </td>
  </tr>
</table>

**Pattern in TTL:**

```turtle
<conceptual_object> a crm:E89_Propositional_Object ; 
    rdfs:label "<conceptual_object>" ;
    crm:P67_refers_to <entity> .

<entity> a crm:E1_CRM_Entity; 
    rdfs:label "<entity>" .
```

#### Text Contents

![Application profile text contents](/img/documentation/application-profile-conceptual-textcontents-(c-LINCS).png)

<table>
  <tr>
    <td rowspan="2">
      <strong>Pattern/Structure Values</strong>
    </td>
    <td>
      <strong>Definition</strong>
    </td>
    <td>
      This pattern declares that a linguistic object has specific contents.
    </td>
  </tr>
  <tr>
    <td>
      <strong>Abstraction</strong>
    </td>
    <td>
      crm:E33_Linguistic_Object → crm:P190_has_symbolic_content →{" "}
      <strong>rdfs:literal</strong>
    </td>
  </tr>
  <tr>
    <td rowspan="3">
      <strong>Content Values</strong>
    </td>
    <td>
      <strong>Type of Value</strong>
    </td>
    <td>Literal value (text)</td>
  </tr>
  <tr>
    <td>
      <strong>Expected Value</strong>
    </td>
    <td>Literal value from project dataset</td>
  </tr>
  <tr>
    <td>
      <strong>Format/Requirements for the Value</strong>
    </td>
    <td>rdfs:literal</td>
  </tr>
  <tr>
    <td rowspan="2">
      <strong>Case Examples</strong>
    </td>
    <td>
      <strong>Typical Example & Abstraction</strong>
    </td>
    <td>
      The Canadian Centre for Ethnomusicology dataset states that there is a
      title that reads “Bibliography and Bibliographic Essay about Traditional
      Music in Ghana”.
      <p>
        <code>
          &lt;cce:title/Bibliography_and_Bibliographic_Essay_about_Traditional_Music_in_Ghana>
          → crm:P190_has_symbolic_content →{" "}
          <strong>
            “Bibliography and Bibliographic Essay about Traditional Music in
            Ghana”
          </strong>
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td>
      <strong>Edge Case Example & Abstraction</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Resource Links</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Discussion Elements Pertaining to This Pattern</strong>
    </td>
    <td>
      crm:E33_Linguistic_Object is a subclass of crm:E28_Conceptual_Object.
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Projects Following This Pattern</strong>
    </td>
    <td>All projects</td>
  </tr>
</table>

**Pattern in TTL:**

```turtle
<text> a crm:E33_Linguistic_Object ;
    rdfs:label "<text>"
    crm:P190_has_symbolic_content "<text_content>" .
```

#### Language

![Application profile language](/img/documentation/application-profile-conceptual-language-(c-LINCS).png)

<table>
  <tr>
    <td rowspan="2">
      <strong>Pattern/ Structure Values</strong>
    </td>
    <td>
      <strong>Definition</strong>
    </td>
    <td>
      This pattern declares that a linguistic object is expressed in a specific
      language.
    </td>
  </tr>
  <tr>
    <td>
      <strong>Abstraction</strong>
    </td>
    <td>
      <code>
        crm:E33_Linguistic_Object → crm:P72_has_language →{" "}
        <strong>crm:E56_Language</strong>
      </code>
    </td>
  </tr>
  <tr>
    <td rowspan="3">
      <strong>Content Values</strong>
    </td>
    <td>
      <strong>Type of Value</strong>
    </td>
    <td>Uniform Resource Identifier (URI)</td>
  </tr>
  <tr>
    <td>
      <strong>Expected Value</strong>
    </td>
    <td>
      URI from project dataset, existing linked data authority, or minted by
      LINCS
    </td>
  </tr>
  <tr>
    <td>
      <strong>Format/Requirements for the Value</strong>
    </td>
    <td>URI (preferably dereferenceable)</td>
  </tr>
  <tr>
    <td rowspan="2">
      <strong>Case Examples</strong>
    </td>
    <td>
      <strong>Typical Example & Abstraction</strong>
    </td>
    <td>
      The Anthologia graeca dataset declares there is a transcription of a
      passage that is in French.
      <p>
        <code>
          &lt;lincs:text/4392> → crm:P72_has_language →{" "}
          <strong>&lt;lincs:language/fra></strong>
          <br /> → crm:P190_has_symbolic_content → **“En jouant de la trompette, Marcus le fluet émit un tout petit
            souffle et, tout droit, tête première, il s’en alla dans l’Hadès!”**
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td>
      <strong>Edge Case Example & Abstraction</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Resource Links</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Discussion Elements Pertaining to This Pattern</strong>
    </td>
    <td>
      crm:E33_Linguistic_Object is a subclass of crm:E28_Conceptual_Object.
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Projects Following This Pattern</strong>
    </td>
    <td>Anthologia graeca, Orlando, MoEML Gazetteer</td>
  </tr>
</table>

**Pattern in TTL:**

```turtle
<text> a crm:E33_Linguistic_Object ;
    rdfs:label "<text>"
    crm:P72_has_language <language> .

<language> a crm:E56_Language ; 
    rdfs:label "<language>" .
```

### Bibliographic Records (FRBRoo)

#### Publishing

A publishing date adds temporal information to the publishing event. For publishing date, see the [Time-Spans of Activities section of the Basic Patterns Application Profile](/docs/explore-lod/understand-lincs-data/application-profiles-main/basic-patterns#time-spans-of-activities).

A publishing place adds location information to the publishing event. For publishing place, see the [Location of Activities section of the Basic Patterns Application Profile](/docs/explore-lod/understand-lincs-data/application-profiles-main/basic-patterns#locations-of-activities).

For information about how to say that an actor was involved in or carried out an activity, see the [Roles section of the Basic Patterns Application Profile](/docs/explore-lod/understand-lincs-data/application-profiles-main/basic-patterns#roles).

Publication is a specific type of Creation event identified through the use of the `E55_Type &lt;event:PublishingEvent> .`

![Application profile publishing](/img/documentation/application-profile-conceptual-publishing-(c-LINCS).png)

<table>
  <tr>
    <td rowspan="2">
      <strong>Pattern/Structure Values</strong>
    </td>
    <td>
      <strong>Definition</strong>
    </td>
    <td>This pattern declares that a publication was published.</td>
  </tr>
  <tr>
    <td>
      <strong>Abstraction</strong>
    </td>
    <td>
      <code>
        frbroo:F2_Expression → crm:P94i_was_created_by →{" "}
        <strong>crm:E65_Creation</strong>
        <br />→ P2_has_type → E55_Type[event:PublishingEvent]
      </code>
    </td>
  </tr>
  <tr>
    <td rowspan="3">
      <strong>Content Values</strong>
    </td>
    <td>
      <strong>Type of Value</strong>
    </td>
    <td>Uniform Resource Identifier (URI)</td>
  </tr>
  <tr>
    <td>
      <strong>Expected Value</strong>
    </td>
    <td>
      URI from project dataset, existing linked data authority, or minted by
      LINCS
    </td>
  </tr>
  <tr>
    <td>
      <strong>Format/Requirements for the Value</strong>
    </td>
    <td>URI (preferably dereferenceable)</td>
  </tr>
  <tr>
    <td rowspan="2">
      <strong>Case Examples</strong>
    </td>
    <td>
      <strong>Typical Example & Abstraction</strong>
    </td>
    <td>
      The HistSex dataset states that there is a publication that was created in
      a creation/publishing event.
      <p>
        <code>
          &lt;histsex:The_Lives_of_Transgender_People> → crm:P94i_was_created_by
          →{" "}
          <strong>&lt;histsex:creation/The_Lives_of_Transgender_People></strong>
          <br />→ P2_has_type → &lt;event:PublishingEvent>
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td>
      <strong>Edge Case Example & Abstraction</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Resource Links</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Discussion Elements Pertaining to This Pattern</strong>
    </td>
    <td>
      When using E65_Creation, use at least one (1) E55_Type on each instance
      specifying what kind of creation activity it is. Publication is a specific
      type of Creation event identified through the use of the E55_Type
      &lt;event:PublishingEvent> .
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Projects Following This Pattern</strong>
    </td>
    <td>AdArchive, Canadian Centre for Ethnomusicology, HistSex, Orlando</td>
  </tr>
</table>

**Pattern in TTL:**

```turtle
<publication> a frbroo:F2_Expression ;
    rdfs:label "<publication>" ;
    crm:P94i_was_created_by <publishing_event> .

<publishing_event> a crm:E65_Creation ; 
    rdfs:label "Creation event of <object>" ; 
    crm:P2_has_type event:PublishingEvent . 

event:PublishingEvent a crm:E55_Type ; 
    rdfs:label "publishing event" .
```

##### Publishing Frequency

![Application profile publishing frequency](/img/documentation/application-profile-conceptual-publishingfrequency-(c-LINCS).png)

<table>
  <tr>
    <td rowspan="2">
      <strong>Pattern/Structure Values</strong>
    </td>
    <td>
      <strong>Definition</strong>
    </td>
    <td>This pattern declares that a publication was published.</td>
  </tr>
  <tr>
    <td>
      <strong>Abstraction</strong>
    </td>
    <td>
      <code>
        frbroo:F2_Expression → crm:P94i_was_created_by →{" "}
        <strong>crm:E65_Creation</strong>
        <br />→ P2_has_type → E55_Type[event:PublishingEvent]
      </code>
    </td>
  </tr>
  <tr>
    <td rowspan="3">
      <strong>Content Values</strong>
    </td>
    <td>
      <strong>Type of Value</strong>
    </td>
    <td>Uniform Resource Identifier (URI)</td>
  </tr>
  <tr>
    <td>
      <strong>Expected Value</strong>
    </td>
    <td>
      URI from project dataset, existing linked data authority, or minted by
      LINCS
    </td>
  </tr>
  <tr>
    <td>
      <strong>Format/Requirements for the Value</strong>
    </td>
    <td>URI (preferably dereferenceable)</td>
  </tr>
  <tr>
    <td rowspan="2">
      <strong>Case Examples</strong>
    </td>
    <td>
      <strong>Typical Example & Abstraction</strong>
    </td>
    <td>
      The HistSex dataset states that there is a publication that was created in
      a creation/publishing event.
      <p>
        <code>
          &lt;histsex:The_Lives_of_Transgender_People> → crm:P94i_was_created_by
          →{" "}
          <strong>&lt;histsex:creation/The_Lives_of_Transgender_People></strong>
          <br />→ P2_has_type → &lt;event:PublishingEvent
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td>
      <strong>Edge Case Example & Abstraction</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Resource Links</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Discussion Elements Pertaining to This Pattern</strong>
    </td>
    <td>
      When using E65_Creation, use at least one (1) E55_Type on each instance
      specifying what kind of creation activity it is. Publication is a specific
      type of Creation event identified through the use of the E55_Type
      &lt;event:PublishingEvent> .
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Projects Following This Pattern</strong>
    </td>
    <td>AdArchive, Canadian Centre for Ethnomusicology, HistSex, Orlando</td>
  </tr>
</table>

**Pattern in TTL:**

```turtle
<publication> a frbroo:F2_Expression ;
    rdfs:label "<publication>" ;
    crm:P94i_was_created_by <publishing_event> .

<publishing_event> a crm:E65_Creation ; 
    rdfs:label "Creation event of <object>" ; 
    crm:P2_has_type event:PublishingEvent ;
    crm:P33_used_specific_technique <issuing_rule> . 

event:PublishingEvent a crm:E55_Type ; 
    rdfs:label "publishing event" .

<issuing_rule> a crm:E29_Design_or_Procedure ; 
    rdfs:label "Publication frequency of <publication>" ; 
    crm:P2_has_type wikidata:Q104480093 ;
    crm:P43_has_dimension <issuing_rule_dimension> .

<issuing_rule_dimension> a crm:E54_Dimension ; 
    rdfs:label "<publication_frequency>" ; 
    crm:P90_has_value "#" ;
    crm:P91_has_unit <unit> . 

<unit> a crm:E58_Measurement_Unit ; 
    rdfs:label "<unit>" .

wikidata:Q104480093 a crm:E55_Type ;
    rdfs:label "frequency" .
```

#### Making a Recording

A recording date adds temporal information to the recording event. For recording date, see the [Time-Spans of Activities section of the Basic Patterns Application Profile](/docs/explore-lod/understand-lincs-data/application-profiles-main/basic-patterns#time-spans-of-activities).

A recording place adds location information to the recording event. For recording place, see the [Location of Activities section of the Basic Patterns Application Profile](/docs/explore-lod/understand-lincs-data/application-profiles-main/basic-patterns#locations-of-activities).

For information about how to say that an actor was involved in or carried out an activity, see the [Roles section of the Basic Patterns Application Profile](/docs/explore-lod/understand-lincs-data/application-profiles-main/basic-patterns#roles).

![Application profile making a recording](/img/documentation/application-profile-conceptual-makingrecording-(c-LINCS).png)

<table>
  <tr>
    <td rowspan="2">
      <strong>Pattern/Structure Values</strong>
    </td>
    <td>
      <strong>Definition</strong>
    </td>
    <td>This pattern declares that a recording was created (recorded).</td>
  </tr>
  <tr>
    <td>
      <strong>Abstraction</strong>
    </td>
    <td>
      <code>
        frbroo:F2_Expression → crm:P94i_was_created_by →{" "}
        <strong>frbroo:F29_Recording_Event></strong>
        <br />→ P2_has_type → E55_Type[aat:300077610]
      </code>
    </td>
  </tr>
  <tr>
    <td rowspan="3">
      <strong>Content Values</strong>
    </td>
    <td>
      <strong>Type of Value</strong>
    </td>
    <td>Uniform Resource Identifier (URI)</td>
  </tr>
  <tr>
    <td>
      <strong>Expected Value</strong>
    </td>
    <td>
      URI from project dataset, existing linked data authority, or minted by
      LINCS
    </td>
  </tr>
  <tr>
    <td>
      <strong>Format/Requirements for the Value</strong>
    </td>
    <td>URI (preferably dereferenceable)</td>
  </tr>
  <tr>
    <td rowspan="2">
      <strong>Case Examples</strong>
    </td>
    <td>
      <strong>Typical Example & Abstraction</strong>
    </td>
    <td>
      The Canadian Centre for Ethnomusicology dataset states that a recording
      was created in a creation/recording event.
      <p>
        <code>
          &lt;cce:31-70160> → crm:P94i_was_created_by →{" "}
          <strong>&lt;cce:production_uri/CCE1995.1.24D></strong>
          <br />→ P2_has_type → &lt;aat:300077610>
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td>
      <strong>Edge Case Example & Abstraction</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Resource Links</strong>
    </td>
    <td>
      The Getty Research Institute. (2017, March 7).{" "}
      <em>Art & Architecture Thesaurus Online</em>.{" "}
      <a href="https://www.getty.edu/research/tools/vocabularies/aat/">
        https://www.getty.edu/research/tools/vocabularies/aat/
      </a>
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Discussion Elements Pertaining to This Pattern</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Projects Following This Pattern</strong>
    </td>
    <td>Canadian Centre for Ethnomusicology</td>
  </tr>
</table>

**Pattern in TTL:**

```turtle
<recording> a frbroo:F2_Expression ;
    rdfs:label "<recording>" ;
    crm:P94i_was_created_by <recording_event> .

<recording_event> a frbroo:F29_Recording_Event ; 
    rdfs:label "Recording event of <recording>" ; 
    crm:P2_has_type aat:300077610 . 

aat:300077610 a crm:E55_Type ; 
    rdfs:label "publishing event" .
```

##### Recording a Performance

A performance date adds temporal information to the performance event. For performance date, see the [Time-Spans of Activities section of the Basic Patterns Application Profile](/docs/explore-lod/understand-lincs-data/application-profiles-main/basic-patterns#time-spans-of-activities).

A performance place adds location information to the performance event. For performance place, see the [Location of Activities section of the Basic Patterns Application Profile](/docs/explore-lod/understand-lincs-data/application-profiles-main/basic-patterns#locations-of-activities).

For information about how to say that an actor was involved in or carunderstand-lincs-dataapplication-profiles/application-profiles-main/basic-patterns#roles).

![Application profile recording a performance](/img/documentation/application-profile-conceptual-recordingperformance-(c-LINCS).png)

<table>
  <tr>
    <td rowspan="2">
      <strong>Pattern/Structure Values</strong>
    </td>
    <td>
      <strong>Definition</strong>
    </td>
    <td>This pattern declares that a recording recorded a performance.</td>
  </tr>
  <tr>
    <td>
      <strong>Abstraction</strong>
    </td>
    <td>
      <code>
        frbroo:F29_Recording_Event → frbroo:R20_recorded →{" "}
        <strong>F31_Performance</strong>
      </code>
    </td>
  </tr>
  <tr>
    <td rowspan="3">
      <strong>Content Values</strong>
    </td>
    <td>
      <strong>Type of Value</strong>
    </td>
    <td>Uniform Resource Identifier (URI)</td>
  </tr>
  <tr>
    <td>
      <strong>Expected Value</strong>
    </td>
    <td>
      URI from project dataset, existing linked data authority, or minted by
      LINCS
    </td>
  </tr>
  <tr>
    <td>
      <strong>Format/Requirements for the Value</strong>
    </td>
    <td>URI (preferably dereferenceable)</td>
  </tr>
  <tr>
    <td rowspan="2">
      <strong>Case Examples</strong>
    </td>
    <td>
      <strong>Typical Example & Abstraction</strong>
    </td>
    <td>
      The Canadian Centre for Ethnomusicology dataset states that a recording
      recorded a performance.
      <p>
        <code>
          &lt;cce:31-70160> → crm:P94i_was_created_by →
          &lt;cce:production_uri/CCE1995.1.24D>
          <br />→ frbroo:R20_recorded → **&lt;cce:performance/CCE1995.1.24D-10>**
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td>
      <strong>Edge Case Example & Abstraction</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Resource Links</strong>
    </td>
    <td>
      The Getty Research Institute. (2017, March 7).{" "}
      <em>Art & Architecture Thesaurus Online</em>.{" "}
      <a href="https://www.getty.edu/research/tools/vocabularies/aat/">
        https://www.getty.edu/research/tools/vocabularies/aat/
      </a>
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Discussion Elements Pertaining to This Pattern</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Projects Following This Pattern</strong>
    </td>
    <td>Canadian Centre for Ethnomusicology</td>
  </tr>
</table>

**Pattern in TTL:**

```turtle
<recording> a frbroo:F2_Expression ;
    rdfs:label "<recording>" ;
    crm:P94i_was_created_by <recording_event> .

<recording_event> a frbroo:F29_Recording_Event ; 
    rdfs:label "Recording event of <recording>" ; 
    frbroo:R20_recorded <performance> .

<performance> a frbroo:F31_Performance ; 
    rdfs:label "<performance>" .
```

### Digital Records

Different types of information objects, such as those used for digital records, are differentiated by the P2_has_type → E55_Type pattern associated with them. For more on this, see the [Types section of the Basic Patterns Application Profile)](/docs/explore-lod/understand-lincs-data/application-profiles-main/basic-patterns#types). For example:

* Digital record: `https://vocab.getty.edu/aat/300379790`
* Website: `https://www.wikidata.org/wiki/Q35127`
* Webpage: `https://www.wikidata.org/wiki/Q36774`
