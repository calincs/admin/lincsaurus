---
slug: icecream-binaries
title: Glaces, binaires et hypothèses
authors: Jingyi Long
tags: [CWRC, metadata, ontologies]
hide_table_of_contents: true
image: ./icecream-binaries-unsplash-(cc0).jpg
Machine Translation: True
Translation Tool: DeepL
Translated Added By: Humna Chaudhry(@humnq)
Last Translated: 2024-06-15
---

![](<./icecream-binaries-unsplash-(cc0).jpg>)

:::info

Cette page web a été traduite automatiquement par DeepL. Bien que nous nous efforcions d’être précis, nous vous informons que les traductions peuvent contenir des erreurs ou des inexactitudes. Pour obtenir les informations les plus précises, veuillez vous référer à la version originale.

:::

Lors de ma première réunion cet été en tant qu'assistant de recherche en science des données, nous avons tous suivi nos présentations personnelles en déclarant nos parfums de crème glacée préférés. Le mien était et est toujours [la glace Häagen Dazs' Strawberry Cheesecake](https://www.haagen-dazs.ca/en/h%C3%A4agen-dazs/extr%C3%A4az-strawberry-cheesecake-ice-cream), et j'ai été agréablement surprise d'apprendre que quelqu'un d'autre dans l'équipe était du même avis. Cependant, la plus grande surprise a été d'apprendre que quelqu'un aimait passer sa glace au micro-ondes pour en modifier la texture. À ce moment-là, est-ce encore de la glace ? Est-ce une soupe ? Ou un milkshake ? Qu'est-ce qui est considéré comme de la crème glacée ? ...{/* truncate */}

Mettant de côté ces questions, j'ai plongé tête la première dans un monde différent, non laitier, celui des systèmes d'organisation des connaissances. Je nageais dans une mer d'acronymes (:Term[RDF]{#resource-description-framework}, :Term[SKOS]{#simple-knowledge-organization-system}, :Term[OWL]{#web-ontology-language}, [CWRC](https://cwrc.ca/), et :Term[TTL]{#turtle} pour n'en citer que quelques-uns) et de structures organisationnelles. J'étais occupé à comprendre comment les humains pouvaient utiliser des systèmes d'organisation et des langages développés dans le seul but de représenter des connaissances pour amener les ordinateurs à voir le monde dans des catégories créées par l'homme.

Alerte au spoiler—c'est compliqué. Mais d'abord, une mini leçon d'étymologie.

Le terme :Term[Ontologie]{#ontology} vient des mots grecs _onto_ (être) et _logia_ (étude), ce qui signifie en fait l'étude de l'être. Les ontologues s'interrogent sur la manière dont les choses sont regroupées en catégories, sur les catégories qui se situent au niveau le plus élevé et sur la manière dont ces catégories informent la classification de _tout_. En sciences de l'information et :Term[Digital Humanities (DH)]{#digital-humanities}, une ontologie fait référence à un modèle des catégories les plus larges d'un ensemble de connaissances spécifique. Si les ontologies comportent des concepts génériques, elles n'incluent pas de spécificités - celles-ci sont laissées au  :Term[vocabulaires]{#vocabulary}. Les vocabulaires développent les concepts génériques des ontologies en leur donnant des contextes et des significations.

Par exemple, une ontologie de desserts peut inclure une classe de desserts laitiers avec une sous-classe pour les crèmes glacées. Cependant, les arômes de crème glacée peuvent être omis de l'ontologie en raison de leur spécificité ; au lieu de cela, ils peuvent apparaître dans un vocabulaire sur les desserts. Le mot clé de cet exemple est "peut". Le créateur de l'ontologie _peut_ décider que la glace au cheesecake et à la fraise est suffisamment importante pour être ajoutée à l'ontologie et que d'autres parfums devraient donc y figurer également.

Le mot _peut_ n'a cessé d'apparaître, non seulement lorsque je pensais à la crème glacée, mais aussi lorsque je naviguais dans les ontologies et les vocabulaires du LINCS. L'un de mes principaux rôles cet été a été de mettre en œuvre les changements apportés aux ontologies dans les vocabulaires également. J'ai notamment corrigé les relations redondantes dans le [Vocabulaire du genre](https://vocab.lincsproject.ca/Skosmos/genre/en/), comme la théorie féministe déclarée comme faisant partie du genre philosophique _et_ savant, alors que le genre philosophique est en fait un sous-ensemble du genre savant. En conséquence, j'ai acquis une bonne connaissance des tenants et aboutissants des vocabulaires [CWRC](https://vocab.lincsproject.ca/Skosmos/cwrc/en/), [Genre](https://vocab.lincsproject.ca/Skosmos/genre/en/) et [Illness & Injuries](https://vocab.lincsproject.ca/Skosmos/ii/en/).

Un petit aparté avant de parler du très redouté _peut_. Les ontologies et les vocabulaires avec lesquels j'ai travaillé n'ont pas été dessinés sur un tableau blanc où je pouvais effacer des concepts et ajouter des connexions entre les classes à ma guise. Au lieu de cela, ils vivaient sur le web où ils étaient codés à l'aide de langages et de structures sémantiques tels que OWL, RDFS et SKOS. Les ordinateurs se nourrissent de binaires où une décision active ou désactive un interrupteur ; l'ambiguïté bloque le système car les machines ne peuvent pas déduire des significations implicites comme le ferait un cerveau humain. Les modèles sémantiques tels que les ontologies et les vocabulaires tentent donc de reproduire les méthodes de saisie et de catégorisation qui semblent être une seconde nature pour les ordinateurs.

Pour résumer, les ordinateurs fonctionnent de manière binaire, ce qui n'est pas le cas pour nous. L'intégralité des données du web est stockée sous forme de uns et de zéros, mais notre cerveau perçoit le monde de manière beaucoup plus complexe et nuancée.

Si les ontologies et les vocabulaires peuvent sembler similaires (ils aident tous deux à catégoriser les choses), les ontologies sont strictes dans la manière dont les concepts peuvent être regroupés puisqu'elles utilisent la logique formelle. En comparaison, un vocabulaire ne prétend pas que la crème glacée est un dessert ; il aide seulement à décrire des concepts, tels que les nombreux parfums de crème glacée décadente. En l'écrivant de cette manière, il semble intuitif que la logique ne s'applique pas aux vocabulaires, ou comme l'écrit [W3.org](https://www.w3.org/TR/skos-reference/):

_[Certains systèmes d'organisation des connaissances] ne sont pas, de par leur conception, destinés à représenter une vue logique de leur domaine. La conversion de ces systèmes d'organisation des connaissances en une représentation formelle basée sur la logique peut, dans la pratique, impliquer des changements qui aboutissent à une représentation qui ne répond plus à l'objectif initialement prévu._

Il est vrai que le recul est de 20/20, car même si cela semble évident aujourd'hui, j'ai passé d'innombrables heures à m'agiter sur des hiérarchies conceptuelles dans les vocabulaires qui n'avaient tout simplement pas de sens logique. Le genre fictionnel _peut_ relever du genre littéraire, mais il _peut_ aussi relever du genre narratif. Si l'on me posait cette question à l'improviste, je demanderais pourquoi pas les deux, mais j'étais tellement habitué à la pensée binaire des ordinateurs que j'ai été pris au dépourvu lorsque j'ai vu que le genre fictionnel était une ramification du genre littéraire _et_ du genre narratif.

![Carte conceptuelle avec des cercles contenant un type de genre et des lignes reliant les différents genres.](<./icecream-binaries-graph-(c-LINCS).png>)

_Visualisation du vocabulaire du genre basée sur l'ontologie du genre produite à l'aide de [WebVOWL](http://vowl.visualdataweb.org/webvowl.html)_.

Il était facile de tomber dans le schéma de pensée selon lequel un concept était soit A, soit B, en particulier lorsque les catégories étaient déjà devant moi. Il était plus difficile de reconnaître que les choses dans la vie réelle sont beaucoup plus compliquées et nuancées qu'un système codé ne peut catégoriser proprement.

J'ai passé une si grande partie de l'été à essayer de comprendre comment le monde pouvait entrer dans des petites boîtes bien rangées que j'ai oublié l'aspect humain de la catégorisation - le désordre et les _peut-être_ qui accompagnent souvent la prise de décision. Alors que le monde raffiné des ontologies et des vocabulaires peut sembler très éloigné de la vie quotidienne, il m'a fallu une bonne partie de l'été pour me rappeler qu'ils sont basés sur des catégories créées par l'homme - des catégories dont les limites peuvent changer au fil du temps et varier selon l'utilisateur.

Tout cela pour dire qu'à la fin de l'été, j'avais une réponse aux questions brûlantes que je me posais depuis le début du mois de mai : tout est subjectif ! Il s'agit peut-être d'une non-réponse dans le sens binaire du oui ou du non, mais tant qu'il existe une sorte de consensus (que ce soit entre deux personnes ou des millions), le destin de la crème glacée fondue, comme les vocabulaires, peut exister dans l'écart entre les absolus.
