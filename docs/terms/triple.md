---
id: triple
title: Triple
definition: A statement in the form of subject-predicate-object that follows the Resource Description Framework (RDF).
---

A triple is a statement in the form of subject-predicate-object. These assertions are made using the :Term[Resource Description Framework (RDF)]{#resource-description-framework} and are what make up the :Term[Semantic Web]{#semantic-web}.
