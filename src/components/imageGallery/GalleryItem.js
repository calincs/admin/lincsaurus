/** @format */

import React from "react";
import Link from '@docusaurus/Link';


function GalleryItem({ path, altlabel, title, caption }) {
  // Potentially could include copyright?

  const fullCaption = `<h4>${title}</h4><em>${caption}</em>`;

  return (
        <Link to={path} data-sub-html={fullCaption} data-src={path}>
          <img alt={altlabel} title={title} src={path} />
        </Link>
  );
}

export default GalleryItem;
