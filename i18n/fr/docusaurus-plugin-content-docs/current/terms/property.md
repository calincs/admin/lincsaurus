---
id: property
title: Propriété
definition: Une relation spécifiée entre deux classes ou entités, comme le prédicat dans un triplet (sujet-prédicat-objet).
---

Une propriété définit une relation spécifiée entre deux classes ou :Term[entités]{#entity}. Dans les déclarations `<subject><predicate><object>` (:Term[triples]{#triple}) qui comprennent le :Term[Web sémantique]{#semantic-web}, la propriété est le prédicat ou “verbe” dans la phrase et est définie en référence à la fois au sujet (:Term[domaine]{#domain}) et objet (:Term[range]{#range}) du :Term[triple]{#triple}.

## Exemples

- L’exemple suivant montre la propriété, ou le prédicat, reliant deux entités ou classes dans un triple énoncé.

![alt=""](</img/documentation/glossary-property-example-(c-LINCS).jpg>)
