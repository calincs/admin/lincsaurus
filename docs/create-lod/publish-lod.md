---
sidebar_position: 9
title: "Publish LOD"
description: "LINCS Transformation Workflows — Setup your Data in ResearchSpace"
---

## Introduction

[ResearchSpace](/docs/tools/researchspace/) is the web platform that LINCS uses to publish :Term[linked open data (LOD)]{#linked-open-data}. Once you have finished transforming your data, ResearchSpace will allow you to search, browse, visualize, and edit your new linked data which will be stored in the LINCS :Term[triplestore]{#triplestore}. See our [ResearchSpace documentation](/docs/tools/researchspace/researchspace-documentation) for more details.

We recommend that your team browse existing projects in ResearchSpace to understand the possibilities for how you can present your transformed data.

The information on this page applies to data that came out of any of the four transformation workflows because the input for ResearchSpace is LINCS RDF, which each workflow outputs.

## Resources Needed

We use the first ingestion into ResearchSpace as a chance to verify that the transformation workflow was successful. Sometimes we go back and iterate until the data is ready for a final ingestion and publication through ResearchSpace.

This step typically takes 1-4 weeks, cycling through data review and edits with communications between the LINCS teams and the research team.

|  | Research Team | Ontology Team | Transformation Team | Storage Team |
| --- | --- | --- | --- | --- |
| [Ingest Trial Data](#ingest-trial-data) |  |  |  | ✓ |
| [Customize View of Trial Data](#customize-view-of-trial-data) | ✓ |  |  | ✓ |
| [Explore Trial Data and Fix Errors](#explore-trial-data-and-fix-errors) | ✓ | ✓ | ✓ | ✓ |
| [Finish Data Cleaning and Entity Matching](#finish-data-cleaning-and-entity-matching) | ✓ |  |  |  |
| [Ingest Official Data](#ingest-official-data) |  |  |  | ✓ |
| [Explore Official Data and Find Errors](#explore-official-data-and-fix-errors) | ✓ |  |  |  |

## Ingest Trial Data

After the [Validate and Enhance](/docs/create-lod/validate-and-enhance) step of any transformation workflow, the transformed data is handed over to the Storage Team to be uploaded to the LINCS triplestore as a trial.

## Customize View of Trial Data

The ingested trial data is explored by the research team, Ontology Team, Transformation Team, and Storage Team in [ResearchSpace](/docs/tools/researchspace).

Without changing the dataset itself, the Storage Team can manipulate how ResearchSpace displays the data’s :Term[entities]{#entity} and relationships. The Storage Team and research team collaborate to customize the view of the data.

While the draft dataset is public in ResearchSpace, it is not yet published LOD at this stage, and cannot yet be used in publications.

## Explore Trial Data and Fix Errors

When someone from the research team, Ontology Team, Transformation Team, or Storage Team spots an error in the dataset, they can report the error via an issue in the LINCS GitLab.

At this stage, the focus is on errors that were introduced through the transformation process, not errors in the data itself.

For :Term[structured data]{#structured-data}, the easiest way to fix these errors is by going back to the relevant transformation step, changing that step to prevent the error, and rerunning the transformation workflow. The most common errors are incorrect data in the source data file or an incorrectly implemented relationship in the 3M configuration files.

For the :Term[TEI]{#tei-data} and :Term[natural language]{#natural-language-data} workflows, we would not change the source data or rerun transformation steps because there is typically manual work completed after those steps. We would instead change the final RDF file.

After rerunning the updated workflow, the Storage Team replaces the dataset in the LINCS triplestore. We continue to repeat these review and edit steps until the research team approves their data for publication.

## Finish Data Cleaning and Entity Matching

If data cleaning or entity matching steps were not completed previously, the research team can continue to do that at this stage before we publish the transformed data. LINCS will advise you on if this should be done in GitLab or through ResearchSpace.

Of course, you can continue editing and enhancing your data once it has been officially published.

## Ingest Official Data

Once all errors introduced during transformation have been removed, the research team decides if they are happy with the current version of the data. If so, the data can be officially ingested and published.

The final version of the dataset is uploaded to the LINCS triplestore by the Storage Team. At this stage, the dataset is accessible via the official version of ResearchSpace and is a part of the public LINCS triplestore.

The research team can approve data for publication by completing the [release approval form](https://uoguelph.eu.qualtrics.com/jfe/form/SV_afS94Jpi5HRCCUK).

## Explore Official Data and Fix Errors

You can now start exploring and editing your fully transformed data in ResearchSpace and connect your data to visualization tools if desired. See our [ResearchSpace documentation](/docs/tools/researchspace/researchspace-documentation) and [Visualization tools documentation](/docs/explore-lod/#visualize) for details.

Once the dataset is published to ResearchSpace, it is the responsibility of the research team to make changes to the data directly in ResearchSpace.

Changes made by the research team affect the version of the dataset that is in the LINCS triplestore, which means that the transformation workflow does not need to be repeated. The tools to fix errors are accessible through ResearchSpace so GitLab is no longer used to edit this data.

:::warning

Once the data is published in ResearchSpace, LINCS has committed to keeping the data in Research Space and maintaining the :Term[Uniform Resource Identifiers (URIs)]{#uniform-resource-identifier}. LINCS is not committed to keeping the source data or to feed back any updates that are made in ResearchSpace to previous versions of the data.

:::

## Archive Data

After the dataset is published to ResearchSpace, a copy of the dataset is also archived in LINCS's space on [Borealis: The Canadian Dataverse Repository](https://borealisdata.ca/dataverse/LINCS). 

If substantial changes are made to the data, a new version of the dataset can be contributed to Borealis.
