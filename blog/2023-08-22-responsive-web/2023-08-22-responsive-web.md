---
slug: responsive-web
title: "Designing and Building Responsive Web Applications"
authors: Marco Lian Bantolino
tags: [UX]
hide_table_of_contents: true
Last Translated: 2024-06-15
image: ./responsive-web-unsplash-(cc0).jpg
---

![](<./responsive-web-unsplash-(cc0).jpg>)

When designing and building a web application, ensuring that it is responsive is paramount. A responsive application looks good and functions well on all screen sizes and devices. LINCS applications are being designed to be viewed in a wide range of ways: from tablets to laptops to whiteboard-sized interactive screens. It can be quite hard to achieve this level of responsiveness, but with the help of CSS tools, techniques, and frameworks, the task becomes a lot easier...{/* truncate */}

There are commonly used layout systems built into CSS called Flexbox and Grid. In addition, CSS has a feature called media queries, which allows content to appear differently depending on the size of the viewport. There are also CSS frameworks like Bootstrap CSS that are easy to use and great for responsive design.

If you are using vanilla (standard) CSS and want full control of the design, you can either use CSS Flexbox for a one-dimensional layout or CSS Grid for a two-dimensional one. [Flexbox](https://css-tricks.com/snippets/css/a-guide-to-flexbox/) offers customizable properties that control layout and alignment. Items inside a flexbox container are called flex items. Flex items can be arranged horizontally or vertically depending on the flex-direction property that is set (row, column, row-reverse, etc). Furthermore, flex items can be centered vertically or horizontally and can grow or shrink as their parent container increases or decreases—options that let you make a layout suited to your needs. CSS Grid similarly has properties that can be configured. I often turn to CSS Flexbox when designing for LINCS’s ResearchSpace, as it allows me to horizontally or vertically align the many components we need to display, keeping the page’s elements distinct no matter the screen size.

![Various flexbox arrangements such as stretch or flex-start where the items go down from the top or flex-bottom where they come up from the bottom.](<./responsive-web-flexbox-(c-LINCS).png>)

Making information accessible is one of the key goals of LINCS and ResearchSpace, and ensuring that users can clearly see what information is on their screen is part of this. Media queries are a great CSS technique for this kind of task. Media queries allow you to create different layouts depending on the size of the viewport (the user’s visible area of a web page), which make it quite a powerful tool. For example, you can rearrange items in a container so that they adjust to suit the screen size. If it is large-sized, you can have the items in one row. If it is medium-sized, you can have the items in two rows. Rendering content according to screen size is essential in a responsive web application, as it allows your user to view the right amount of information for their screen.

Lastly, there are CSS frameworks like [Bootstrap CSS](https://getbootstrap.com/docs/3.4/css/). With Bootstrap, you can quickly and easily make responsive layouts that look good on all different screen sizes and devices—including mobile devices. Bootstrap is commonly used to build responsive grid layouts. A bootstrap container is divided into twelve columns, and all you need to do is specify, for example, how many columns an item should take up. The responsive features are already built into the layout, so all the work is done for you when it comes to making the layout responsive. ResearchSpace uses Bootstrap CSS. One instance where its application was necessary arose during the redesign of the entity aggregation page. In this case, I needed to construct a grid layout in which I relied on Bootstrap CSS. The resulting design, as shown from the following screenshots, showcases its responsiveness across different screen sizes, including the iPad Pro (1024px x 1366px) and the standard full HD monitor (1920px x 1280px).

![ResearchSpace on iPad.](<./responsive-web-ipad-(c-LINCS).png>)

_ResearchSpace on an iPad Pro (1024px x 1366px)._

![ResearchSpace on monitor.](<./responsive-web-monitor-(c-LINCS).png>)

_ResearchSpace on a standard full HD monitor (1920px x 1280px)._

These CSS tools and techniques have helped me redesign the appearance of ResearchSpace to make it more responsive. They are quick and easy to use, and they also have very good documentation online, so it is not difficult to find information about how to use them. In turn, I hope that implementing them for ResearchSpace will make it easier for users to find information using LINCS.
