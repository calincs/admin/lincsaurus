/** @format */

import React from "react";
import styles from "./cards.module.css";
import PrimaryButton from "../buttons/PrimaryButton";
import EntityButton from "../buttons/EntityButton";
import Link from "@docusaurus/Link";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { library } from "@fortawesome/fontawesome-svg-core";
import { faKey } from "@fortawesome/free-solid-svg-icons";

library.add(faKey);

// TODO: Add buttons to link the various buttons
function DatasetCard({ dataset }) {
  const { title, shortDescription, keywords, page, images, buttons } = dataset;
  const image = images["logo"] || images["entity"];

  return (
    <div className={styles.datasetCardContainer}>
      <Link to={page} className={styles.DatasetCardLink}>
        <div className={styles.cardTitle}>
          <h2 title={title}>{title}</h2>
        </div>

        <img
          src={image.file}
          alt={image.altText}
          className={styles.cardImage}
        />

        <div className={styles.staticCardDescription}>
          <p>{shortDescription}</p>
        </div>

        <div className={styles.datasetKeywords}>
          {keywords.length > 0 ? (
            <>
              <p>
                #
                {keywords
                  .map((keyword) => keyword.toLowerCase())
                  .sort()
                  .join(", #")}
              </p>
            </>
          ) : null}
        </div>
      </Link>
    </div>
  );
}

export default DatasetCard;
