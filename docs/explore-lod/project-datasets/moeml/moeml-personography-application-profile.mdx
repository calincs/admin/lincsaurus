---
sidebar_position: 3
title: "MoEML Personography Application Profile"
---





## Purpose

To document how _Map of Early Modern London_ personography data is modelled for compatibility with the wider LINCS data model. This document provides patterns used in the mapping of _Map of Early Modern London_ personography data which correspond to the categories outlined in the [LINCS Application Profile](/docs/explore-lod/understand-lincs-data/application-profiles-main). 

This document introduces class and property concepts as used in this specific context (LINCS and the _Map of Early Modern London_ personography dataset), and are not complete definitions. Consult [CIDOC CRM v. 7.1.1 documentation](https://cidoc-crm.org/Version/version-7.1.1) for full class and property descriptions. 

### Acronyms

**Ontology Acronyms:**

* CIDOC-CRM - [CIDOC Conceptual Reference Model](http://cidoc-crm.org/)

**Vocabulary and Authority Acronyms:**

* Biography - [The LINCS Biography Vocabulary](https://vocab.lincsproject.ca/Skosmos/biography/en/)
* LINCS - LINCS minted entities
* Wikidata - [Wikimedia Knowledge Base](http://www.wikidata.org/entity/)

## Main Classes

<table>
  <tr>
    <td>
      <strong>Entity Type</strong>
    </td>
    <td>
      <strong>Class</strong>
    </td>
    <td>
      <strong>Declaration Snippet (TTL)</strong>
    </td>
  </tr>
  <tr>
    <td>Person</td>
    <td>crm:E21_Person</td>
    <td>
      ```turtle
      <person> a crm:E21_Person .
      ```
    </td>
  </tr>
  <tr>
    <td>Names</td>
    <td>crm:E33_E41_Linguistic_Appellation</td>
    <td>
      ```turtle
      <name> a crm:E33_E41_Linguistic_Appellation ;
        crm:P2_has_type aat:300417193 .
      ```
    </td>
  </tr>
  <tr>
    <td>Type</td>
    <td>crm:E55_Type</td>
    <td>
      ```turtle
      <https://mapoflondon.uvic.ca/mdtEncyclopediaPersonographyHistorical>
        a crm:E55_Type .
      ```
    </td>
  </tr>
  <tr>
    <td>Web Page</td>
    <td>crm:E73_Information_Object</td>
    <td>
      ```turtle
      <webpage> a crm:E73_Information_Object ;
        crm:P2_has_type wikidata:Q36774 .
      ```
    </td>
  </tr>
  <tr>
    <td>Web SIte</td>
    <td>crm:E73_Information_Object</td>
    <td>
      ```turtle
      <website> a crm:E73_Information_Object ;
        crm:P2_has_type wikidata:Q35127 .
      ```
    </td>
  </tr>
  <tr>
    <td>Note</td>
    <td>rdfs:literal</td>
    <td>
      ```turtle
      rdfs:literal
      ```
    </td>
  </tr>
  <tr>
    <td>Birth Event</td>
    <td>crm:E67_Birth</td>
    <td>
      ```turtle
      <Birth_Event> a crm:E67_Birth .
      ```
    </td>
  </tr>
  <tr>
    <td>Death Event</td>
    <td>crm:E69_Death</td>
    <td>
      ```turtle
      <Death_Event> a crm:E69_Death .
      ```
    </td>
  </tr>
</table>


## Overview Diagram

Below is an image of the application profile overview diagram. Follow this link for a zoomable, more readable version.{/* ADD LINK */} The segments below align with the document sections.

![Application profile](/img/documentation/application-profile-moeml-personography-overview-(c-LINCS).jpg)

## Nodes

### Basic Patterns

#### Identifiers

##### Names & Titles 

![Application profile](/img/documentation/application-profile-moeml-personography-names-(c-LINCS).jpg)

<table>
  <tr>
    <td rowspan="2">
      <strong>Pattern/Structure Values</strong>
    </td>
    <td>
      <strong>Definition</strong>
    </td>
    <td>This pattern declares that a person is identified by a name.</td>
  </tr>
  <tr>
    <td>
      <strong>Abstraction</strong>
    </td>
    <td>
      <p>
        <code>
          crm:E21_Person → crm:P1_is_identified_by →
          crm:E33_E41_Linguistic_Appellation
          <br />→ crm\:P2_has_type → **crm\:E55_Type[cwrc:&lt;type>]**
          <br />→ crm\:P190_has_symbolic_content → <strong>rdfs:Literal</strong>
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td rowspan="4">
      <strong>Content Values</strong>
    </td>
    <td>
      <strong>Type of Value</strong>
    </td>
    <td>Uniform Resource Identifier (URI); literal value (text)</td>
  </tr>
  <tr>
    <td>
      <strong>Expected Value</strong>
    </td>
    <td>
      URI minted by LINCS; Literal value (text) from the MoEML Personography
      dataset
    </td>
  </tr>
  <tr>
    <td>
      <strong>Format/Requirements for the Value</strong>
    </td>
    <td>URI (preferably dereferenceable); rdfs:literal</td>
  </tr>
  <tr>
    <td>
      <strong>Example Values</strong>
    </td>
    <td>
      “Sir Nicholas Bacon”,
      &lt;http://id.lincsproject.ca/biography#PreferredName>; “Nicholas”,
      &lt;http://id.lincsproject.ca/cwrc#Forename>; “Bacon”,
      &lt;http://id.lincsproject.ca/cwrc#Surname>;
    </td>
  </tr>
  <tr>
    <td rowspan="2">
      <strong>Case Examples</strong>
    </td>
    <td>
      <strong>Typical Example & Abstraction</strong>
    </td>
    <td>
      <p>
        The Map of Early Modern London Personography dataset states that Sir
        Nicholas Bacon has the display name “Sir Nicholas Bacon.” This is a
        preferred name.
      </p>
      <p>
        <code>
          &lt;moeml:BACO1> → crm:P1_is_identified_by →
          crm:E33_E41_Linguistic_Appellation
          <br />→ crm:P2_has_type → **&lt;biography:PreferredName>**
          <br />→ crm:P190_has_symbolic_content →
          <br />
          <strong>“Sir Nicholas Bacon”</strong>
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td>
      <strong>Edge Case Example & Abstraction</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Resource Links</strong>
    </td>
    <td>
      <p>
        MoEML. (2022, May 5). <em>Map of Early Modern London</em>.{" "}
        <a href="https://mapoflondon.uvic.ca/">https://mapoflondon.uvic.ca/</a>
      </p>
      <p>
        Brown, S., Cummings, J., Drudge-Wilson, J., Faulkner, C., Lemak, A.,
        Martin, K., Mo, A., Penancier, J., Simpson, J., Singh, G., Stacey, D., &
        Warren, R. (2020, July 14). <em>The CWRC Ontology Specification</em>{" "}
        0.99.86. The Canadian Writing Research Collaboratory.{" "}
        <a href="https://sparql.cwrc.ca/ontologies/cwrc.html#">
          https://sparql.cwrc.ca/ontologies/cwrc.html#
        </a>
      </p>
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Discussion Elements Pertaining to the Pattern</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>MoEML Elements Following this Pattern</strong>
    </td>
    <td>
      MoEML foaf:name; MoEM foafL:familyName; MoEML foaf:firstName; MoEML
      foaf:title
    </td>
  </tr>
</table>


**Pattern in TTL:**

```turtle
<MoEML:Person> a crm:E21_Person ; 
	rdfs:label "<MoEML:Person>" ; 
	crm:P1_is_identified_by <name>.

<name> a crm:E33_E41_Linguistic_Appellation ; 
	rdfs:label "<name>" ; 
	crm:P2_has_type cwrc:<type> ;
	crm:P190_has_symbolic_content "<name>" .

cwrc:<type> a crm:E55_Type ; 
	rdfs:label "<type>" .
```

<div className="primary-button-row">
  <PrimaryButton
    link="/docs/tools/sparql/yasgui"
    buttonName="SPARQL: Coming Soon"
    ></PrimaryButton>
</div>

##### Name Formation

![Application profile](/img/documentation/application-profile-moeml-personography-nameformation-(c-LINCS).jpg)

<table>
  <tr>
    <td rowspan="2">
      <strong>Pattern/Structure Values</strong>
    </td>
    <td>
      <strong>Definition</strong>
    </td>
    <td>This pattern describes the relationships between names.</td>
  </tr>
  <tr>
    <td>
      <strong>Abstraction</strong>
    </td>
    <td>
      <p>
        <code>
          crm:E21_Person → crm:P1_is_identified_by →{" "}
          <strong>crm:E33_E41_Linguistic_Appellation</strong>
          <br />→ crm:P106_is_composed_of → **crm:E33_E41_Linguistic_Appellation**
          <br />
          crm:E21_Person → crm:P1_is_identified_by → **crm:E33_E41_Linguistic_Appellation**
          <br />→ crm:P106i_forms_part_of →{" "}
          <strong>crm:E33_E41_Linguistic_Appellation</strong>
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td rowspan="4">
      <strong>Content Values</strong>
    </td>
    <td>
      <strong>Type of Value</strong>
    </td>
    <td>Uniform Resource Identifier (URI); literal value (text)</td>
  </tr>
  <tr>
    <td>
      <strong>Expected Value</strong>
    </td>
    <td>
      URI minted by LINCS; Literal value (text) from the MoEML Personography
      dataset
    </td>
  </tr>
  <tr>
    <td>
      <strong>Format/Requirements for the Value</strong>
    </td>
    <td>URI (preferably dereferenceable); rdfs:literal</td>
  </tr>
  <tr>
    <td>
      <strong>Example Values</strong>
    </td>
    <td>“Sir Nicholas Bacon”; “Nicholas”; “Bacon”</td>
  </tr>
  <tr>
    <td rowspan="2">
      <strong>Case Examples</strong>
    </td>
    <td>
      <strong>Typical Example & Abstraction</strong>
    </td>
    <td>
      <p>
        The Map of Early Modern London Personography dataset states that Sir
        Nicholas Bacon has the display name of “Sir Nicholas Bacon”, which is
        itself made up of “Nicholas” and “Bacon.”
      </p>
      <p>
        <code>
          &lt;moeml:BACO1> → crm:P1_is_identified_by →{" "}
          <strong>&lt;lincs:foaf_name/Sir_Nicholas_Bacon></strong>
          <br />→ crm:P106_is_composed_of → **&lt;lincs:foaf_firstName/Nicholas>**
          <br />
          &lt;moeml:BACO1> → crm:P1_is_identified_by → **&lt;lincs:foaf_firstName/Nicholas>**
          <br />→ crm:P106i_forms_part_of →{" "}
          <strong>&lt;lincs:foaf_name/Sir_Nicholas_Bacon></strong>
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td>
      <strong>Edge Case Example & Abstraction</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Resource Links</strong>
    </td>
    <td>
      <p>
        MoEML. (2022, May 5). <em>Map of Early Modern London</em>.{" "}
        <a href="https://mapoflondon.uvic.ca/">https://mapoflondon.uvic.ca/</a>
      </p>
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Discussion Elements Pertaining to the Pattern</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>MoEML Elements Following this Pattern</strong>
    </td>
    <td>MoEML foaf:name; MoEML foaf:familyName; MoEML foaf:firstName</td>
  </tr>
</table>


**Pattern in TTL:**

```turtle
<MoEML:Person> a crm:E21_Person ; 
	rdfs:label "<MoEML:Person>" ; 
	crm:P1_is_identified_by <name> , <firstName> , <familyName>.

<name> a crm:E33_E41_Linguistic_Appellation ; 
	crm:P106_is_composed_of <firstName> , <familyName> .

<firstName> a crm:E33_E41_Linguistic_Appellation ; 
	crm:P106i_forms_part_of <name> .

<familyName> a crm:E33_E41_Linguistic_Appellation ; 
	crm:P106i_forms_part_of <name> .
```

<div className="primary-button-row">
  <PrimaryButton
    link="/docs/tools/sparql/yasgui"
    buttonName="SPARQL: Coming Soon"
    ></PrimaryButton>
</div>

#### Types

![Application profile](/img/documentation/application-profile-moeml-personography-types-(c-LINCS).jpg)

<table>
  <tr>
    <td rowspan="2">
      <strong>Pattern/Structure Values</strong>
    </td>
    <td>
      <strong>Definition</strong>
    </td>
    <td>This pattern declares that a person is a MoEML Historical Person.</td>
  </tr>
  <tr>
    <td>
      <strong>Abstraction</strong>
    </td>
    <td>
      <strong>crm:E21_Person</strong> → crm:P2_has_type → crm:E55_Type
    </td>
  </tr>
  <tr>
    <td rowspan="4">
      <strong>Content Values</strong>
    </td>
    <td>
      <strong>Type of Value</strong>
    </td>
    <td>Uniform Resource Identifier (URI); literal value (text)</td>
  </tr>
  <tr>
    <td>
      <strong>Expected Value</strong>
    </td>
    <td>
      URI from the MoEML Personography dataset; Literal value (text) from the
      MoEML Personography dataset
    </td>
  </tr>
  <tr>
    <td>
      <strong>Format/Requirements for the Value</strong>
    </td>
    <td>URI (preferably dereferenceable); rdfs:literal</td>
  </tr>
  <tr>
    <td>
      <strong>Example Values</strong>
    </td>
    <td>
      &lt;https://mapoflondon.uvic.ca/mdtEncyclopediaPersonographyHistorical>
    </td>
  </tr>
  <tr>
    <td rowspan="2">
      <strong>Case Examples</strong>
    </td>
    <td>
      <strong>Typical Example & Abstraction</strong>
    </td>
    <td>
      <p>
        The Map of Early Modern London Personography dataset declares that Sir
        Nicholas Bacon is a historical person.
      </p>
      <p>
        <code>
          &lt;moeml:BACO1> → crm:P2_has_type →{" "}
          <strong>&lt;moeml:mdtEncyclopediaPersonographyHistorical></strong>
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td>
      <strong>Edge Case Example & Abstraction</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Resource Links</strong>
    </td>
    <td>
      <p>
        MoEML. (2022, May 5). <em>Map of Early Modern London</em>.{" "}
        <a href="https://mapoflondon.uvic.ca/">https://mapoflondon.uvic.ca/</a>
      </p>
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Discussion Elements Pertaining to the Pattern</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>MoEML Elements Following this Pattern</strong>
    </td>
    <td>MoEML rdf:type</td>
  </tr>
</table>


**Pattern in TTL:**

```turtle
<MoEML:Person> a crm:E21_Person ; 
	rdfs:label "<name>" ; 
    crm:P2_has_type moeml:mdtEncyclopediaPersonographyHistorical .

moeml:mdtEncyclopediaPersonographyHistorical a crm:E55_Type ;
	rdfs:label "MoEML Project Historical Person" .
```

<div className="primary-button-row">
  <PrimaryButton
    link="/docs/tools/sparql/yasgui"
    buttonName="SPARQL: Coming Soon"
    ></PrimaryButton>
</div>

#### Describing or Referencing a Person

##### Subject of a Web Page

![Application profile](/img/documentation/application-profile-moeml-personography-subjectwebpage-(c-LINCS).jpg)

<table>
  <tr>
    <td rowspan="2">
      <strong>Pattern/Structure Values</strong>
    </td>
    <td>
      <strong>Definition</strong>
    </td>
    <td>
      This pattern declares that a person is the main subject of a web page.
    </td>
  </tr>
  <tr>
    <td>
      <strong>Abstraction</strong>
    </td>
    <td>
      <p>
        <code>
          crm:E21_Person → crm:P129i_is_subject_of →{" "}
          <strong>crm:E73_Information_Object</strong>
          <br />→ crm:P2_has_type → crm:E55_Type[wikidata:Q36774]
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td rowspan="4">
      <strong>Content Values</strong>
    </td>
    <td>
      <strong>Type of Value</strong>
    </td>
    <td>Uniform Resource Identifier (URI)</td>
  </tr>
  <tr>
    <td>
      <strong>Expected Value</strong>
    </td>
    <td>URI from the MoEML Personography dataset</td>
  </tr>
  <tr>
    <td>
      <strong>Format/Requirements for the Value</strong>
    </td>
    <td>URI (preferably dereferenceable)</td>
  </tr>
  <tr>
    <td>
      <strong>Example Values</strong>
    </td>
    <td>
      &lt;https://en.wikipedia.org/wiki/Nicholas_Bacon_(Lord_Keeper)>;
      &lt;https://en.wikipedia.org/wiki/William_Fleetwood_(judge)>;
      &lt;https://en.wikipedia.org/wiki/Sir_Henry_Hobart,_1st_Baronet>
    </td>
  </tr>
  <tr>
    <td rowspan="2">
      <strong>Case Examples</strong>
    </td>
    <td>
      <strong>Typical Example & Abstraction</strong>
    </td>
    <td>
      <p>
        The Map of Early Modern London Personography dataset states that Sir
        Nicholas Bacon is the subject of a webpage.
      </p>
      <p>
        <code>
          &lt;moeml:BACO1> → crm:P129i_is_subject_of →{" "}
          <strong>crm:E73_Information_Object</strong>
          <br />→ crm:P2_has_type → &lt;wikidata:Q36774>
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td>
      <strong>Edge Case Example & Abstraction</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Resource Links</strong>
    </td>
    <td>
      <p>
        The Wikimedia Foundation. (2021). <em>Wikidata</em>.{" "}
        <a href="https://www.wikidata.org/">https://www.wikidata.org/</a>
      </p>
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Discussion Elements Pertaining to the Pattern</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>MoEML Elements Following this Pattern</strong>
    </td>
    <td>MoEML dc:source</td>
  </tr>
</table>


**Pattern in TTL:**

```turtle
<MoEML:Person> a crm:E21_Person ; 
	rdfs:label "<MoEML:Person>" ; 
	crm:P129i_is_subject_of <page>.

<page> a crm:E73_Information_Object ; 
	rdfs:label "<page>" ; 
	crm:P2_has_type wikidata:Q36774 .

wikidata:Q36774 a crm:E55_Type ; 
	rdfs:label "web page" .
```

<div className="primary-button-row">
  <PrimaryButton
    link="/docs/tools/sparql/yasgui"
    buttonName="SPARQL: Coming Soon"
    ></PrimaryButton>
</div>

##### Mentioned or Referenced by a Web Page or Website

![Application profile](/img/documentation/application-profile-moeml-personography-referwebpage-(c-LINCS).jpg)

<table>
  <tr>
    <td rowspan="2">
      <strong>Pattern/Structure Values</strong>
    </td>
    <td>
      <strong>Definition</strong>
    </td>
    <td>
      This pattern declares that a person is referred to by a web page or
      website.
    </td>
  </tr>
  <tr>
    <td>
      <strong>Abstraction</strong>
    </td>
    <td>
      <p>
        <code>
          crm:E21_Person → crm:P67i_is_referred_to_by →{" "}
          <strong>crm:E73_Information_Object</strong>
          <br />→ crm:P2_has_type → <strong>crm:E55_Type</strong>
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td rowspan="4">
      <strong>Content Values</strong>
    </td>
    <td>
      <strong>Type of Value</strong>
    </td>
    <td>Uniform Resource Identifier (URI)</td>
  </tr>
  <tr>
    <td>
      <strong>Expected Value</strong>
    </td>
    <td>URI from the MoEML Personography dataset</td>
  </tr>
  <tr>
    <td>
      <strong>Format/Requirements for the Value</strong>
    </td>
    <td>URI (preferably dereferenceable)</td>
  </tr>
  <tr>
    <td>
      <strong>Example Values</strong>
    </td>
    <td>
      &lt;https://masl.library.utoronto.ca/person/762>;
      &lt;https://mapoflondon.uvic.ca/>;
      &lt;https://doi.org/10.1093/ref:odnb/13391>
    </td>
  </tr>
  <tr>
    <td rowspan="2">
      <strong>Case Examples</strong>
    </td>
    <td>
      <strong>Typical Example & Abstraction</strong>
    </td>
    <td>
      <p>
        The Map of Early Modern London Personography dataset states that John
        Stokker is referred to by a webpage.
      </p>
      <p>
        <code>
          &lt;moeml:STOK10> → crm:P67i_is_referred_to_by →{" "}
          <strong>crm:E73_Information_Object</strong>
          <br />→ crm:P2_has_type → <strong>&lt;wikidata:Q36774></strong>
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td>
      <strong>Edge Case Example & Abstraction</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Resource Links</strong>
    </td>
    <td>
      <p>
        The Wikimedia Foundation. (2021). <em>Wikidata</em>.{" "}
        <a href="https://www.wikidata.org/">https://www.wikidata.org/</a>
      </p>
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Discussion Elements Pertaining to the Pattern</strong>
    </td>
    <td>Types: web page (wikidata:Q36774) and website (wikidata:Q35127)</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>MoEML Elements Following this Pattern</strong>
    </td>
    <td>MoEML foaf:page</td>
  </tr>
</table>


**Pattern in TTL:**

```turtle
<MoEML:Person> a crm:E21_Person ; 
	rdfs:label "<name>" ; 
	crm:P67i_is_referred_to_by <page>.

<page> a crm:E73_Information_Object ; 
	rdfs:label "<page>" ; 
	crm:P2_has_type wikidata:"<type>" .

wikidata:"<type>" a crm:E55_Type ; 
	rdfs:label "<type>" .
```

<div className="primary-button-row">
  <PrimaryButton
    link="/docs/tools/sparql/yasgui"
    buttonName="SPARQL: Coming Soon"
    ></PrimaryButton>
</div>

##### Note

![Application profile](/img/documentation/application-profile-moeml-personography-note-(c-LINCS).jpg)

<table>
  <tr>
    <td rowspan="2">
      <strong>Pattern/Structure Values</strong>
    </td>
    <td>
      <strong>Definition</strong>
    </td>
    <td>This pattern declares that a person has a note.</td>
  </tr>
  <tr>
    <td>
      <strong>Abstraction</strong>
    </td>
    <td>
      <code>
        crm:E21_Person → crm:P3_has_note → <strong>rdfs:Literal</strong>
      </code>
    </td>
  </tr>
  <tr>
    <td rowspan="4">
      <strong>Content Values</strong>
    </td>
    <td>
      <strong>Type of Value</strong>
    </td>
    <td>literal value (text)</td>
  </tr>
  <tr>
    <td>
      <strong>Expected Value</strong>
    </td>
    <td>Literal value (text) from the MoEML Personography dataset</td>
  </tr>
  <tr>
    <td>
      <strong>Format/Requirements for the Value</strong>
    </td>
    <td>rdfs:literal</td>
  </tr>
  <tr>
    <td>
      <strong>Example Values</strong>
    </td>
    <td>
      "Sheriff of London\n 1477-1478.\n Member of the Drapers’ Company. Not to
      be confused\n with John Stokker." ; "Sheriff of London\n 1337-1338.\n
      Member of the Bladers’ Company."; "Wife of Andrew Pikeman. Buried at St.\n
      Botolph, Billingsgate."
    </td>
  </tr>
  <tr>
    <td rowspan="2">
      <strong>Case Examples</strong>
    </td>
    <td>
      <strong>Typical Example & Abstraction</strong>
    </td>
    <td>
      <p>
        The Map of Early Modern London Personography dataset states that John
        Stokker has the following note: “Sheriff of London\n 1477-1478.\n Member
        of the Drapers’ Company. Not to be confused\n with John Stokker."
      </p>
      <p>
        <code>
          &lt;moeml:STOK10> → crm:P3_has_note →{" "}
          <strong>
            “Sheriff of London\n 1477-1478.\n Member of the Drapers’ Company.
            Not to be confused\n with John Stokker."
          </strong>
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td>
      <strong>Edge Case Example & Abstraction</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Resource Links</strong>
    </td>
    <td>
      <p>
        MoEML. (2022, May 5). <em>Map of Early Modern London</em>.{" "}
        <a href="https://mapoflondon.uvic.ca/">https://mapoflondon.uvic.ca/</a>
      </p>
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Discussion Elements Pertaining to the Pattern</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>MoEML Elements Following this Pattern</strong>
    </td>
    <td>MoEML bio:olb</td>
  </tr>
</table>


**Pattern in TTL:**

```turtle
<MoEML:Person> a crm:E21_Person ; 
	rdfs:label "<MoEML:Person>" ; 
	crm:P3_has_note "<note>".
```

<div className="primary-button-row">
  <PrimaryButton
    link="/docs/tools/sparql/yasgui"
    buttonName="SPARQL: Coming Soon"
    ></PrimaryButton>
</div>

### Persons

#### Birth 

![Application profile](/img/documentation/application-profile-moeml-personography-birth-(c-LINCS).jpg)

<table>
  <tr>
    <td rowspan="2">
      <strong>Pattern/Structure Values</strong>
    </td>
    <td>
      <strong>Definition</strong>
    </td>
    <td>This pattern declares that a person was born.</td>
  </tr>
  <tr>
    <td>
      <strong>Abstraction</strong>
    </td>
    <td>crm:E21_Person → crm:P98i_was_born → crm:E67_Birth</td>
  </tr>
  <tr>
    <td rowspan="4">
      <strong>Content Values</strong>
    </td>
    <td>
      <strong>Type of Value</strong>
    </td>
    <td>Uniform Resource Identifier (URI)</td>
  </tr>
  <tr>
    <td>
      <strong>Expected Value</strong>
    </td>
    <td>URI minted by LINCS</td>
  </tr>
  <tr>
    <td>
      <strong>Format/Requirements for the Value</strong>
    </td>
    <td>URI (preferably dereferenceable)</td>
  </tr>
  <tr>
    <td>
      <strong>Example Values</strong>
    </td>
    <td>
      &lt;http://temp.lincsproject.ca/persons/birth/JOAN5>;
      &lt;http://temp.lincsproject.ca/persons/birth/ALEX4>;
      &lt;http://temp.lincsproject.ca/persons/birth/FERD2>
    </td>
  </tr>
  <tr>
    <td rowspan="2">
      <strong>Case Examples</strong>
    </td>
    <td>
      <strong>Typical Example & Abstraction</strong>
    </td>
    <td>
      <p>
        The Map of Early Modern London Personography dataset declares that Sir
        Nicholas Bacon was born.
      </p>
      <p>
        <code>
          &lt;moeml:BACO1> → crm:P98i_was_born → &lt;lincs:birth/BACO13>
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td>
      <strong>Edge Case Example & Abstraction</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Resource Links</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Discussion Elements Pertaining to the Pattern</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>MoEML Elements Following this Pattern</strong>
    </td>
    <td>MoEML bio:Birth</td>
  </tr>
</table>


**Pattern in TTL:**

```turtle
<MoEML:Person> a crm:E21_Person ; 
	rdfs:label "<name>" ; 
	crm:P98i_was_born <Birth_Event> .

<Birth_Event> a crm:E67_Birth ; 
	rdfs:label "Birth event of <name>" .
```

<div className="primary-button-row">
  <PrimaryButton
    link="/docs/tools/sparql/yasgui"
    buttonName="SPARQL: Coming Soon"
    ></PrimaryButton>
</div>

##### Birth Date

![Application profile](/img/documentation/application-profile-moeml-personography-birthdate-(c-LINCS).jpg)

<table>
  <tr>
    <td rowspan="2">
      <strong>Pattern/Structure Values</strong>
    </td>
    <td>
      <strong>Definition</strong>
    </td>
    <td>This pattern declares that a person was born on a date.</td>
  </tr>
  <tr>
    <td>
      <strong>Abstraction</strong>
    </td>
    <td>
      <p>
        <code>
          crm:E67_Birth
          <br />→ crm:P4_has_time_span → crm:E52_Time_Span
          <br />→ crm:P82_at_some_time_within → <strong>rdfs:Literal</strong>
          <br />→ crm:P82a_begin_of_the_begin → <strong>xsd:dateTime</strong>
          <br />→ crm:P82b_end_of_the_end → <strong>xsd:dateTime</strong>
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td rowspan="4">
      <strong>Content Values</strong>
    </td>
    <td>
      <strong>Type of Value</strong>
    </td>
    <td>literal value (text); date-time value</td>
  </tr>
  <tr>
    <td>
      <strong>Expected Value</strong>
    </td>
    <td>
      Literal value (text) from the MoEML Personography dataset; date-time value
      from the MoEML Personography dataset
    </td>
  </tr>
  <tr>
    <td>
      <strong>Format/Requirements for the Value</strong>
    </td>
    <td>rdfs:literal; xsd:dateTime</td>
  </tr>
  <tr>
    <td>
      <strong>Example Values</strong>
    </td>
    <td>“1513-08-14”, “0899”; “1545”</td>
  </tr>
  <tr>
    <td rowspan="2">
      <strong>Case Examples</strong>
    </td>
    <td>
      <strong>Typical Example & Abstraction</strong>
    </td>
    <td>
      <p>
        The Map of Early Modern London Personography dataset declares that Sir
        Nicholas Bacon was born in 1510.
      </p>
      <p>
        <code>
          &lt;moeml:BACO1> → crm:P98i_was_born → crm:E67_Birth
          <br />→ crm:P4_has_time_span → crm:E52_Time_Span
          <br />→ crm:P82_at_some_time_within → <strong>“1510”</strong>
          <br />→ crm:P82a_begin_of_the_begin → <strong>“1510-01-01”</strong>
          <br />→ crm:P82b_end_of_the_end → <strong>“1510-12-31”</strong>
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td>
      <strong>Edge Case Example & Abstraction</strong>
    </td>
    <td>
      <p>bio:precedingEvent: Only P82, P82a</p>
      <p>
        The Map of Early Modern London Personography dataset declares that
        Edward the Confessor was born after 1003.
      </p>
      <p>
        <code>
          &lt;moeml:EDWA7> → crm:P98i_was_born → crm:E67_Birth
          <br />→ crm:P4_has_time_span → crm:E52_Time_Span
          <br />→ crm:P82_at_some_time_within → <strong>“After 1510”</strong>
          <br />→ crm:P82a_begin_of_the_begin → <strong>“1510-12-31”</strong>
          <br />
          bio:followingEvent: Only P82, P82b
        </code>
      </p>
      <p>
        The Map of Early Modern London Personography dataset declares that
        Christine Barantyn was born before 1415.
      </p>
      <p>
        <code>
          &lt;moeml:BARA2> → crm:P98i_was_born → crm:E67_Birth
          <br />→ crm:P4_has_time_span → crm:E52_Time_Span
          <br />→ crm:P82_at_some_time_within → <strong>“Before 1415”</strong>
          <br />→ crm:P82b_end_of_the_end → <strong>“1415-01-01”</strong>
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Resource Links</strong>
    </td>
    <td>
      <p>
        W3C. (2012, April 5).{" "}
        <em>W3C XML Schema Definition Language (XSD) 1.1 Part 1: Structures</em>
        .{" "}
        <a href="https://www.w3.org/TR/xmlschema11-1/">
          https://www.w3.org/TR/xmlschema11-1/
        </a>
      </p>
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Discussion Elements Pertaining to the Pattern</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>MoEML Elements Following this Pattern</strong>
    </td>
    <td>MoEML bio:Birth</td>
  </tr>
</table>


**Pattern in TTL:**

```turtle
<Birth_Event> a crm:E67_Birth ; 
	rdfs:label "Birth event of <name>" ;
	crm:P4_has_time-span <Birth_Date> .

<Birth_Date> a crm:E52_Time-Span ; 
	rdfs:label "Date of birth of <name>" ; 
	crm:P82_at_some_time_within "<Birth_Date>" ;
	crm:P82a_begin_of_the_begin "<Birth_Date>"^^xsd:dateTime ; 
	crm:P82b_end_of_the_end "<Birth_Date>"^^xsd:dateTime .
```

<div className="primary-button-row">
  <PrimaryButton
    link="/docs/tools/sparql/yasgui"
    buttonName="SPARQL: Coming Soon"
    ></PrimaryButton>
</div>

#### Death 

![Application profile](/img/documentation/application-profile-moeml-personography-death-(c-LINCS).jpg)

<table>
  <tr>
    <td rowspan="2">
      <strong>Pattern/Structure Values</strong>
    </td>
    <td>
      <strong>Definition</strong>
    </td>
    <td>This pattern declares that a person died.</td>
  </tr>
  <tr>
    <td>
      <strong>Abstraction</strong>
    </td>
    <td>
      <code>crm:E21_Person → crm:P100i_died_in→ crm:E69_Death</code>
    </td>
  </tr>
  <tr>
    <td rowspan="4">
      <strong>Content Values</strong>
    </td>
    <td>
      <strong>Type of Value</strong>
    </td>
    <td>Uniform Resource Identifier (URI)</td>
  </tr>
  <tr>
    <td>
      <strong>Expected Value</strong>
    </td>
    <td>URI minted by LINCS</td>
  </tr>
  <tr>
    <td>
      <strong>Format/Requirements for the Value</strong>
    </td>
    <td>URI (preferably dereferenceable)</td>
  </tr>
  <tr>
    <td>
      <strong>Example Values</strong>
    </td>
    <td>
      &lt;http://temp.lincsproject.ca/persons/death/WALS1>;
      &lt;http://temp.lincsproject.ca/persons/death/WEST17>;
      &lt;http://temp.lincsproject.ca/persons/death/DALL3>
    </td>
  </tr>
  <tr>
    <td rowspan="2">
      <strong>Case Examples</strong>
    </td>
    <td>
      <strong>Typical Example & Abstraction</strong>
    </td>
    <td>
      <p>
        The Map of Early Modern London Personography dataset declares that
        Christine Barantyn died.
      </p>
      <p>
        <code>
          &lt;moeml:BARA2> → crm:P100i_died_in → &lt;moeml:death/BARA2>
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td>
      <strong>Edge Case Example & Abstraction</strong>
    </td>
    <td>
      <p>bio:precedingEvent: Only P82, P82a</p>
      <p>
        The Map of Early Modern London Personography dataset declares that
        Ælfwine of Elmham died after 1023.
      </p>
      <p>
        <code>
          &lt;https://mapoflondon.uvic.ca/AELF1> → crm:P100i_died_in →
          crm:E69_Death
          <br />→ crm:P4_has_time_span → crm:E52_Time_Span
          <br />→ crm:P82_at_some_time_within → <strong>“After 1023”</strong>
          <br />→ crm:P82a_begin_of_the_begin → <strong>“1023-12-31”</strong>
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Resource Links</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Discussion Elements Pertaining to the Pattern</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>MoEML Elements Following this Pattern</strong>
    </td>
    <td>MoEML bio:Death</td>
  </tr>
</table>


**Pattern in TTL:**

```turtle
<MoEML:Person> a crm:E21_Person ; 
	rdfs:label "<MoEML:Person>" ; 
	crm:P100i_died_in <Death_Event> .

<Death_Event> a crm:E69_Death ; 
	rdfs:label "Death event of <name>" .
```

<div className="primary-button-row">
  <PrimaryButton
    link="/docs/tools/sparql/yasgui"
    buttonName="SPARQL: Coming Soon"
    ></PrimaryButton>
</div>

##### Death Date

![Application profile](/img/documentation/application-profile-moeml-personography-deathdate-(c-LINCS).jpg)

<table>
  <tr>
    <td rowspan="2">
      <strong>Pattern/Structure Values</strong>
    </td>
    <td>
      <strong>Definition</strong>
    </td>
    <td>This pattern declares that a person died on a date.</td>
  </tr>
  <tr>
    <td>
      <strong>Abstraction</strong>
    </td>
    <td>
      <p>
        <code>
          crm:E21_Person → crm:P100i_died_in→ crm:E69_Death
          <br />→ crm:P4_has_time_span → crm:E52_Time_Span
          <br />→ crm:P82_at_some_time_within → <strong>rdfs:Literal</strong>
          <br />→ crm:P82a_begin_of_the_begin → <strong>xsd:dateTime</strong>
          <br />→ crm:P82b_end_of_the_end → <strong>xsd:dateTime</strong>
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td rowspan="4">
      <strong>Content Values</strong>
    </td>
    <td>
      <strong>Type of Value</strong>
    </td>
    <td>literal value (text); date-time value</td>
  </tr>
  <tr>
    <td>
      <strong>Expected Value</strong>
    </td>
    <td>
      Literal value (text) from the MoEML Personography dataset; date-time value
      from the MoEML Personography dataset
    </td>
  </tr>
  <tr>
    <td>
      <strong>Format/Requirements for the Value</strong>
    </td>
    <td>Rdfs:literal; xsd:dateTime</td>
  </tr>
  <tr>
    <td>
      <strong>Example Values</strong>
    </td>
    <td>“1427”, “1571-10-27”; “1390”</td>
  </tr>
  <tr>
    <td rowspan="2">
      <strong>Case Examples</strong>
    </td>
    <td>
      <strong>Typical Example & Abstraction</strong>
    </td>
    <td>
      <p>
        The Map of Early Modern London Personography dataset declares that
        Christine Barantyn died in 1427.
      </p>
      <p>
        <code>
          &lt;https://mapoflondon.uvic.ca/BARA2> → crm:P100i_died_in →
          crm:E69_Death
          <br />→ crm:P4_has_time_span → crm:E52_Time_Span
          <br />→ crm:P82_at_some_time_within → <strong>“1427”</strong>
          <br />→ crm:P82a_begin_of_the_begin → <strong>“1427-01-01”</strong>
          <br />→ crm:P82b_end_of_the_end → <strong>“1427-12-31”</strong>
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td>
      <strong>Edge Case Example & Abstraction</strong>
    </td>
    <td>
      <p>bio:precedingEvent: Only P82, P82a</p>
      <p>
        The Map of Early Modern London Personography dataset declares that
        Ælfwine of Elmham died after 1023.
      </p>
      <p>
        <code>
          &lt;https://mapoflondon.uvic.ca/AELF1> → crm:P100i_died_in →
          crm:E69_Death
          <br />→ crm:P4_has_time_span → crm:E52_Time_Span
          <br />→ crm:P82_at_some_time_within → <strong>“After 1023”</strong>
          <br />→ crm:P82a_begin_of_the_begin → <strong>“1023-12-31”</strong>
        </code>
      </p>
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Resource Links</strong>
    </td>
    <td>
      <p>
        W3C. (2012, April 5).{" "}
        <em>W3C XML Schema Definition Language (XSD) 1.1 Part 1: Structures</em>
        .{" "}
        <a href="https://www.w3.org/TR/xmlschema11-1/">
          https://www.w3.org/TR/xmlschema11-1/
        </a>
      </p>
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>Discussion Elements Pertaining to the Pattern</strong>
    </td>
    <td>N/A</td>
  </tr>
  <tr>
    <td colspan="2">
      <strong>MoEML Elements Following this Pattern</strong>
    </td>
    <td>MoEML bio:Death</td>
  </tr>
</table>


**Pattern in TTL:**

```turtle
<Death_Event> a crm:E69_Death ; 
	rdfs:label "Death event of <name>" ; 
	crm:P4_has_time_span <Death_Date> .

<Death_Date> a crm:E52_Time-Span ;
	rdfs:label "Date of death of <name>" ; 
	crm:P82_at_some_time_within "<Death_Date>" ; 
	crm:P82a_begin_of_the_begin "<Death_Date>"^^<xsd:dateTime ; 
	crm:P82b_end_of_the_end "<Death_Date>"^^<xsd:dateTime .
```

<div className="primary-button-row">
  <PrimaryButton
    link="/docs/tools/sparql/yasgui"
    buttonName="SPARQL: Coming Soon"
    ></PrimaryButton>
</div>
