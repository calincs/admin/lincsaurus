---
id: metadata
title: Metadata
definition: Informations structurées qui décrivent ou expliquent un objet d’information afin qu’il puisse être recherché, récupéré, contextualisé, validé, conservé ou géré.
Machine Translation: True     
Translation Tool: DeepL 
Translated Added By: Humna Chaudhry(@humnq)
Last Translated: 2024-06-21
---

:::info
Cette page web a été traduite automatiquement par DeepL. Bien que nous nous efforcions d’être précis, nous vous informons que les traductions peuvent contenir des erreurs ou des inexactitudes. Pour obtenir les informations les plus précises, veuillez vous référer à la version originale.
:::

Les métadonnées sont des données structurées sur les données. Il s'agit d'informations qui décrivent ou expliquent un objet d'information afin qu'il puisse être recherché, récupéré, contextualisé, validé, préservé ou géré. Il existe de nombreux types de métadonnées, notamment

- **Administratives:** Les métadonnées qui aident à gérer ou à administrer un objet d'information (par exemple, le type de ressource, le numéro de version, les droits de propriété intellectuelle, la provenance, les informations de conservation, les enregistrements de circulation).
- Les métadonnées descriptives aident à identifier, authentifier, décrire, découvrir ou retrouver un objet d'information (par exemple, le titre, l'auteur, l'éditeur, la date de publication, le résumé, les mots-clés).
- Les métadonnées structurelles : elles permettent d'encoder les relations au sein des objets et entre eux grâce à des éléments tels que des liens vers d'autres composants (par exemple, la façon dont les pages sont assemblées pour former un chapitre, le fonctionnement d'un système).

Pour garantir la cohérence et l'utilité des métadonnées, il est important de suivre une norme de métadonnées : une manière documentée de structurer et de mettre en œuvre les métadonnées, généralement utilisée dans un domaine ou une discipline particulière.

## Exemples

- Première page d'un livre imprimé (titre, auteur, éditeur, date, etc.)
- Informations contextuelles stockées dans une photographie numérique (marque de l'appareil photo, heure à laquelle la photographie a été prise, résolution de l'image, coordonnées GPS, etc.)
- Informations contextuelles sur les données d'un système d'information géographique (SIG) (qualité des données, organisme source, système de coordonnées, contraintes d'accès et d'utilisation, etc.)
