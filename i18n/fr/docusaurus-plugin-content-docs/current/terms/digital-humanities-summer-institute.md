---
id: digital-humanities-summer-institute
title: Institut d’été des humanités numériques (DHSI)
definition: Un institut annuel de formation en bourses numériques organisé à l’Université de Victoria.
---

Le Digital Humanities Summer Institute (DHSI) est un institut annuel de formation en bourses numériques organisé à l’Université de Victoria. DHSI rassemble des professeurs, du personnel et des étudiants des communautés des arts, des sciences humaines, des bibliothèques et des archives pour discuter et apprendre de nouvelles technologies et méthodes par le biais de cours, de séminaires et de conférences.

## Autres ressources

- Digital Humanities Summer Institute (2022) [“About DHSI”](https://dhsi.org/about/)
