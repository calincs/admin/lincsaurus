---
id: entity
title: Entity
definition: A discrete thing, often described as the subject and object (or the domain and range) of a triple (subject-predicate-object).
---

Entities are discrete things in existence, which can be linked together using attributes that build relationships between them. In :Term[Linked Open Data (LOD)]{#linked-open-data}, entities are often described as the subject and object (or the :Term[domain]{#domain} and :Term[range]{#range}) of an :Term[triple]{#triple} (subject-predicate-object).

## Examples

- CIDOC CRM (2022) [“E1 CRM Entity in version 7.1.1”](https://cidoc-crm.org/Entity/E1-CRM-Entity/version-7.1.1)

## Further Resources

- [Entity (Wikipedia)](https://en.wikipedia.org/wiki/Entity)
