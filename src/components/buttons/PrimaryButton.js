/** @format */

import React from "react";
import Link from "@docusaurus/Link";
import styles from "./buttons.module.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faUpRightFromSquare } from "@fortawesome/free-solid-svg-icons";

function PrimaryButton({ link, buttonName, icon = null, ariaLabel = null }) {
  const title = typeof buttonName == "object" ? buttonName.props.children : buttonName;

  if (icon) {
    return (
      <Link className={styles.primaryButton} to={link} title={title} aria-label={ariaLabel}>
        {buttonName}
        <div className={styles.iconShift}>{icon ? <FontAwesomeIcon icon={icon} /> : null}</div>
      </Link>
    );
  }

  return (
    <Link className={styles.primaryButton} title={title} to={link} aria-label={ariaLabel}>
      {buttonName}
      {icon ? <FontAwesomeIcon icon={icon} /> : null}
    </Link>
  );
}

export default PrimaryButton;
