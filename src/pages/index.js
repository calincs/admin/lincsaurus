/** @format */

import React from "react";
import clsx from "clsx";
import Layout from "@theme/Layout";
import { useLocation } from "react-router-dom";
import Translate, { translate } from "@docusaurus/Translate";
import styles from "./index.module.css";
import HomepageFeatures from "../components/HomepageFeatures";
import HomepageCarousel from "../components/HomepageCarousel";
import HomepageTileBanner from "../components/HomepageTileBanner";
import HomepageTOD from "../components/HomepageTOD";
import StaticCard from "../components/cards/StaticCard";
import StatCard from "../components/cards/StatCard";
import Link from "@docusaurus/Link";
import HeroButton from "../components/buttons/HeroButton";
import GridLayout from "../components/layouts/GridLayout";
import ToolCatalogue from "../components/lists/toolCatalogue.js";
import { EventList, filterEvents } from "../components/lists/EventList";
import blogImage1 from "../../blog/2024-02-26-all-about-people/busa-punchcard-operators-(cc-by-nc-CIRCSE-research-centre).jpeg";
import blogImage2 from "../../blog/2023-08-22-responsive-web/responsive-web-unsplash-(cc0).jpg";
import lincsLogo from "@site/static/img/index-page-images/lincs-stacked-logo-(c-LINCS).png";
import frenchLINCSLogo from "@site/static/img/logos/lincs-logos/LINCS-French-dark-(c-LINCS).png";
function HomepageHeader() {
  const location = useLocation();

  return (
    <header className={clsx("hero hero--primary", styles.heroBanner)}>
      <div className="hero-container">
        {location.pathname == "/fr/" ? (
          <img src={frenchLINCSLogo} alt="LINCS Logo" className="hero-image" />
        ) : (
          <img src={lincsLogo} alt="LINCS Logo" className="hero-image" />
        )}
        <div className="hero-content">
          <h1>
            <Translate
              id="homepage.heroBanner.h1"
              description="The h1 tag for the hero banner">
              Create and explore cultural data
            </Translate>
          </h1>
          <p>
            <Translate
              id="homepage.heroBanner.p"
              description="The p tag for the hero banner">
              LINCS provides the tools and infrastructure to make humanities
              data more discoverable, searchable, and shareable. Discover how
              you can explore, create, and publish cultural data.
            </Translate>
          </p>
        </div>
      </div>
    </header>
  );
}

export default function Home() {
  return (
    <Layout
      title={`Home`}
      description="Documentation site for the LINCS project">
      <HomepageHeader />
      <section className={styles.homeSection}>
        <div className={styles.container}>
          <div className="hero-primary-button-row">
            <HeroButton
              link="/docs/about-lincs/"
              buttonName={translate({
                message: "What is the LINCS Project?",
              })}
            />

            <HeroButton
              link="/docs/learn-lod"
              buttonName={translate({
                message: "How do I get started with Linked Open Data? ",
              })}
            />

            <HeroButton
              link="/docs/explore-lod/project-datasets/"
              buttonName={translate({
                message: "What datasets does LINCS have?",
              })}
            />
            <HeroButton
              link="/docs/about-lincs/get-involved"
              buttonName={translate({
                message: "How do I get involved with LINCS?",
              })}
            />
          </div>
        </div>
      </section>

      <div>
        <main>
          <section className={styles.homeSection}>
            <div className={styles.container}>
              <h2 className={styles.header}>
                <Translate
                  id="homepage.carousel.header"
                  description="The carousel header">
                  Explore cultural data with LINCS
                </Translate>
              </h2>
              <p>
                <Translate>
                  LINCS data is published through the ResearchSpace platform,
                  where you can browse entities and investigate connections.
                </Translate>
              </p>
              <HomepageTileBanner />
              <GridLayout>
                <StatCard
                  heading={"10,000,000"}
                  text={translate({ message: "Triples in Progress" })}
                  queryType={"triples"}
                />
                <StatCard
                  heading={"400,000"}
                  text={translate({
                    message: "People, Places, Groups, Events, and Objects",
                  })}
                  queryType={"entity types"}
                />
                <StatCard
                  heading={"10"}
                  text={translate({ message: "Datasets in Progress" })}
                  queryType={"datasets"}
                />
                <StatCard
                  heading={"175"}
                  text={translate({
                    message: "Researchers, Students, Staff and Partners",
                  })}
                />
              </GridLayout>
            </div>
          </section>

          <section className={styles.homeSection}>
            <div className={styles.container}>
              <Link to={"/docs/explore-lod/project-datasets/"}>
                <h2 className={styles.header}>
                  <Translate>LINCS Datasets</Translate>
                </h2>
              </Link>
              <HomepageCarousel />
            </div>
          </section>

          {Object.keys(filterEvents("Ongoing")).length > 0 && (
            <section className={styles.homeSection}>
              <div className={styles.container}>
                <Link
                  to={"/docs/about-lincs/get-involved/events/#ongoing-events"}>
                  <h2 className={styles.header}>
                    <Translate
                      id="homepage.ongoing-events.header"
                      description="The ongoing events header">
                      Ongoing Events
                    </Translate>
                  </h2>
                </Link>
                <EventList filter="Ongoing" />
              </div>
            </section>
          )}
          {Object.keys(filterEvents("Upcoming")).length > 0 && (
            <section className={styles.homeSection}>
              <div className={styles.container}>
                <Link
                  to={"/docs/about-lincs/get-involved/events/#upcoming-events"}>
                  <h2 className={styles.header}>
                    <Translate
                      id="homepage.upcoming-events.header"
                      description="The upcoming events header">
                      Upcoming Events
                    </Translate>
                  </h2>
                </Link>
                <EventList filter="Upcoming" />
              </div>
            </section>
          )}

          <section className={styles.homeSection}>
            <div className={styles.container}>
              <h2 className={styles.header}>
                <Translate>Featured Tools</Translate>
              </h2>
              <ToolCatalogue preFilter="featured" sortOrder="featured" />
            </div>
          </section>

          <section className={styles.homeSection}>
            <div className={styles.container}>
              <h2 className={styles.header}>
                <Translate>Learn More with LINCS</Translate>
              </h2>
              <HomepageTOD
                buttonName1={translate({
                  id: "homepage.tod.learnMoreButton",
                  message: "Learn More",
                  description: "The term of the day Learn More button",
                })}
                link1="/docs/learn-lod/glossary"
                buttonName2={translate({
                  id: "homepage.tod.viewGlossaryButton",
                  message: "View Glossary",
                  description: "The term of the day View Glossary button",
                })}
              />
            </div>
          </section>

          <section className={styles.homeSection}>
            <div className={styles.container}>
              <Link to={"/blog/"}>
                <h2 className={styles.header}>
                  <Translate>Blog Posts</Translate>
                </h2>
              </Link>

              <div className={styles.blogPreview}>
                <GridLayout>
                  <StaticCard
                    src={blogImage1}
                    title={translate({
                      id: "homepage.staticCard.blog1Title",
                      message: "It's All About the People",
                      description: "The spotlight blog title",
                    })}
                    description={translate({
                      id: "homepage.staticCard.blog1Description",
                      message:
                        "I am surprised and thrilled that someone thought it worth nominating me for the Roberto Busa Prize, and overwhelmed to have been placed by ADHO in such illustrious company, fully aware that there is so much superb work in our community...",
                      description: "The spotlight blog description",
                    })}
                    buttonName1={translate({
                      id: "homepage.staticCard.readMoreButton",
                      message: "Read More",
                      description: "The spotlight Read More button label",
                    })}
                    buttonName2={translate({
                      id: "homepage.staticCard.viewAllBlogsButton",
                      message: "View All Blog Posts",
                      description: "The spotlight View All Blogs button label",
                    })}
                    link1="/blog/all-about-people"
                    link2="/blog"
                  />
                  <StaticCard
                    src={blogImage2}
                    title={translate({
                      id: "homepage.staticCard.blog2Title",
                      message:
                        "Designing and Building Responsive Web Applications",
                      description: "The spotlight blog title",
                    })}
                    description={translate({
                      id: "homepage.staticCard.blog2Description",
                      message:
                        "When designing and building a web application, ensuring that it is responsive is paramount. A responsive application looks good and functions well on all screen sizes and devices. LINCS applications are being designed to be viewed in a wide range of ways: from tablets to laptops to whiteboard-sized interactive screens.",
                      description: "The spotlight blog description",
                    })}
                    buttonName1={translate({
                      id: "homepage.staticCard.readMoreButton",
                      message: "Read More",
                      description: "The spotlight Read More button label",
                    })}
                    buttonName2={translate({
                      id: "homepage.staticCard.viewAllBlogsButton",
                      message: "View All Blog Posts",
                      description: "The spotlight View All Blogs button label",
                    })}
                    link1="/blog/responsive-web"
                    link2="/blog"
                  />
                </GridLayout>
              </div>
            </div>
          </section>
        </main>
      </div>
    </Layout>
  );
}
