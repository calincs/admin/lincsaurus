---
id: event-oriented-ontology
title: Ontologie orientée événement
definition: Une ontologie qui utilise des événements pour relier les choses, les concepts, les personnes, le temps et le lieu.
Machine Translation: True     
Translation Tool: DeepL 
Translated Added By: Humna Chaudhry(@humnq)
Last Translated: 2024-06-21
---

:::info
Cette page web a été traduite automatiquement par DeepL. Bien que nous nous efforcions d’être précis, nous vous informons que les traductions peuvent contenir des erreurs ou des inexactitudes. Pour obtenir les informations les plus précises, veuillez vous référer à la version originale.
:::

Une :Term[ontologie]{#ontology} orientée vers les événements utilise les événements pour relier les choses, les concepts, les personnes, le temps et le lieu ([Fichtner & Ribaud](https://www.researchgate.net/publication/267462327_Paths_and_Shortcuts_in_an_Event-Oriented_Ontology)). Les ontologies orientées événements placent les événements au centre de leur structure et décrivent les informations comme les résultats ou les sorties de ces événements, par opposition aux :Term[ontologies orientées objets]{#object-oriented-ontology}, qui placent les objets au centre.

L'utilisation d'une ontologie orientée événement permet la création de :Term[metadata]{#metadata} qui fournissent un parcours plus complet des activités humaines, facilitant ainsi l'apprentissage automatique. En outre, la modélisation explicite des événements conduit à une intégration plus complète des informations culturelles tout en permettant l'interconnexion, la médiation et l'échange d'ensembles de données hétérogènes sur le patrimoine culturel.

## Exemples

- L'exemple suivant montre un livre modélisé comme le produit d'un "événement de création", avec l'auteur et la date de publication comme aspects de cet événement.

![Indian Horse, Richard Wagamese et l'année 2012 sont les résultats d'un événement de création](</img/documentation/glossary-event-oriented-ontology-(c-LINCS).jpg>)

## Autres Ressources

- CIDOC CRM (2021) _[Volume A: Definition of the CIDOC Conceptual Reference Model, 7.1.1](https://cidoc-crm.org/sites/default/files/cidoc_crm_v.7.1.1_0.pdf)_
- Fichtner & Ribaud (2012) [“Paths and Shortcuts in an Event-Oriented Ontology”](https://www.researchgate.net/publication/267462327_Paths_and_Shortcuts_in_an_Event-Oriented_Ontology)
