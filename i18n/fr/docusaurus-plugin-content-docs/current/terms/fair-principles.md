---
id: fair-principles
title: Principes FAIR
definition: Un ensemble de principes (trouvable, accessibilité, interopérabilité et réutilisabilité) pour la gestion et la gestion des données et des métadonnées.
Machine Translation: True     
Translation Tool: DeepL 
Translated Added By: Humna Chaudhry(@humnq)
Last Translated: 2024-06-21
---

:::info
Cette page web a été traduite automatiquement par DeepL. Bien que nous nous efforcions d’être précis, nous vous informons que les traductions peuvent contenir des erreurs ou des inexactitudes. Pour obtenir les informations les plus précises, veuillez vous référer à la version originale.
:::

Les principes FAIR sont un ensemble de principes pour la gestion et l'intendance des données et :Term[metadata]{#metadata} : trouvabilité, accessibilité, interopérabilité et réutilisation. L'acronyme a été inventé par un consortium de scientifiques et d'organisations dans [Scientific Data](https://www.nature.com/articles/sdata201618) (2016) pour améliorer la gestion des actifs numériques.

Pour que des données soient "FAIR", elles doivent être.. :

- **Trouvables:** Les données doivent être faciles à localiser pour les humains et les ordinateurs.
- **Accessibles:** Les données doivent être accessibles aux utilisateurs.
- **Interopérables** : les données doivent pouvoir être intégrées à d'autres données et interopérables avec des applications et des flux de travail pour l'analyse, le stockage et le traitement.
- **Réutilisables** : les données doivent être bien décrites afin de pouvoir être utilisées dans différents contextes.

FAIR met l'accent sur la facilité d'utilisation par les machines, car l'augmentation du volume et de la complexité des données a rendu les gens plus dépendants de l'aide informatique.

## Autres Ressources

- Data.org (2024) [“The FAIR Data Principles”](https://data.org/resources/the-fair-data-principles/)
- [Fair Data (Wikipedia)](https://en.wikipedia.org/wiki/FAIR_data)
- GoFAIR (2014) [“FAIR Principles”](https://www.go-fair.org/fair-principles/)
- Statistics Canada (2022) [“FAIR data principles: What is FAIR?”](https://www.statcan.gc.ca/en/wtc/data-literacy/catalogue/892000062022002) [Video]
- Wilkinson, et al. (2016) [“The FAIR Guiding Principles for Scientific Data Management and Stewardship”](https://www.nature.com/articles/sdata201618)
