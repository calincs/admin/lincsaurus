---
id: web-annotation-data-model
title: Web Annotation Data Model (WADM)
definition: A standard for the formatting and structuring of web annotations.
Last Translated: 2024-06-21
---

Web Annotation Data Model (WADM) is a standard for the formatting and structuring of :Term[web annotations]{#web-annotation}. It was designed to make descriptions of web annotations consistent and therefore more shareable and reusable across different sources. LINCS uses WADM to describe the sources used in the datasets that are being transformed.

WADM is not an extension of :Term[CIDOC CRM]{#cidoc-crm}, nor is it an :Term[event-centric]{#event-oriented-ontology} by nature. While there is no pre-existing alignment between WADM and CIDOC CRM, however, there is overlap in people who work on the ontologies, which means that efforts are being made to find ways for WADM and CIDOC CRM to work together.

## Further Resources

- [Application Profile for Web Annotations](/docs/explore-lod/understand-lincs-data/application-profiles-main/sources-metadata)
- W3C (2017) [“Web Annotation Data Model”](https://www.w3.org/TR/annotation-model/)
