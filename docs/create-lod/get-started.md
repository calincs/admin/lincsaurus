---
sidebar_position: 1
title: "Get Started"
description: "Get Started - Prerequisites"
---



LINCS collaborates with scholars to help transform your research data into :Term[linked open data (LOD)]{#linked-open-data} and to publish that LOD as part of the LINCS :Term[Knowledge Graph]{#knowledge-graph}. Publishing data with LINCS assumes that you have existing data—though not necessarily LOD—as opposed to creating LOD from the start

The transformation and publication process is a collaborative one, where LINCS provides resources, tools, and advice to guide your team’s work.

## Knowledge Requirements

- It is helpful to have a basic understanding of [LOD basics](/docs/learn-lod/linked-open-data-basics) before beginning the transformation process. 
- Think about how your project and data will benefit from using LOD technologies and what research questions you seek to answer with your transformed data.

:::tip

- Start with [introductory resources](/docs/learn-lod/linked-open-data-basics) written by the LINCS team.
- View our [LINCS Zotero Library](https://www.zotero.org/groups/5145265/lincs-project/library) for recommended readings, training modules, videos, and tools.
- LINCS often leads workshops at the [DH@Guelph Summer Workshops](https://www.uoguelph.ca/arts/dhguelph), [DHSI](https://dhsi.org/), and [DHSITE](https://dhsite.org/). Check our [events page](/docs/about-lincs/get-involved/events/) for upcoming opportunities.


:::

## Data Requirements

- To contribute data to LINCS, you need to come with an existing dataset that fits within the [LINCS Areas of Inquiry](/docs/about-lincs/research#areas-of-inquiry) and one of our [four data categories](/docs/create-lod/workflow-paths): :Term[structured]{#structured-data}, :Term[semi-structured]{#semi-structured-data}, :Term[TEI]{#tei-data}, or :Term[natural language]{#natural-language-data}.
- Your dataset falls within the bounds of our Dataset Inclusion Policy (Coming Soon) {/* ADD LINK */}
- You own the source dataset or have permission from the source dataset owner
- You have a long-term plan to store the source dataset in an institutional repository, a [Trustworthy Digital Repository](https://en.wikipedia.org/wiki/Trustworthy_Repositories_Audit_%26_Certification) (such as [Scholars Portal](https://scholarsportal.info/)), or Canada’s [Federated Research Data Repository](https://www.frdr-dfdr.ca/repo/)
- Your dataset contents are publishable under Canadian law
- If your dataset involves people, you have ethics approval from your research institution (if applicable), and you meet the criteria set out in the [Tri-Council Policy Statement: Ethical Conduct for Research Involving Humans](https://ethics.gc.ca/eng/policy-politique_tcps2-eptc2_2018.html)

When you publish with LINCS, you are making your dataset available to—and connecting it with data created by—others. LINCS publishes data under a [Creative Commons Attribution 4.0 License](https://creativecommons.org/licenses/by/4.0/). In some cases, we are able to work with data that is not open. We respect data sovereignty and adhere to the :Term[CARE Principles for Indigenous Data Governance]{#care-principles-for-indigenous-data-governance}.

Before we can prepare your data for publication, you need to sign the [Data Publication License Agreement](/docs/about-lincs/policies/data-publication-license-agreement) and complete the [Data Publication License Agreement Supplementary Form](https://docs.google.com/document/d/1dA01sGhzetsTytrhoIbrv5I4PXBkRNtXur0aSbGeHaM). We recommend you review these documents before starting to create LOD. For more information, see the Data Publication License Agreement Instructions (Coming Soon).{/* ADD LINK */}

## Research Team Requirements

- The transformation process is led by the data holders, or research team, with support from the LINCS team. Each research team needs to have a Project Lead who is able to make conceptual decisions about the datasets. It is also a good idea to have someone on the research team who is able to perform data extraction and manipulation (i.e., transform the data into a particular format).

- Many of the steps in the transformation process, especially :Term[entity matching]{#entity-matching}, can be completed by student research assistants who understand the source data and have a basic technical aptitude. These research assistants are generally hired by and work with the research team. LINCS will assist with the more difficult tasks, including :Term[mapping]{#mapping} the data to an :Term[ontology]{#ontology}.

## Next Step

Learn about the [transformation and publication workflows](/docs/create-lod/workflow-overview). 

