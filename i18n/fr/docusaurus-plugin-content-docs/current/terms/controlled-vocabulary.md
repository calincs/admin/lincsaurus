---
id: controlled-vocabulary
title: Vocabulaire contrôlé
definition: Un arrangement standardisé et organisé de mots et de phrases, qui fournit une manière cohérente de décrire les données.
---

Les vocabulaires contrôlés sont des arrangements normalisés et organisés de mots et de phrases qui fournissent une manière cohérente de décrire les données. Ils fournissent un langage concis et cohérent, permettant une recherche et une récupération plus faciles.

## Exemples

- [Getty Art & Architecture Thesaurus (AAT)](https://www.getty.edu/research/tools/vocabularies/aat/)
- [Getty Thesaurus of Geographic Names (TGN)](https://www.getty.edu/research/tools/vocabularies/tgn/)
- [Getty Union List of Artist Names (ULAN)](https://www.getty.edu/research/tools/vocabularies/ulan/)
- [Homosaurus](https://homosaurus.org/)
- [Library of Congress Name Authority File (NAF)](http://id.loc.gov/authorities/names.html)
- [Library of Congress Subject Headings (LCSH)](http://id.loc.gov/authorities/subjects.html)
- [Nomenclature](https://www.nomenclature.info/)
- [Virtual International Authority File (VIAF)](http://viaf.org/)

## Autres ressources

- [Controlled Vocabulary (Wikipedia)](https://en.wikipedia.org/wiki/Controlled_vocabulary)
- Harping (2010) [“What Are Controlled Vocabularies?”](https://www.getty.edu/research/publications/electronic_publications/intro_controlled_vocab/what.pdf)
- Library and Archives Canada (2018) [“Controlled Vocabularies”](https://library-archives.canada.ca/eng/services/government-canada/controlled-vocabularies-government-canada/Pages/controlled-vocabularies-government-canada.aspx)
