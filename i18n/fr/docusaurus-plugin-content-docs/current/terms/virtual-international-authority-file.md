---
id: virtual-international-authority-file
title: Fichier d’autorité international virtuel (VIAF)
definition: Un service qui regroupe les catalogues de nombreuses bibliothèques nationales et divers fichiers d’autorité.
---

Le Virtual International Authority File (VIAF) est un service qui regroupe les catalogues de nombreuses bibliothèques nationales et divers :Term[enregistrements d’autorité]{#authority-record} pour fournir un accès pratique aux principaux fichiers d’autorité de noms dans le monde. Au LINCS, VIAF est utilisé comme source de :Term[Uniform Resource Identifiers (URI)]{#uniform-resource-identifier} pour les personnes et pour les entités bibliographiques pendant :Term[rapprochement]{#entity-matching}.

## Autres ressources

- OCLC (2022) [“VIAF Overview”](https://www.oclc.org/en/viaf.html)
- [VIAF: The Virtual International Authority File](https://viaf.org/)
