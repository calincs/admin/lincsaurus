---
sidebar_position: 3
title: "Ontologies"
description: "Introduction à la cartographie, CIDOC CRM, WADM et OWL"
Last Translated: 2023-07-26
sidebar_class_name: "hide"
---
import ZoomableImage from "@site/src/components/imageGallery/ZoomableImage" ;

Pour que les :Term[Données ouvertes et liées (LOD)]{#linked-open-data} soient aussi cohérentes et précises que possible, elles doivent être conformes à une ontologie. Une ontologie crée des règles pour vos données, définissant exactement quels types d':Term[entités]{#entity} et de relations peuvent exister dans vos données. Les ontologies rendent sémantiques les liens entre vos points de données, ce qui signifie qu'un ordinateur peut lire ssddsset comprendre la signification spécifique des relations.

## Les ontologies en tant que visions du monde

En métaphysique, l'**ontologie** est l'étude philosophique de l'être, c'est-à-dire des éléments qui s'assemblent pour former le monde et de la manière dont ces éléments interagissent. Dans les sciences de l'information, les ontologies codent un modèle conceptuel - une façon abstraite de penser - d'une manière lisible par une machine. Plus précisément, pour les LOD, une ontologie formelle est un ensemble de classifications qui définissent quels sujets, objets et prédicats peuvent être utilisés pour décrire un certain ensemble de données.

Il est important de comprendre que toutes les ontologies présentent une vision du monde. En spécifiant ce qui est permis d'appartenir à un :Term[domaine]{#domain} et une :Term[plage]{#range}, une ontologie détermine ce qu'est une catégorie et comment différentes entités peuvent être reliées entre elles. On peut donc dire que les ontologies créent autant qu'elles décrivent. Nos données et les décisions que nous prenons sur la manière dont nous choisissons de les modéliser et de les représenter sont liées à des systèmes de pouvoir, d'agence et de contrôle. LINCS s'intéresse tout autant à la manière dont les structures que nous utilisons donnent un sens aux données qu'à l'établissement de liens entre les données.

LINCS vise à adopter des ontologies qui :

* soutiennent la nuance, la différence et la diversité, y compris les diverses façons de savoir
* reflètent une compréhension des catégories d'identité et de la classification sociale comme étant culturellement produites, croisées et discursives
* fournissent le contexte et la provenance des affirmations
* réduisent le risque de (re)codage du cissexisme, du racisme, du colonialisme, de l'hétéronormativité, du capacitisme, du classisme et d'autres formes de discrimination dans les données.

Pour en savoir plus sur l'utilisation des ontologies par LINCS, consultez  la [Politique d'adoption et de développement des ontologies](/docs/about-lincs/policies/ontologies-adoption-and-development) .

## Cartographie

L'adaptation d'un ensemble de données à une ontologie est appelée :Term[cartographie]{#mapping}. La cartographie forme les données dans une structure qui permet une représentation cohérente de tous les éléments décrits par l'ensemble de données et des relations entre eux. Certaines ontologies sont destinées à décrire une discipline ou un domaine général (par exemple, :Term[CIDOC CRM]{#cidoc-crm} couvre les données relatives au patrimoine culturel). D'autres, souvent appelées **ontologies de domaine**, sont conçues sur mesure pour des sujets plus spécifiques.

La mise en correspondance peut être un processus compliqué. Lorsque LINCS convertit des ensembles de données en LOD, l'équipe chargée de l'ontologie rédige un schéma conceptuel basé sur la structure de l'ensemble de données à convertir. Si possible, LINCS adaptera une cartographie existante s'il existe un jeu de données dans l':Term[entrepôt de triplets]{#triplestore} qui a déjà été converti et dont la structure et le contenu sont similaires à ceux du nouveau jeu de données. Les ensembles de données qui adhèrent à une ontologie ont tendance à être plus cohérents et plus précis dans la description de leurs données que les ensembles de données qui n'ont pas encore été cartographiés. Cette cohérence permet la création de LOD plus riches et plus utiles, en particulier lorsqu'une ontologie largement adoptée est utilisée.

## Orientations

La plupart des ontologies sont orientées soit vers les objets, soit vers les événements. :Term[Ontologies orientées sur l'objet]{#object-oriented-ontology} placent les objets au centre de leur structure et relient les éléments d'information qui sont des attributs de ces objets. Par exemple, un livre pourrait être modélisé comme un objet central, avec son auteur et sa date de publication comme attributs connectés :

![alt="The book Indian Horse has the properties of having the author Richard Wagamese and having the publication date 2012"](/img/documentation/ontologies-object-driven-ontology-(c-LINCS).jpg)

:Term[Les ontologies axées sur les événements]{#event-oriented-ontology} placent les événements au centre de leur structure et décrivent les résultats de ces événements. Par exemple, un livre peut être modélisé comme le produit d'un événement de création, l'auteur et la date de publication étant des aspects de cet événement :

![alt="The book Indian Horse is a product of a creation event. The author Richard Wagamese and publication date 2012 are aspects of that event."](/img/documentation/ontologies-event-driven-ontology-(c-LINCS).jpg)

## CIDOC CRM

LINCS utilise CIDOC CRM et ses extensions comme ontologie de base. CIDOC CRM est le modèle de référence conceptuel (CRM) du comité de documentation du Conseil international des musées. CIDOC CRM est une ontologie événementielle conçue pour modéliser les données du patrimoine culturel et a été développée à l'origine pour les musées et les archives.

CIDOC CRM contient une liste d'entités (c'est-à-dire de sujets) et de :Term[propriétés]{#property} (c'est-à-dire de prédicats). Les entités sont identifiées par le préfixe "E" et les propriétés par le préfixe "P". L'ontologie principale du CIDOC CRM se compose d'environ quatre-vingts entités qui peuvent être reliées par environ deux cents propriétés.

Les entités et les propriétés sont organisées en hiérarchie, chaque sous-entité ou sous-propriété étant plus spécifique que sa superentité ou sa super-propriété. Le diagramme suivant décrit une œuvre d'art dans l'ontologie événementielle du CIDOC CRM. L'ontologie place l'événement de la production de l'œuvre d'art au centre (E12). E22 et E57 sont des sous-entités de E12. P45 est une sous-propriété de P106.

![alt="The production of a piece of art, for example, has properties such as has produced a human-made object and subproperties such as consists of the materials paper and ink"](/img/documentation/ontologies-classes-and-properties-hierarchy-(c-LINCS).jpg)

En tant qu'ontologie centrée sur les événements, dans CIDOC CRM, toutes les connaissances proviennent des événements. Voici un diagramme de haut niveau de l'ontologie.

<ZoomableImage path="/img/documentation/ontologies-CIDOC-CRM-(c-LINCS).jpg" title="" altlabel="Simplified overview of the CIDOC CRM ontology shows that everything starts with an event" caption=""/>

Ce diagramme décrit ce qui suit :

* Les acteurs (E39 Actor) peuvent participer à des événements (E2 Temporal Entity).
* Les événements se produisent à un moment (E52 Time-Span) et à un endroit (E53 Place).
* Les événements peuvent affecter les choses.
* Les choses peuvent être physiques (E24 Chose physique faite par l'homme) ou conceptuelles (E28 Objet conceptuel).
* Les choses peuvent avoir un emplacement (E53 Place).
* Les choses peuvent être classées dans de nombreux groupes différents (E55 Type).
* Les objets peuvent avoir un nombre quelconque d'identifiants (E41 Appellation).

L'utilisation d'une ontologie telle que CIDOC CRM pour organiser vos données les rend plus compatibles avec d'autres données. La compatibilité rend vos données plus significatives, réutilisables et durables.

## Extensions

Les extensions de CIDOC CRM permettent à l'ontologie de couvrir de nombreux domaines de connaissance plus spécifiques, si nécessaire. L'adaptabilité fournie par les extensions est utile pour LINCS, car les ensembles de données LINCS, bien qu'appartenant tous au patrimoine culturel, proviennent d'un large éventail de disciplines :

* [CRMarchaeo](https://cidoc-crm.org/crmarchaeo/): Fouilles archéologiques
* [CRMba](https://cidoc-crm.org/crmba/): Bâtiments archéologiques
* [CRMdig](https://cidoc-crm.org/crmdig/): Informations sur la provenance
* [CRMgeo](https://cidoc-crm.org/crmgeo/): Propriétés spatio-temporelles
* [CRMinf](https://cidoc-crm.org/crminf/): Argumentation
* [CRMsci](https://cidoc-crm.org/crmsci/): Observation scientifique
* [CRMsoc](https://cidoc-crm.org/crmsoc/): Phénomènes sociaux
* [CRMtex](https://cidoc-crm.org/crmtex/): Textes anciens
* [FRBRoo/LRMoo](https://cidoc-crm.org/frbroo/): Informations bibliographiques
* [PRESSoo](https://cidoc-crm.org/pressoo/): Revues et périodiques
* [DoReMus](https://www.doremus.org/): Musique

## Modèle d'annotation web (WADM)

LINCS utilise parfois le :Term[Web Annotation Data Model (WADM)]{#web-annotation-data-model} en conjonction avec CIDOC CRM. Le WADM est une norme de formatage et de structuration des :Term[annotations web]{#web-annotation}. Il a été conçu pour rendre les descriptions d'annotations web cohérentes et donc plus faciles à partager et à réutiliser dans différentes sources. LINCS utilise WADM pour décrire les sources utilisées dans les ensembles de données en cours de conversion.

Le WADM n'est pas une extension du CIDOC CRM et n'est pas non plus centré sur les événements. Bien qu'il n'y ait pas d'alignement préexistant entre le WADM et le CIDOC CRM, les ontologues travaillent à faire fonctionner ensemble le WADM et le CIDOC CRM. Par exemple, au LINCS, les annotations sont classées comme crm:E33_Linguistic_Object dans CIDOC CRM.

## Langage d'ontologie web (OWL)

Au LINCS, nous utilisons le :Term[Langage OWL (OWL)]{#web-ontology-language} pour exprimer nos ontologies afin qu'elles puissent être comprises par les ordinateurs. OWL est comme un langage (ou mieux, une grammaire) pour les ontologies. Il est utilisé pour représenter des relations complexes entre des catégories de choses et leurs propriétés.

Dans le diagramme suivant, OWL peut être utilisé pour exprimer :

* "P1 est identifié par" est une propriété.
* Le domaine de "P1 identifié par" est "E21 Personne".
* L'étendue de "P1 est identifié par" est "E41 Appellation".
* La relation opposée (inversée) de "P1 est identifié par" est "P1i identifie".
* Le libellé anglais de cette propriété est "is identified by".

![alt="The domain Person is identified by the range Appellation"](/img/documentation/ontologies-OWL-(c-LINCS).jpg)

Voici à quoi ressemble l'encodage OWL :

<owl:ObjectProperty rdf:about="http://www.cidoc-crm.org/cidoc-crm/P1_is_identified_by">
    <rdfs:domain rdf:resource="http://www.cidoc-crm.org/cidoc-crm/E21_CRM_Entity"/>
    <rdfs:range rdf:resource="http://www.cidoc-crm.org/cidoc-crm/E41_Appellation"/>
    <owl:inverseOf rdf:resource="http://www.cidoc-crm.org/cidoc-crm/P1i_identifies"/>
    <rdfs:label xml:lang="en">est identifié par</rdfs:label>
</owl:ObjectProperty>

## Résumé

* Dans les sciences de l'information, les ontologies encodent un modèle conceptuel d'une manière lisible par une machine.
* Toutes les ontologies présentent une vision du monde.
* La mise en correspondance forme les données dans une structure qui permet une représentation cohérente de toutes les choses et des relations entre elles.
* La plupart des ontologies sont axées sur les objets ou sur les événements.
* CIDOC CRM est une ontologie événementielle conçue pour modéliser les données du patrimoine culturel.
* Les extensions de CIDOC CRM lui permettent de couvrir de nombreux autres domaines de connaissance.
* Web Annotation Data Model (WADM) est une norme pour le formatage et la structuration des annotations web.
* Le langage OWL (Web Ontology Language - OWL) est une grammaire utilisée pour exprimer des ontologies.

## Ressources

* Bruseker (2019) [“Learning Ontology & CIDOC CRM”](https://www.archesproject.org/wp-content/uploads/2020/04/Learning-Ontology-CRM.pdf)
* Bruseker & Guillem (2021) [“CIDOC CRM Game”](http://www.cidoc-crm-game.org/)
* Bruseker (2022) [“Formal Ontologies: A Complete Novice’s Guide"](https://training.parthenos-project.eu/sample-page/formal-ontologies-a-complete-novices-guide/)
* CIDOC CRM (2022) [“Compatible Models & Collaborations”](https://cidoc-crm.org/collaboration_resources)
* CIDOC CRM (2022) [“What is the CIDOC CRM?”](https://cidoc-crm.org/)
* Guarino, Oberle, & Staab (2009) [“What Is an Ontology?”](https://iaoa.org/isc2012/docs/Guarino2009_What_is_an_Ontology.pdf)
* Noy & McGuinness (2001) [“Ontology Development 101: A Guide to Creating Your First Ontology”](https://protege.stanford.edu/publications/ontology_development/ontology101-noy-mcguinness.html)
