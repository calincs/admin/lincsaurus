---
id: wikimedia-foundation
title: Fondation Wikimédia
definition: L’organisation faîtière qui gère Wikipedia, Wikibase, Media Wiki, Wiktionary et d’autres projets et chapitres Wiki.
---

La Wikimedia Foundation, souvent appelée Wikimedia, est l’organisation faîtière qui gère Wikipédia, :Term[Wikibase]{#wikibase}, Media Wiki, Wiktionary et autres projets et chapitres Wiki.

## Exemples

- [Wiktionary](https://www.wiktionary.org)
- [Wikisource](https://wikisource.org/wiki/Main_Page)
- [Wikispecies](https://species.wikimedia.org/wiki/Main_Page)

## Autres ressources

- [Wikimedia](https://www.wikimedia.org)
