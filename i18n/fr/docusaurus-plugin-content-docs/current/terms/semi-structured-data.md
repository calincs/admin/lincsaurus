---
id: semi-structured-data
title: Données semi-structurées
definition: Données présentant une certaine structure, mais pas d’une manière permettant d’extraire facilement des entités et des relations sans travail manuel.
Machine Translation: True     
Translation Tool: DeepL 
Translated Added By: Humna Chaudhry(@humnq)
Last Translated: 2024-06-21
---

:::info
Cette page web a été traduite automatiquement par DeepL. Bien que nous nous efforcions d’être précis, nous vous informons que les traductions peuvent contenir des erreurs ou des inexactitudes. Pour obtenir les informations les plus précises, veuillez vous référer à la version originale.
:::

Les données semi-structurées sont généralement des documents :Term[XML]{#xml} présentant une certaine structure, mais pas de manière à faciliter l'extraction des :Term[entités]{#entity} et des relations sans travail manuel. Par exemple, un document XML contenant un texte en langage naturel fortement annoté avec des balises XML peut être considéré comme une donnée semi-structurée. Ces balises peuvent identifier certaines entités et certaines relations entre entités qui pourraient être transformées en :Term[Linked Open Data (LOD)]{#linked-open-data} à l'aide d'une combinaison de scripts personnalisés, d'annotations manuelles supplémentaires et de vérifications.

## Exemples

- Extrait simplifié des données du [Projet Orlando](https://orlando.cambridge.org/), dont le processus de conversion a débuté sous la forme de documents XML annotés à la main.

```xml
<DATE>By March 1643</DATE>, early in this year of fierce <TOPIC>Civil War</TOPIC> fighting <NAME>Dorothy Osborne</NAME>'s mother moved with her children from <PLACE>Chicksands</PLACE> to the fortified port of <PLACE>St Malo</PLACE>.
```
