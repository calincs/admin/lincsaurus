---
sidebar_position: 4
title: "User Experience Credits"
description: "Credits for LINCS’s User Experience (UX) and user testing"
sidebar_class_name: "hide"
---

Credits are provided for the User Experience (UX) design and user testing of LINCS’s tools and materials. For more information on how to construct citations for LINCS, see [Cite](/docs/about-lincs/cite). To sign up to be a UX tester for LINCS, see [Participate in UX Testing](/docs/about-lincs/get-involved/participate).

## UX Testing and Design

- Kim Martin (UX Lead)
- Robin Bergart
- Nem Brunell
- Jordan Lum
- Farhad Omarzad
- Evan Rees

## UX Testers

- Sandra Ansic
- Timothy Arthur
- John Aspler
- Sehrish Basir
- Jon Bath
- Reg Beatty
- Francisco Berrizbeitia
- Carrie Breton
- Jason Camlot
- Grace Campagna
- Humna Chaudhry
- Chiara Cremona
- Alice Defours
- Meg Ecclestone
- Jana Smith Elford
- Iman El Gamal
- Kailey Fallis
- Michael Frischkopf
- Amelia Flynn
- Marion Tempest Grant
- Laura Hanikenne
- Janelle Jenstad
- Diane Jakacki
- Wayne Johnston
- Tim Knight
- Lorraine Janzen Kooistra
- Matthew Kunkel
- Jingyi Long
- Filomena Lopedoto
- Jasmin Macarios
- Jenny Marvin
- Juliene McLaughlin
- Michelle Meagher
- Matthew Milner
- Antonio Munoz Gomez
- Terhi Nurmikko-Fuller
- Jaehoon Pyon
- Ibrahim Rather
- Moni Razavi
- Sarah Roger
- Sarah Simpkin
- Thomas Smith
- Esther Sole
- Elena Spadini
- Peter Stephen
- Kirisan Suthanthireswaran
- Adele Torrance
- Mathilde Verstraete
- Marcello Vitali-Rosati
- Kristy von Moos
- Bri Watson
- Caroline Winter
