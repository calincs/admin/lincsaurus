import React from 'react';
import styles from "./cards.module.css";
import DownloadButton from '../buttons/DownloadButton';

function LogoCard({ src, alt, color }) {
    if (color == "light") {
        return (
            <div className={styles.logoCard}>
                <img src={src} alt={alt} />
                <DownloadButton url={src} />
            </div>
        );
    }

    if (src == "/img/logos/lincs-logos/LINCS-French-dark-(c-LINCS).png") {
        return (
            <div className={styles.frenchLogoCard}>
                <img src={src} alt={alt} />
                <DownloadButton url={src} />
            </div>
        );
    }

    if (src == "/img/logos/lincs-logos/LINCS-French-light-(c-LINCS).png") {
        return (
            <div className={styles.logoCard}>
                <img src={src} alt={alt} className={styles.frenchLogoCard} />
                <DownloadButton url={src} />
            </div>
        );
    }

    return (
        <div>
            <img src={src} alt={alt} />
            <DownloadButton url={src} />
        </div>
    );

}

export default LogoCard;