import React from 'react';
import Translate from '@docusaurus/Translate';
import styles from "./buttons.module.css";

function DownloadButton({ url }) {

    function handleClick() {
        let a = document.createElement("a");
        a.href = url;
        a.setAttribute("download", url);
        a.click();
    }

    return (
        <div className="primary-button-row">
            <button onClick={handleClick} className={styles.downloadButton}><Translate id="logoCard.downloadButtonLabel" description="Logo Card Download Button Label">Download</Translate></button>
        </div>
    );
}

export default DownloadButton;