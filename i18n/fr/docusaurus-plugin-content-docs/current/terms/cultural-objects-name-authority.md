---
id: cultural-objects-name-authority
title: Cultural Objects Name Authority (CONA)
definition: L’un des cinq vocabulaires Getty contenant des identifiants de ressources uniformes (URI) pour les titres, les attributions des créateurs, les caractéristiques physiques et les sujets représentés concernant les œuvres d’art, l’architecture et le patrimoine culturel visuel.
Machine Translation: True     
Translation Tool: DeepL 
Translated Added By: Humna Chaudhry(@humnq)
Last Translated: 2024-06-21
---

:::info
Cette page web a été traduite automatiquement par DeepL. Bien que nous nous efforcions d’être précis, nous vous informons que les traductions peuvent contenir des erreurs ou des inexactitudes. Pour obtenir les informations les plus précises, veuillez vous référer à la version originale.
:::

La Cultural Objects Name Authority (CONA) est l'un des cinq vocabulaires Getty développés par le Getty Research Institute pour fournir des termes et des :Term[Uniform Resource Identifiers (URIs)]{#uniform-resource-identifier} pour les concepts et les objets liés aux arts. LINCS utilise CONA pour fournir des URI pour les titres, les attributions aux créateurs, les caractéristiques physiques et les sujets représentés concernant les œuvres d'art, l'architecture et le patrimoine culturel visuel.

CONA comprend des termes relatifs à :

- l'architecture
- Artefacts
- Vannerie
- Céramique
- Dessins
- Fresques
- Objets fonctionnels ou cérémoniels
- Manuscrits
- Peinture
- Arts de la scène
- Photographies
- Gravures
- Sculpture
- Textiles

CONA exclut les termes relatifs aux films, aux œuvres littéraires, aux œuvres musicales et aux objets des collections scientifiques et d'histoire naturelle.

## Autres ressources

- [Cultural Objects Name Authority (CONA)](https://www.getty.edu/research/tools/vocabularies/cona/index.html)
- J. Paul Getty Trust (2022) ["About CONA and IA"](https://www.getty.edu/research/tools/vocabularies/cona/about.html)
