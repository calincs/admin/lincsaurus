---
slug: writing-in-circles
title: "Écrire en cercle: Du débutant à l'expert et inversement"
authors: Sam Peacock
tags: [digital humanities, research]
hide_table_of_contents: true
image: ./writing-in-circles-unsplash-(cc0).png
Machine Translation: True
Translation Tool: DeepL
Translated Added By: Humna Chaudhry(@humnq)
Last Translated: 2024-06-15
---

![](<./writing-in-circles-unsplash-(cc0).png>)

:::info

Cette page web a été traduite automatiquement par DeepL. Bien que nous nous efforcions d’être précis, nous vous informons que les traductions peuvent contenir des erreurs ou des inexactitudes. Pour obtenir les informations les plus précises, veuillez vous référer à la version originale.

:::

En tant qu'étudiante en anglais, on m'a toujours dit de rester concise dans mes écrits. C'était souvent facile, car je pouvais supposer que la personne qui lirait mon travail serait un spécialiste de l'anglais, et que ses attentes à l'égard de mes écrits et des connaissances qu'elle y apporterait se situeraient donc dans une fourchette très précise. De cette manière, je me suis entraînée à adopter par défaut un ton et un niveau de complexité académiques et à produire des écrits comparatifs et analytiques.

Cet été, cependant, en tant que membre de l'équipe de documentation LINCS, j'ai travaillé sur un type d'écriture totalement différent : des guides d'utilisation et des manuels d'instruction pour les nombreux outils logiciels développés ou utilisés par le projet. Pour commencer à rédiger de la documentation, j'ai d'abord dû réapprendre à écrire...{/* truncate */}

La rédaction d'instructions commence par la simplicité. Je pensais que puisque j'étais un écrivain concis, je serais capable d'écrire simplement et clairement, mais lorsque la première version des instructions étape par étape pour [maintenir le site web de documentation LINCS](https://gitlab.com/calincs/admin/lincsaurus/-/wikis/maintenance-guide) a été éditée, j'ai vu que mon nombre de mots avait été réduit de près de la moitié. En parcourant les pages de mon texte, j'ai constaté que l'intention avait été entièrement inversée - il ne s'agissait pas simplement d'intervertir des synonymes, mais d'un remaniement complet de la voix et du point de vue.

Pour expliquer le processus de création d'une demande de fusion, par exemple, j'ai d'abord écrit ce paragraphe verbeux et personnel :

_Pour travailler sur un problème, vous devez créer une demande de fusion. Cela créera une nouvelle "branche" sur notre dépôt GitLab, qui vous permettra d'effectuer et de suivre les modifications sans affecter la branche principale du dépôt (et, par conséquent, le site web lui-même). Plus tard, lorsque vous serez satisfait de vos modifications, elles pourront être ajoutées à la branche principale afin d'être publiées sur le site web de documentation._

En travaillant avec l'équipe de documentation pour réviser ma première tentative, nous avons commencé par remanier mon projet pour lui donner un ton plus actif. Nous avons supprimé les déclarations excessives telles que "faire et suivre" et "à son tour, le site web lui-même", ce qui a permis de raccourcir l'ensemble de la section sans compromettre la qualité de l'information :

_Créer une demande de fusion pour travailler sur un problème. Cette demande créera une nouvelle branche dans GitLab, ce qui vous permettra d'apporter des modifications sans affecter le dépôt principal. Lorsque vos modifications sont complètes et vérifiées, elles peuvent être ajoutées à la branche principale pour être publiées sur le site web de la documentation._

Nous avons élaboré un [guide de style](https://gitlab.com/calincs/admin/lincsaurus/-/wikis/style-guide) pour nous aider à orienter nos choix, et bien qu'il m'ait permis de rester dans les clous la plupart du temps, j'ai toujours eu du mal à réprimer l'étudiant anglais auteur d'essais qui avait élu domicile au plus profond de mon cerveau. D'une certaine manière, je trouvais la rédaction d'instructions bien plus difficile que celle d'une dissertation. Je ne pouvais plus dissimuler un manque de vision claire ou des lacunes de compréhension derrière des mots qui sonnaient bien. S'il n'y avait pas de sens clair, mon écriture tombait à l'eau.

Lorsque j'ai commencé à diviser les instructions en sections et les sections en étapes individuelles, j'ai vu qu'il était plus facile d'écrire simplement. Pourtant, mon instinct n'était pas encore tout à fait au point. Je cherchais encore des moyens de rendre l'écriture plus fluide entre les sections. J'imaginais un public qui me suivait d'étape en étape, parcourant les tâches dans le même ordre que moi : en commençant au même endroit et en terminant par le même objectif. Notre guide de style m'a toutefois rappelé qu'il fallait envisager l'écriture du point de vue du public. Comme moi, mon lecteur pouvait être un débutant qui ne connaissait pas l'utilisation d'une plateforme comme GitLab et qui avait besoin de connaître chaque étape de son parcours. Mais il peut également s'agir d'un utilisateur chevronné qui n'a oublié qu'une seule étape et qui a sauté directement à un sous-titre spécifique. Chaque petite section devait donc fonctionner comme un élément de documentation indépendant. Je ne pouvais pas supposer que mon lecteur avait lu toutes les sections précédentes. En adhérant à ce principe, j'ai dû parfois enfreindre l'une de mes règles d'or personnelles en matière d'écriture : éviter les répétitions ! Ce processus - bien qu'il ait donné lieu à des écrits qui répétaient des informations figurant dans les sections précédentes - a contribué à rendre ces outils utilisables par le plus grand nombre possible de personnes.

Cette répétition a également mis en évidence la diversité des lecteurs pour lesquels nous devions écrire. Dès les premiers jours de la planification de notre stratégie de documentation, nous avons cherché à identifier le public imaginaire pour lequel nous adaptions la complexité et la technicité de chaque document. En tant que domaine, :Term[Digital Humanities (DH)]{#digital-humanities} accueille la participation de personnes ayant un large éventail d'antécédents universitaires et de niveaux d'expertise technique. Nous avons compris qu'il nous faudrait répondre aux besoins de tous, des étudiants en arts de premier cycle aux développeurs de logiciels expérimentés, ce qui signifie que nous supposions un niveau de compétence technique de base, mais pas plus.

En ce qui concerne mon processus d'écriture, essayer de répondre aux besoins d'un éventail aussi diversifié d'utilisateurs signifiait adopter une autre approche circulaire. J'ai découvert les outils que je documentais en tant que débutant et, en me familiarisant avec eux pour apprendre ce qui devait être documenté, je suis devenu un expert. Ensuite, j'ai dû revenir au rôle de débutant (au début du cercle), en écrivant pour quelqu'un qui ne connaissait pas l'outil. J'ai dû apprendre à utiliser des outils que je ne connaissais pas du tout jusqu'à être capable d'en écrire les instructions, puis reprendre immédiatement la position de mon ancien moi, qui ne connaissait pas du tout les outils.

Grâce à mon expérience de la rédaction de documentation, je ne suis plus seulement attaché à la concision et à la simplicité, mais j'ai une approche entièrement différente de l'écriture. Rédiger de la documentation m'a poussé à prendre en compte le point de vue du public et à ne pas avoir peur d'enfreindre certaines de mes propres règles d'écriture. Le fait d'écrire en cercle m'a appris à rédiger des textes plus accessibles pour mes lecteurs.
