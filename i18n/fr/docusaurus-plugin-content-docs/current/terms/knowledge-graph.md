---
id: knowledge-graph
title: Graphique des connaissances
definition: Représentation d’un ensemble de triplets liés qui illustre les relations entre eux.
---

Un graphe de connaissances représente un ensemble fini de :Term[triples]{#triple} et illustre les relations entre eux. Les informations sont stockées dans une :Term[base de données de graphes]{#graph-database} et visualisée sous forme de structure de graphe. Le :Term[les entités]{#entity} et les relations qui forment les triplets liés doivent être créées à l’aide d’une sémantique formelle, contribuer les unes aux autres et représenter diverses données connectées et décrites par des métadonnées sémantiques. Un graphe de connaissances est similaire dans sa structure à un :Term[property-graph]{#property-graph}, mais il a des schémas formalisés et d’autres structures plus sophistiquées qui étendent son utilité sur différentes plates-formes et avec divers ensembles de données.

## Exemples

- WC3 (2014) *[RDF 1.1 Primer](https://www.w3.org/TR/rdf11-primer/)* : le schéma suivant décrit un graphe informel de triplets simples.

![Graphique de connaissances montrant la relation d’un exemple de personne avec La Joconde et les métadonnées associées en triplets.](</img/documentation/glossary-knowledge-graph-example-(fair-dealing).png>)

_Autorisé avec [licence W3C](https://www.w3.org/Consortium/Legal/2015/doc-license)._

## Autres ressources

- British Museum (2021) [“ResearchSpace: What is a Knowledge Graph?”](https://researchspace.org/knowledge-graph-and-patterns/)
- [Knowledge Graph (Wikipedia)](https://en.wikipedia.org/wiki/Knowledge_graph)
- Oldman & Tanase (2018) [“Reshaping the Knowledge Graph by Connecting Researchers, Data and Practices in ResearchSpace”](https://link.springer.com/content/pdf/10.1007/978-3-030-00668-6.pdf)
- Ontotext (2022) [“What is a Knowledge Graph?”](https://www.ontotext.com/knowledgehub/fundamentals/what-is-a-knowledge-graph/)
- WC3 (2014) _[RDF 1.1 Primer](https://www.w3.org/TR/rdf11-primer/)_
