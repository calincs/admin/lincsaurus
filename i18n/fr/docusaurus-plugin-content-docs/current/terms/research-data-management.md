---
id: research-data-management
title: Gestion des données de recherche (RDM)
definition: Processus et activités exécutés par les chercheurs tout au long du cycle de vie d’un projet de recherche pour guider la collecte, l’organisation, la documentation, le stockage, l’accessibilité, la réutilisabilité et la préservation des données.
Machine Translation: True     
Translation Tool: DeepL 
Translated Added By: Humna Chaudhry(@humnq)
Last Translated: 2024-06-21
---

:::info
Cette page web a été traduite automatiquement par DeepL. Bien que nous nous efforcions d’être précis, nous vous informons que les traductions peuvent contenir des erreurs ou des inexactitudes. Pour obtenir les informations les plus précises, veuillez vous référer à la version originale.
:::

La gestion des données de recherche (GDR) désigne les processus et les activités réalisés par les chercheurs tout au long du cycle de vie d'un projet de recherche pour guider la collecte, l'organisation, la documentation, le stockage, l'accès, la réutilisation et la préservation des données. Une bonne gestion des données de recherche peut aider à éviter la reproduction des recherches, à promouvoir la sécurité des données, à améliorer la comparaison et la co-analyse des données provenant de différentes sources, à garantir la réutilisation des ensembles de données et à contribuer à l'intégrité de la recherche.

## Autres ressources

- American Library Association (2023) [“Keeping Up With... Research Data Management”](https://www.ala.org/acrl/publications/keeping_up_with/rdm)
- Digital Research Alliance of Canada (2023) [“Research Data Management”](https://alliancecan.ca/en/services/research-data-management)
- Goodchild (2020, June 16). [“Data Curation Tool Scholars Portal Dataverse”](https://alliancecan.ca/sites/default/files/2022-05/Webinar_RDMTech_DataCuration.pdf) [PowerPoint]
- Government of Canada (2021, March 15) [“Tri-Agency Research Data Management Policy”](https://science.gc.ca/site/science/en/interagency-research-funding/policies-and-guidelines/research-data-management/tri-agency-research-data-management-policy)
- Scholars Portal (2019) [“RDM 101 – Module 4. Scholars Portal”](https://learn.scholarsportal.info/modules/portage/rdm-101-module-4/) [PowerPoint]
