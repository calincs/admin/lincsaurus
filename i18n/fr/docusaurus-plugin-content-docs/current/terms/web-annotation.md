---
id: web-annotation
title: Annotation Web
definition: Annotation en ligne d’une ressource Web qui indique une connexion entre différentes ressources.
Machine Translation: True     
Translation Tool: DeepL 
Translated Added By: Humna Chaudhry(@humnq)
Last Translated: 2024-06-21
---

:::info
Cette page web a été traduite automatiquement par DeepL. Bien que nous nous efforcions d’être précis, nous vous informons que les traductions peuvent contenir des erreurs ou des inexactitudes. Pour obtenir les informations les plus précises, veuillez vous référer à la version originale.
:::

Une annotation web est une annotation en ligne d'une ressource web, généralement sous la forme d'un commentaire ou d'une étiquette, qui indique une connexion entre différentes ressources. Les annotations web sont souvent conçues comme une couche qui existe au-dessus d'une ressource existante. Si un utilisateur utilise un système d'annotation spécifique pour créer ses annotations, les autres utilisateurs utilisant le même système d'annotation peuvent parfois voir les annotations de l'autre utilisateur.

Il existe différentes normes pour le formatage des annotations web, telles que le :Term[Web Annotation Data Model (WADM)]{#web-annotation-data-model}.

## Autres ressources

- [Web Annotation (Wikipedia)](https://en.wikipedia.org/wiki/Web_annotation)