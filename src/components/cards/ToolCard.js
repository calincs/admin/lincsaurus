/** @format */

import React from "react";
import Link from "@docusaurus/Link";
import styles from "./cards.module.css";
import functionTypes from "../functions";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { library } from "@fortawesome/fontawesome-svg-core";
import {
  faMagnifyingGlass,
  faDiagramProject,
  faFileLines,
  faBroom,
  faLink,
  faArrowsRotate,
  faEye,
} from "@fortawesome/free-solid-svg-icons";
import { ExpertiseButtons } from "../lists/ToolButtonList";
import { FunctionButtons } from "../lists/ToolButtonList";
import { CreatorButtons } from "../lists/ToolButtonList";

library.add(
  faMagnifyingGlass,
  faDiagramProject,
  faFileLines,
  faBroom,
  faLink,
  faArrowsRotate,
  faEye,
);


function ToolCard({ tool, image, description, path, functions, creator, lvl }) {
  return (
    <Link to={path} style={{ textDecoration: "none" }}>
      <div className={styles["tool-card"]}>
        <div className={styles["title"]}>
          <h3>{tool}</h3>
        </div>
        <ul className={styles["tool-card-buttons-extra"]}>
          <ExpertiseButtons
            levels={lvl}
            className={"tool-card-button-expertise"}
          />
        </ul>
        <div className={styles["image"]}>
          {image ? <img alt={"Logo of " + tool} src={"/img/" + image} /> : null}
        </div>

        <div className={styles["body"]}>
          <p>{description}</p>
        </div>

        <div className={styles["footer"]}>
          <ul>
            <FunctionButtons
              functions={functions}
              className={"tool-card-button-function"}
            />
          </ul>
        </div>
      </div>
    </Link>
  );
}

export default ToolCard;
