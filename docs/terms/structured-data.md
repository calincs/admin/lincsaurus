---
id: structured-data
title: Structured Data
definition: Data that takes the form of spreadsheets, relational databases, JSON files, RDF files, and XML files.
Last Translated: 2024-06-21
---

Structured Data is data that takes the form of spreadsheets (e.g., CSV, TSV, XSL, XSLX), :Term[relational databases]{#relational-database}, JSON files, :Term[RDF]{#resource-description-framework} files, and :Term[XML]{#xml} files.

At LINCS, we count data as structured if the :Term[entities]{#entity} are all tagged individually (e.g., one entity per spreadsheet cell or XML element) and the entities are connected, either in a hierarchical way (e.g., nested XML elements) or with relationships between entities expressed following some clearly-defined schema and data structure (e.g., spreadsheet headings relating columns of entities together).

## Examples

- The [Canadian Centre for Ethnomusicology](https://www.ualberta.ca/museums/museum-collections/canadian-centre-for-ethnomusicology.html) data started as several spreadsheets with a row for each artifact

| ID            | Title               | placeMade | placeMadeID                      | material | materialID                               |
| ------------- | ------------------- | --------- | -------------------------------- | -------- | ---------------------------------------- |
| CCEA-L1995.63 | Bamboo Flute        | Edmonton  | https://sws.geonames.org/5946768 | bamboo   | http://www.wikidata.org/entity/Q27891820 |
| CCEA1995.21   | Pair of Taiko Drums | Shinano   | https://sws.geonames.org/1852136 | hide     | http://www.wikidata.org/entity/Q3291230  |

- The [University of Saskatchewan Art Collection](https://saskcollections.org/) data began as an XML file with a parent element for each art object

```xml
<?xml version="1.0" ?>
<rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
    <rdf:Description>
        <ObjectIdentifier>1910.001.001</ObjectIdentifier>
        <AcquistionDate>1910</AcquistionDate>
        <ObjectTitle>Portrait of Thomas Copland</ObjectTitle>
        <ArtistName url="http://www.wikidata.org/entity/Q100921439">Victor Albert Long</ArtistName>
        <Medium url="http://vocab.getty.edu/aat/300015050">oil paint</Medium>
        <Category url="http://vocab.getty.edu/aat/300033618">painting</Category>
    </rdf:Description>
    <rdf:Description>
        <ObjectIdentifier>2018.026.001</ObjectIdentifier>
        <AcquistionDate>2018</AcquistionDate>
        <ObjectTitle>Grace</ObjectTitle>
        <ArtistName url="http://www.wikidata.org/entity/Q19609740">Lori Blondeau</ArtistName>
        <Medium url="http://vocab.getty.edu/aat/300265621">inkjet print</Medium>
        <Category url="http://vocab.getty.edu/aat/300046300">Photograph</Category>
    </rdf:Description>
</rdf:RDF>
```
