---
id: vocabulary
title: Vocabulaire
definition: Ensemble de termes pouvant être concrètement décrits dans une ontologie, une taxonomie ou un thésaurus.
---

Un vocabulaire est un ensemble ou une collection de termes. C’est un terme générique pour désigner des ensembles de concepts qui pourraient être concrètement décrits dans un :Term[ontologie]{#ontology}, :Term[taxonomie]{#taxonomy}, ou :Term[thésaurus]{#thesaurus}.

|                | Ontologie                                                                                            | Taxonomie                                                                                        | Thésaurus                                                   | Vocabulaire                                                              |
| -------------- | ---------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------ | ----------------------------------------------------------- | ------------------------------------------------------------------------ |
| **Décrit**     | Concepts (contenu) et les relations entre eux (structure), y compris les axiomes et les restrictions | Relations hiérarchiques entre les concepts, et spécifie le terme à utiliser pour désigner chacun | Relations hiérarchiques et non hiérarchiques entre concepts | Terme général désignant un ensemble de concepts (mots) liés à un domaine |
| **Relations**  | Hiérarchique typé et associatif                                                                      | Fondamentalement hiérarchique, mais tous modélisés en utilisant la même notation                 | Non typé hiérarchique, associatif et équivalence            | Non spécifié (concept abstrait)                                          |
| **Propriétés** | RDFS définit les propriétés et les restrictions des relations                                        | Aucun                                                                                            | Peut être décrit dans les notes d’application si nécessaire | Non spécifié (concept abstrait)                                          |
| **Structuré**  | Réseau                                                                                               | Arbre                                                                                            | Arbre à branches croisées                                   | Non spécifié (concept abstrait)                                          |

## Autres ressources

- [Vocabulary (Wikipedia)](https://en.wikipedia.org/wiki/Vocabulary)
