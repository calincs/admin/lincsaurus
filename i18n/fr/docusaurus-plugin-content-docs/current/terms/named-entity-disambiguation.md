---
id: named-entity-disambiguation
title: Désambiguïsation d’entité nommée (NED)
definition: Attribuer une identité unique à une entité dans un texte pour la différencier d’une autre entité qui partage le même nom.
---

La désambiguïsation d’entité nommée (NED) consiste à attribuer une identité unique à :Term[entités]{#entity} mentionnées dans le texte. Il est étroitement lié à :Term[Reconnaissance d’entité nommée (NER)]{#named-entity-recognition}, qui implique le processus d’identification et de catégorisation des entités mentionnées dans le texte, mais elles produisent des résultats différents. Alors que NER s’intéresse à la catégorie à laquelle appartient une entité (par exemple, Regina est une ville), la désambiguïsation détermine que l’instance de « Regina » dans le texte est en effet une référence à la capitale de la Saskatchewan. L’instance est liée à un :Term[enregistrement d’autorité]{#authority-record} pour cette entité et non pour une autre ville, la déesse romaine ou une personne nommée Regina.

## Autres ressources

- [Entity Linking (Wikipedia)](https://en.wikipedia.org/wiki/Entity_linking)
