---
title: "Learn LOD"
sidebar_class_name: "landing-page"
Last Translated: 2023-07-26
---

import DocCardList from '@theme/DocCardList';
import {useCurrentSidebarCategory} from '@docusaurus/theme-common';

LINCS develops infrastructure and tools so scholars and students of the humanities can create and access :Term[Linked Open Data (LOD)]{#linked-open-data}. The promise and potential of LOD is at the heart of LINCS.

While the term **data** is not commonly used to describe the scholarly output of humanities researchers, many humanities researchers produce data. Humanities data can take many forms, such as:

- Image files (e.g., pictures, paintings, photographs)
- Text files (e.g., poems, manuscripts, letters, newspapers, journals, short stories, social media posts)
- Maps and geospatial coordinates
- Audio recordings (e.g., music, oral histories)
- :Term[Metadata]{#metadata} files

These items can all be transformed into standard formats that both humans and computers can read. When data is made openly available on the web, scholars—and everyone!—can discover data, link to data, and advance humanities scholarship.

When research materials are transformed into LOD, anyone can:

- Discover unexpected connections and influences among creative works, people, places, events, and organizations  

> _What are the social connections among people and networks that created the artwork and literature in these fin-de-siècle magazines?_  

- Ask new questions by searching across multiple diverse datasets  

> _Who (demographic data) lived on these streets (location data) in early modern London?_

- Query the data using multiple filters  

> _Show me all women authors who were published by independent presses and whose fathers were members of the clergy._  

- Reveal relationships by viewing data graphically as a timeline, word cloud, network, or chart

<DocCardList items={useCurrentSidebarCategory().items.slice(0,4)}/>

## Learning Resources

LINCS has created and collected a variety of resources of different levels and formats to help users learn about :Term[Linked Open Data (LOD)]{#linked-open-data}.

### Zotero Library

The [LINCS Zotero Library](https://www.zotero.org/groups/5145265/lincs-project/library) contains citations and links to articles, books, conference presentations, videos, tutorials, and websites on LOD topics, ranging from introductory to advanced. The resources have been divided by topic. Tags have been used to denote the following:

- **Favourite:** Resources that are LINCS’s favourite
- **Introductory Material:** Resources that are ideal for beginners
- **Training Module:** Resources that are interactive
- **LINCS Output:** Resources that have been created by LINCS and/or the project’s collaborators

[Contact LINCS](/docs/about-lincs/get-involved/contact-us) to share resources you would like to add to the Zotero library or to request library editing privileges.

### YouTube

[LINCS’s YouTube](https://www.youtube.com/@lincsproject/featured) features video tutorials and conference presentations on LOD topics and [LINCS’s tools](/docs/tools). Subscribe to the channel to be notified of new content!

### Events

LINCS holds many events, including tutorials and conferences. See the [Events](/docs/about-lincs/get-involved/events) page to learn about past and upcoming events.
