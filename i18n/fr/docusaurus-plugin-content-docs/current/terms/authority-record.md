---
id: authority-record
title: Notice d’autorité
definition: Un identificateur de ressource uniforme (URI) stable et persistant pour un concept dans l’écosystème des données liées (LD).
---

Une notice d’autorité est une :Term[Uniform Resource Identifier (URI)]{#uniform-resource-identifier} pour un concept dans le Écosystème de :Term[données liées (LD)]{#linked-data}. En tant qu’ensemble de termes, les notices d’autorité représentent un type de :Term[vocabulaire contrôlé]{#controlled-vocabulary}.
