---
id: hypertext-markup-language
title: HyperText Markup Language (HTML)
definition: Le langage de balisage standard pour les pages Web.
Machine Translation: True     
Translation Tool: DeepL 
Translated Added By: Humna Chaudhry(@humnq)
Last Translated: 2024-06-21
---

:::info
Cette page web a été traduite automatiquement par DeepL. Bien que nous nous efforcions d’être précis, nous vous informons que les traductions peuvent contenir des erreurs ou des inexactitudes. Pour obtenir les informations les plus précises, veuillez vous référer à la version originale.
:::

Le langage de balisage hypertexte (HTML) est le langage de balisage standard pour les pages web. Il est utilisé pour décrire sémantiquement la structure des documents afin de permettre aux navigateurs de les transformer en contenu web multimédia.

Le langage HTML est souvent utilisé en conjonction avec les feuilles de style en cascade (CSS) (pour affecter la mise en page du contenu et des pages web) et JavaScript (pour affecter le comportement du contenu et des pages web).

## Autres ressources

- [HTML (Wikipedia)](https://en.wikipedia.org/wiki/HTML)
- Glass & Boucheron (2024) [“How To Build a Website with HTML”](https://www.digitalocean.com/community/tutorial-series/how-to-build-a-website-with-html) [Tutorial]
- MDN Web Docs (2024) [“HTML: HyperText Markup Language”](https://developer.mozilla.org/en-US/docs/Web/HTML)
- W3Schools (2014) [“HTML Responsive Web Design”](https://www.w3schools.com/html/html_responsive.asp)
- W3Schools (2024) [“HTML Tutorial”](https://www.w3schools.com/html/) [Tutorial]