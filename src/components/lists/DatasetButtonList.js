import React from "react";
// import styles from "./buttons.module.css";
// import Translate, { translate } from "@docusaurus/Translate";
import datasetData from "../datasets.js";
import PrimaryButton from "../buttons/PrimaryButton.js";

function DatasetButtonList({ datasetID }) {
  const dataset = datasetData[datasetID];

  const buttons = ["Website", "RS Search", "Application Profile", "Vocabulary", "Borealis"];
  dataset.buttons["RS Search"].label = "To ResearchSpace";

return (
    <div className="dataset-button-row">
        <ul className="padding--none">
            {buttons.map((button) => (
                dataset.buttons[button] && (
                    button !== "RS Search" || (button === "RS Search" && dataset.published)
                ) && (
                    <li key={button}>
                        <PrimaryButton
                            link={dataset.buttons[button].link}
                            buttonName={dataset.buttons[button].label}
                        />
                    </li>
                )
            ))}
        </ul>
    </div>
);
}

export default DatasetButtonList;