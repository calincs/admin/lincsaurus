/** @format */

import React, { useCallback, useRef, useState } from "react";
import LightGallery from "lightgallery/react";
import "lightgallery/css/lightgallery.css";
import "lightgallery/css/lg-zoom.css";
import "lightgallery/css/lg-thumbnail.css";

import lgThumbnail from "lightgallery/plugins/thumbnail";
import lgZoom from "lightgallery/plugins/zoom";
import lgFullScreen from "lightgallery/plugins/fullscreen";

import useDocusaurusContext from "@docusaurus/useDocusaurusContext";

function ZoomableImageGallery({ children }) {
  // Potentially could include copyright?
  const lightGallery = useRef(null);
  const [container, setContainer] = useState(null);

  const { siteConfig } = useDocusaurusContext();

  const onInit = useCallback((detail) => {
    if (detail) {
      lightGallery.current = detail.instance;
      lightGallery.current.openGallery();
    }
  }, []);

  const setContainerRef = useCallback((node) => {
    if (node !== null) {
      setContainer(node);
    }
  }, []);

  const LG = () => {
    return (
      <LightGallery
        speed={500}
        plugins={[lgZoom, lgThumbnail,lgFullScreen]}
        closable={false}
        actualSize={true}
        showZoomInOutIcons
        zoom
        showMaximizeIcon
        fullScreen={true}
        container={container}
        onInit={onInit}
        defaultCaptionHeight={80}
        licenseKey={siteConfig.customFields.LightGalleryLicense}
        mode={"lg-fade"}>
        {children}
      </LightGallery>
    );
  };
  return (
    <div id="lg-app" ref={setContainerRef}>
      {LG()}
    </div>
  );
}

export default ZoomableImageGallery;
