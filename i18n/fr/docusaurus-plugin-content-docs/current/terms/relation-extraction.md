---
id: relation-extraction
title: Extraction de relations (RE)
definition: La tâche de détecter, classer et extraire les relations sémantiques d’un texte.
Machine Translation: True     
Translation Tool: DeepL 
Translated Added By: Humna Chaudhry(@humnq)
Last Translated: 2024-06-21
---

:::info
Cette page web a été traduite automatiquement par DeepL. Bien que nous nous efforcions d’être précis, nous vous informons que les traductions peuvent contenir des erreurs ou des inexactitudes. Pour obtenir les informations les plus précises, veuillez vous référer à la version originale.
:::

L'extraction de relations (RE) consiste à détecter, classer et extraire les relations sémantiques d'un texte. Ces relations ont tendance à se produire entre deux ou plusieurs :Term[entités]{#entity} d'un certain type (par exemple, personne, organisation, lieu) et peuvent être dénotées à l'aide de :Term[triples]{#triple}. L'ER est l'une des techniques de :Term[Traitement du langage naturel (NLP)]{#natural-language-processing}.

## Autres ressources

- Deep Dive (2023) [“Relation Extraction”](http://deepdive.stanford.edu/relation_extraction)
- NLP Progress (2023). [“Relation Extraction”](https://nlpprogress.com/english/relationship_extraction.html)
- [Relation Extraction (Wikipedia)](https://en.wikipedia.org/wiki/Relationship_extraction)