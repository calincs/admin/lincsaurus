/** @format */

import React from "react";
import Datasets from "../datasets";
import DatasetCard from "../cards/DatasetCard";
import buttonStyles from "../buttons/buttons.module.css";
import GridLayout from "../layouts/GridLayout";
import styles from "./toolCatalogue.module.css";
import { useState, useEffect } from "react";
import Translate, { translate } from "@docusaurus/Translate";
import functionTypes from "../functions";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { library } from "@fortawesome/fontawesome-svg-core";
import {
  faMagnifyingGlass,
  faDiagramProject,
  faFileLines,
  faBroom,
  faLink,
  faArrowsRotate,
  faEye,
} from "@fortawesome/free-solid-svg-icons";

library.add(
  faMagnifyingGlass,
  faDiagramProject,
  faFileLines,
  faBroom,
  faLink,
  faArrowsRotate,
  faEye,
);

// get keywords from all datasets
const getKeywordsFromDatasets = () => {
  const keywords = [];
  Object.keys(Datasets).forEach((key) => {
    keywords.push(...Datasets[key].keywords);
  });
  return [...new Set(keywords)].sort();
};

const sortedKeywords = getKeywordsFromDatasets();

function DatasetFilters({ activeFilters, setActiveFilters }) {
  const handleClick = (filter) => {
    let _activeFilters = [...activeFilters];
    // Resetting filter
    if (activeFilters.includes(filter)) {
      _activeFilters = _activeFilters.filter((item) => item !== filter);
    } else {
      _activeFilters = _activeFilters.filter((item) => item !== "All");
      _activeFilters.push(filter);
    }

    //Resets and sets filters
    if (_activeFilters.length === 0) {
      setActiveFilters(["All"]);
    } else {
      setActiveFilters(_activeFilters);
    }
  };

  return (
    <div className={styles.filters}>
      <p className={styles["filter-label"]}>
        <Translate
          id="datasetCatalogue.filterByStatusHeader"
          description="Dataset Catalogue Filter by Status">
          Filter by Status
        </Translate>
      </p>
      <div className="function-button-row">
        <button
          key={"Published"}
          disabled={activeFilters.includes("Unpublished")} // Disable if "Unpublished" is in the filters
          aria-disabled={activeFilters.includes("Unpublished")} // Disable if "Unpublished" is in the filters
          className={`${
            activeFilters.includes("Published")
              ? [styles.filterButton, buttonStyles.functionButtonFilter].join(
                  " ",
                )
              : buttonStyles.functionButtonFilter
          } `}
          onClick={() => handleClick("Published")}>
          <div className={styles.functionButtonName}>Published</div>
        </button>
        <button
          key={"Unpublished"}
          disabled={activeFilters.includes("Published")} // Disable if "Published" is in the filters
          aria-disabled={activeFilters.includes("Published")} // Disable if "Published" is in the filters
          className={`${
            activeFilters.includes("Unpublished")
              ? [styles.filterButton, buttonStyles.functionButtonFilter].join(
                  " ",
                )
              : buttonStyles.functionButtonFilter
          } `}
          onClick={() => handleClick("Unpublished")}>
          <div className={styles.functionButtonName}>Unpublished</div>
        </button>
      </div>

      <p className={styles["filter-label"]}>
        <Translate
          id="datasetCatalogue.filterByKeywordHeader"
          description="Dataset Catalogue Filter by Keyword Header">
          Filter by Keyword
        </Translate>
      </p>
      <div className="function-button-row">
        {sortedKeywords.map((filter) => (
          <button
            key={filter}
            className={`${
              activeFilters.includes(filter)
                ? [styles.filterButton, buttonStyles.functionButtonFilter].join(
                    " ",
                  )
                : buttonStyles.functionButtonFilter
            } `}
            onClick={() => handleClick(filter)}>
            <div className={styles.functionButtonName}>{filter}</div>
          </button>
        ))}
      </div>
    </div>
  );
}

const DatasetList = ({ datasets, resetFilter, resetQuery, sortOrder }) => {
  if (datasets.length === 0) {
    return (
      <div className={styles["empty-list"]}>
        <p>
          <Translate
            id="datasetCatalogue.noDatasetsFoundLabel"
            description="Dataset Catalogue No Datasets Found label">
            No Datasets Found!
          </Translate>
        </p>
        <button
          className={buttonStyles.primaryButton}
          onClick={() => {
            resetFilter(["All"]);
            resetQuery("");
          }}>
          <Translate
            id="datasetCatalogue.resetFiltersLabel"
            description="Dataset Catalogue Reset Filters label">
            Reset Filters
          </Translate>
        </button>
      </div>
    );
  }

  if (sortOrder === "featured") {
    datasets = datasets.sort(
      (a, b) => a["Featured Order"] - b["Featured Order"],
    );
  } else if (sortOrder === "alphabetical") {
    datasets = datasets.sort((a, b) => a["title"].localeCompare(b["title"]));
  }

  return (
    <GridLayout maxColumns={2}>
      {datasets.map((x) => (
        <DatasetCard key={x["id"]} dataset={x} />
      ))}
    </GridLayout>
  );
};

const matchesFilter = (item, filt) =>
  item.status == filt || item.keywords.includes(filt);

const isPublished = (item, filt) =>
  (filt == "Unpublished" && item.published == false) ||
  (filt == "Published" && item.published == true);

const isFeatured = (item, filt) => filt == "featured" && item.published == true;

function DatasetCatalogue({ preFilter = "All", sortOrder = "alphabetical" }) {
  const [items, setItems] = useState([]);
  const [query, setQuery] = useState("");
  const [searchParam] = useState([
    "title",
    "longTitle",
    "description",
    "shortDescription",
    "keywords",
    "status",
    "id",
  ]);
  const [filters, setFilters] = useState(
    Array.isArray(preFilter) ? preFilter : [preFilter],
  );
  useEffect(() => {
    setItems(search(Datasets));
  }, []);

  const search = (items) => {
    const combinedItems = Object.keys(items).reduce((acc, key) => {
      return acc.concat(items[key]);
    }, []);

    return combinedItems.filter((item) => {
      if (
        filters.every(
          (filt) =>
            matchesFilter(item, filt) ||
            isPublished(item, filt) ||
            isFeatured(item, filt),
        )
      ) {
        // Looking for exact string match, may want to change this to a more forgivable regex
        return searchParam.some((newItem) => {
          return (
            item[newItem]
              .toString()
              .toLowerCase()
              .indexOf(query.toLowerCase()) > -1
          );
        });
      } else if (filters == "All") {
        return searchParam.some((newItem) => {
          return (
            item[newItem]
              .toString()
              .toLowerCase()
              .indexOf(query.toLowerCase()) > -1
          );
        });
      }
    });
  };

  // Allows list to be prefiltered to be used on other pages
  // REVIEW: is it desirable to have input field and filters visible
  return preFilter != "All" ? (
    <DatasetList datasets={search(items)} sortOrder={sortOrder} />
  ) : (
    <>
      <DatasetFilters
        setActiveFilters={setFilters}
        activeFilters={filters}></DatasetFilters>

      <input
        type="search"
        className={styles["tool-search"]}
        placeholder={translate({
          id: "datasetCatalogue.searchBarPlaceholder",
          description: "Dataset Catalogue Search Bar Placeholder",
          message: "Search by title or keyword",
        })}
        aria-label={translate({
          id: "datasetCatalogue.searchBarPlaceholder",
          description: "Dataset Catalogue Search Bar Placeholder",
          message: "Search by title or keyword",
        })}
        value={query}
        onChange={(e) => setQuery(e.target.value)}></input>
      <DatasetList
        datasets={search(items)}
        resetFilter={setFilters}
        resetQuery={setQuery}
        sortOrder={sortOrder}
      />
    </>
  );
}

export default DatasetCatalogue;
