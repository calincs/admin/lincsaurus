---
id: natural-language-processing
title: Traitement automatique des langues naturelles (TALN)
definition: Une branche de l’intelligence artificielle qui implique le traitement automatique et/ou la manipulation de la parole, du texte et d’autres formes de données non structurées qui représentent la façon dont les humains communiquent entre eux.
---

Le traitement automatique des langues naturelles est une branche de l’intelligence artificielle qui implique le traitement automatique et/ou la manipulation de la parole, du texte et d’autres formes de données non structurées qui représentent la façon dont les humains communiquent entre eux. Il décrit le travail nécessaire pour essayer d’amener les ordinateurs à comprendre notre langage naturel et nos modes de communication. La plupart des techniques de traitement automatique des langues naturelles reposent sur l’apprentissage automatique pour tirer un sens des communications humaines.

## Exemples

- [Google Translate](https://translate.google.ca/)
- Alexa, Siri et autres assistants personnels qui utilisent la reconnaissance vocale
- Chatbots

## Autres ressources

- Lopez Yse (2019) [“Your Guide to Natural Language Processing (NLP)”](https://towardsdatascience.com/your-guide-to-natural-language-processing-nlp-48ea2511f6e1)
- [Natural Language Processing (Wikipedia)](https://en.wikipedia.org/wiki/Natural_language_processing)
- SparkCognition (2018) [“The Basics of Natural Language Processing”](https://www.youtube.com/watch?v=d4gGtcobq8M) [Vidéo]
