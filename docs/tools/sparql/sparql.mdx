---
description: "Query data in the LINCS triplestore using SPARQL."
title: "SPARQL Endpoint"
Last Translated: 2023-05-15
image: /img/documentation/SPARQL-LINCS-logo-(c-LINCS).png
---


<ToolButtons toolName="SPARQL Endpoint"/>

The SPARQL Endpoint is an endpoint that allows you to query the data in the LINCS :Term[triplestore]{#triplestore}. The endpoint is capable of processing simple or complex :Term[SPARQL queries]{#sparql-protocol-and-rdf-query-language} to search for and retrieve data.

<div className="banner">
  <img src="/img/documentation/SPARQL-LINCS-logo-(c-LINCS).png" alt="" />
</div>

<div className="primary-button-row">
  <PrimaryButton
    link="/docs/tools/sparql/yasgui"
    buttonName="To the Tool"
    ></PrimaryButton>
  <PrimaryButton
    link="https://gitlab.com/calincs/infrastructure/yasgui/"
    buttonName="To GitLab"
    />
</div>

## SPARQL Endpoint and LINCS

At LINCS, researchers can use the SPARQL Endpoint to query data from their own project, from other projects within LINCS, and from other endpoints (e.g., [The Getty Research Institute](http://vocab.getty.edu/sparql)).

By querying data, you can ask specific research questions across multiple datasets, visualize and further explore data in [ResearchSpace](/docs/tools/researchspace), and export and then import data into other visualization tools. For more information about constructing queries, see our [SPARQL explanation](/docs/learn-lod/linked-open-data-basics/concepts-sparql).

## Prerequisites

- You not need to come with your own dataset.
- You do not need a user account.
- You will need a basic understanding of [SPARQL](/docs/learn-lod/linked-open-data-basics/concepts-sparql).

The SPARQL Endpoint supports the following inputs and outputs:

- **Input:** SPARQL query
- **Output:** Results of query as JSON or in a table which can be exported as a CSV

## Resources

To learn more about the SPARQL Endpoint, see the following resources:

**Introductions:**

- Blaney (2017) [“Introduction to the Principles of Linked Open Data”](https://programminghistorian.org/en/lessons/intro-to-linked-data)
- bobdc (2015) [“SPARQL in 11 Minutes”](https://www.youtube.com/watch?v=FvGndkpa4K0) [Video]
- Ontotext (2022) [“What is SPARQL?”](https://www.ontotext.com/knowledgehub/fundamentals/what-is-sparql/)
- Stardog Union (2022) [“Learn SPARQL”](https://docs.stardog.com/getting-started-series/getting-started-1#:~:text=The%20basic%20building%20block%20for,wildcards%20that%20match%20any%20node)
- W3C (2008) [“SPARQL By Example: The Cheat Sheet”](https://www.iro.umontreal.ca/~lapalme/ift6281/sparql-1_1-cheat-sheet.pdf) [PowerPoint]

**Other SPARQL Endpoints:**

- [CWRC SPARQL Endpoint](https://yasgui.lincsproject.ca/)
- [The Getty SPARQL Endpoint](http://vocab.getty.edu/queries)
- [DBPedia + Sparnatural Simplified Query Builder](https://sparnatural.eu/demos/demo-dbpedia-v2/index.html?lang=en)

Information about the team that developed the [SPARQL Endpoint](/docs/about-lincs/credits/tools-credits#sparql-endpoint) is available on the Tool Credits page.