#!/bin/sh

export CONFIG="$(cat ./config.json | jq -r tostring)"

docker run -it --add-host=host.docker.internal:host-gateway --env-file=$PWD/.env -e CONFIG typesense/docsearch-scraper
