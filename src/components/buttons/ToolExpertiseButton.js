import React from "react";
import styles from "./buttons.module.css";
import Translate, { translate } from "@docusaurus/Translate";

const message = {
  Beginner: (
    <Translate
      id="tool.expertise.beginner"
      description="Tool Expertise Beginner">
      Beginner
    </Translate>
  ),
  Intermediate: (
    <Translate
      id="tool.expertise.intermediate"
      description="Tool Expertise Intermediate">
      Intermediate
    </Translate>
  ),
  Expert: (
    <Translate id="tool.expertise.expert" description="Tool Expertise Expert">
      Expert
    </Translate>
  ),
};

function ExpertiseButton({ buttonName, className }) {
  const containerClass = className ? styles[className] : styles.expertiseButton;
  const nameClass = className
    ? styles[`${className}Name`]
    : styles.expertiseButtonName;

  return (
    <div className={containerClass}>
      <div className={nameClass}>{message[buttonName]}</div>
    </div>
  );
}

export default ExpertiseButton;
