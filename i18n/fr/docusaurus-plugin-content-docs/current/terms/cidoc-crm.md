---
id: cidoc-crm
title: CIDOC CRM
definition: Une suite d’ontologies centrées sur les événements pour décrire les données dans le domaine du patrimoine culturel, développées pour relier des ensembles hétérogènes de données gérés par des musées, des galeries et d’autres institutions patrimoniales.
---

CIDOC CRM est un :Term[ontologie centrée sur les événements]{#event-oriented-ontology} pour décrire les données dans le domaine du patrimoine culturel. Il s’agit du modèle conceptuel de référence développé par le [Conseil international des musées (ICOM)](https://icom.museum/en/). Le :Term[ontologie]{#ontology} a été développé dans le but de fournir un moyen de relier des ensembles hétérogènes de données de collections détaillant le patrimoine culturel matériel et immatériel géré par des musées, des galeries et d’autres institutions patrimoniales.

CIDOC CRM est depuis devenu une suite d’ontologies. CIDOC CRM, également appelé CRMbase ou CRMcore, est l’ontologie de base d’origine, et les modèles compatibles couvrent un certain nombre de domaines supplémentaires, notamment les données bibliographiques/de bibliothèque, l’archéologie, l’observation scientifique, etc.

## Exemples

- [CIDOC CRM (Explorable Version)](http://cidoc-crm.org/html/cidoc_crm_v7.1.1_with_translations.html)
- [CIDOC CRM (Navigable Version)](http://www.cidoc-crm.org/Version/version-7.1.1)
- [CIDOC CRM (OWL Version)](http://erlangen-crm.org/current-version)
- [CIDOC CRM (RDFS Version)](https://cidoc-crm.org/rdfs/7.1.1/CIDOC_CRM_v7.1.1.rdfs)
- [CIDOC CRM (XML Version)](http://cidoc-crm.org/html/cidoc_crm_v7.1.1.xml)

## Autres ressources

- Bruseker (2019) [“Learning Ontology & CIDOC CRM”](https://www.archesproject.org/wp-content/uploads/2020/04/Learning-Ontology-CRM.pdf)
- Bruseker, Carboni, & Guillem (2017) [“Cultural Heritage Data Management: The Role of Formal Ontology and CIDOC CRM”](https://link.springer.com/chapter/10.1007/978-3-319-65370-9_6)
- Bruseker & Guillem (2021) [“CIDOC CRM Game”](http://www.cidoc-crm-game.org/) [Jeu]
- Canning (2021) [“Ontologies at LINCS—Part 5: LINCS and CIDOC-CRM”](https://www.youtube.com/watch?v=VOkF7A_gTd0) [Vidéo]
- CIDOC CRM (2022) [“Compatible Models & Collaborations”](https://cidoc-crm.org/collaboration_resources)
- CIDOC CRM (2022) [“What is the CIDOC CRM?”](https://cidoc-crm.org/)
- Doerr (2003) [“The CIDOC Conceptual Reference Module: An Ontological Approach to Semantic Interoperability of Metadata”](https://ojs.aaai.org//index.php/aimagazine/article/view/1720)
- Oldman, Doerr, de Jong, Norton, & Wikman (2014) [“Realizing Lessons of the Last 20 Years: A Manifesto for Data Provisioning & Aggregation Services for the Digital Humanities”](http://www.dlib.org/dlib/july14/oldman/07oldman.html)
- Oldman & Kurtz (2014) [“The CIDOC Conceptual Reference Model (CIDOC-CRM): PRIMER”](http://www.cidoc-crm.org/sites/default/files/CRMPrimer_v1.1_1.pdf)
- PARTHENOS (2022) [“Formal Ontologies: A Complete Novice’s Guide”](http://training.parthenos-project.eu/sample-page/formal-ontologies-a-complete-novices-guide/)
- Stead (2022) [“CIDOC CRM Tutorial”](https://cidoc-crm.org/cidoc-crm-tutorial) [Diapositives et vidéo]
