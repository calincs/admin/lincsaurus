---
id: property-graph
title: Graphique de propriété
definition: Un graphe où les relations (propriétés) entre les entités sont nommées et portent certaines propriétés définies qui leur sont propres, étendant la base de données de graphes de base des triplets liés pour montrer des connexions complexes qui décrivent comment différents types de métadonnées sont liés.
---

Un graphe de propriétés (également connu sous le nom de graphe de propriétés étiquetées) est un type de modèle de graphe où les relations (:Term[propriétés]{#property}) entre :Term[les entités]{#entity} sont nommées et portent leurs propres propriétés définies. Il étend la base :Term[base de données de graphes]{#graph-database} de :Term[triples]{#triple} pour montrer des connexions complexes qui décrivent comment différents types de métadonnées sont liées, y compris une modélisation plus poussée des dépendances de données. Cependant, les graphes de propriétés n’ont pas le même degré de sophistication que :Term[les graphes de connaissances]{#knowledge-graph}, qui ont tendance à utiliser un schéma plus formalisé, non local :Term[Uniform Resource Identifiers (URI)]{#uniform-resource-identifier}, et sont conçus (en théorie) pour permettre de fédérer des données sur plusieurs ensembles de données.

## Autres ressources

- Foote (2022) [“Property Graphs vs. Knowledge Graphs”](https://www.dataversity.net/property-graphs-vs-knowledge-graphs/)
- Knight (2021) [“What Is a Property Graph?”](https://www.dataversity.net/what-is-a-property-graph/)
