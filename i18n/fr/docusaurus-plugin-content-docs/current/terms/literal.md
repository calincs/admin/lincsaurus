---
id: literal
title: Littéral
definition: Un objet dans un triplet qui ne fait pas référence à une ressource avec un identificateur de ressource uniforme (URI), mais transmet à la place une valeur, telle que du texte, un nombre ou une date.
---

Certains objets dans :Term[Resource Description Framework (RDF)]{#resource-description-framework} ne font pas référence à d’autres ressources avec un :Term[Uniform Resource Identifier (URI)]{#uniform-resource-identifier}, mais transmettent à la place une valeur, telle que du texte, un nombre ou une date. Ces objets sont appelés littéraux.

Il existe deux types de littéraux :

1. Littéraux simples : chaînes avec une balise de langue
2. Littéraux typés : chaînes avec une référence URI de type de données

## Exemples

- University of Saskatchewan Art Gallery: L’extrait de tortue suivant indique que le titre de 1998.004.001 a un contenu symbolique, un littéral ordinaire de “Night Fire” avec une étiquette en anglais.

```turtle
<http://id.lincsproject.ca/kl4QYIdodRH>
        a crm:E35_Title ;
        rdfs:label "Title of University of Saskatchewan Art Gallery object 1998.004.001"@en ;
        crm:P190_has_symbolic_content "Night Fire"@en .
```

## Autres ressources

- Apache Jena (2022) [“Typed Literals How-To”](https://jena.apache.org/documentation/notes/typed-literals.html)
- Emmons et al. (2011) [“RDF Literal Data Types in Practice”](http://iswc2011.semanticweb.org/fileadmin/iswc/Papers/Workshops/SSWS/Emmons-et-all-SSWS2011.pdf)
- W3C (2004) [“3.4 Literals”](https://www.w3.org/TR/2004/REC-rdf-concepts-20040210/#section-Literals)
- W3C (2004) [“6.5 RDF Literals”](https://www.w3.org/TR/2004/REC-rdf-concepts-20040210/#section-Graph-Literal)
