---
id: fair-principles
title: FAIR Principles
definition: A set of principles (findability, accessibility, interoperability, and reusability) for data and metadata management and stewardship.
Last Translated: 2024-06-21
---

The FAIR Principles are a set of principles for data and :Term[metadata]{#metadata} management and stewardship: findability, accessibility, interoperability, and reusability. The acronym was coined by a consortium of scientists and organizations in [Scientific Data](https://www.nature.com/articles/sdata201618) (2016) to improve the management of digital assets.

For data to be “FAIR,” it must be:

- **Findable:** Data need to be easy to locate for both humans and computers.
- **Accessible:** Data need to be accessible to users.
- **Interoperable:** Data need to integratable with other data and interoperable with applications and workflows for analysis, storage, and processing.
- **Reusable:** Data need to be well-described so they can be used in different settings.

FAIR emphasizes machine-actionability since data’s increase in volume and complexity has made people more dependent on computational support.

## Further Resources

- Data.org (2024) [“The FAIR Data Principles”](https://data.org/resources/the-fair-data-principles/)
- [Fair Data (Wikipedia)](https://en.wikipedia.org/wiki/FAIR_data)
- GoFAIR (2014) [“FAIR Principles”](https://www.go-fair.org/fair-principles/)
- Statistics Canada (2022) [“FAIR data principles: What is FAIR?”](https://www.statcan.gc.ca/en/wtc/data-literacy/catalogue/892000062022002) [Video]
- Wilkinson, et al. (2016) [“The FAIR Guiding Principles for Scientific Data Management and Stewardship”](https://www.nature.com/articles/sdata201618)
