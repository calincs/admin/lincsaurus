---
id: property-graph
title: Property Graph
definition: A graph where relationships (properties) between entities are named and carry some defined properties of their own, extending the basic graph database of linked triples to show complex connections that describe how different types of metadata relate.
---

A property graph (also known as a labeled-property graph) is a type of graph model where relationships (:Term[properties]{#property}) between :Term[entities]{#entity} are named and carry some defined properties of their own. It extends the basic :Term[graph database]{#graph-database} of linked :Term[triples]{#triple} to show complex connections that describe how different types of :Term[metadata]{#metadata} relate, including greater modeling of data dependencies. However, property graphs do not have the same degree of sophistication as :Term[knowledge graphs]{#knowledge-graph}, which tend to use more formalized schema, non-local :Term[Uniform Resource Identifiers (URIs)]{#uniform-resource-identifier}, and are designed (in theory) to allow federating data across multiple datasets.

## Further Resources

- Foote (2022) [“Property Graphs vs. Knowledge Graphs”](https://www.dataversity.net/property-graphs-vs-knowledge-graphs/)
- Knight (2021) [“What Is a Property Graph?”](https://www.dataversity.net/what-is-a-property-graph/)
